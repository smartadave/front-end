import React, {useEffect} from 'react';
import {Router, Route, Switch} from 'react-router-dom';
import {getCookie} from 'utilities/cookie';
import {useDispatch, useSelector} from 'react-redux';
import history from './history';
import {ManagerGuard, ProtectedRoute} from 'utilities/router';
import Header from 'components/Header';
import Login from 'pages/Login';
import ForgotPassword from 'pages/ForgotPassword';
import CreateNewPassword from 'pages/CreateNewPassword';
import SignUp from 'pages/SignUp';
import EmailWasSent from 'pages/EmailWasSent';
import Loader from 'components/Loader';
import Dashboard from 'pages/Dashboard';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer} from 'react-notifications';
import {reauthenticate} from 'store/actions/authActions';
import ChangePassword from 'pages/ChangePassword';
import EditPersonalInfo from 'pages/EditPersonalInfo';
import {getUser} from 'store/actions/userActions';
import {getCampuses, getAmenities} from 'store/actions/constantsActions';
import PreviewProperty from 'pages/AddPropertySteps/PreviewProperty';
import Favorites from 'pages/Favorites';
import PersonalApplication from 'pages/PersonalApplication';
import Conversations from 'pages/Conversations';

import AddPropertyStart from 'pages/AddPropertySteps/AddPropertyStart';
import PropertyInformation from 'pages/AddPropertySteps/PropertyInformation';
import UnitInformation from 'pages/AddPropertySteps/UnitInformation';
import MyProperties from './pages/MyProperties';

import {getConversationsList} from 'store/actions/conversationActions';

function App() {
  const dispatch = useDispatch();
  const loader = useSelector((state) => state.loader.loader);
  const auth_user = useSelector((state) => state.auth.user);

  const initialize = () => {
    const token = getCookie('token');
    const refresh_token = getCookie('refresh_token');
    const user_data = getCookie('user');

    const pathname = window.location.pathname;

    if ((token, refresh_token, user_data)) {
      dispatch(reauthenticate({user_data, token, refresh_token}));

      if (pathname === '/') {
        history.push('/dashboard');
      }
    }
  };

  useEffect(() => {
    initialize();
  }, []);


  useEffect(() => {
    if (auth_user) {
      dispatch(getUser(auth_user.id, auth_user.role));
      dispatch(getConversationsList(auth_user.id))
    }
  }, [auth_user]);

  useEffect(() => {
    dispatch(getAmenities());
    return dispatch(getCampuses());
  }, [dispatch]);

  const router = () => {
    return (
      <>
        <Header />
        <Switch>
          <Route exact path="/login" render={(props) => <Login {...props} />} />
          <Route
            exact
            path="/forgot-password"
            render={(props) => <ForgotPassword {...props} />}
          />
          <Route
            exact
            path="/reset-password"
            render={(props) => <CreateNewPassword {...props} />}
          />
          <Route
            exact
            path="/sign-up"
            render={(props) => <SignUp {...props} />}
          />
          <Route
            exact
            path="/sign-up"
            render={(props) => <SignUp {...props} />}
          />
          <Route
            exact
            path="/email-was-sent"
            render={(props) => <EmailWasSent {...props} />}
          />
          <ProtectedRoute
            exact
            path="/dashboard"
            render={(props) => <Dashboard {...props} />}></ProtectedRoute>
          <ProtectedRoute
            path="/change-password"
            render={(props) => <ChangePassword {...props} />}></ProtectedRoute>
          <ProtectedRoute
            path="/personal-information"
            render={(props) => (
              <EditPersonalInfo {...props} />
            )}></ProtectedRoute>
          <ProtectedRoute
            exact
            path="/property"
            render={(props) => (
              <PreviewProperty {...props} viewType="list-view" />
            )}></ProtectedRoute>
          <ProtectedRoute
            exact
            path="/favorites"
            render={(props) => <Favorites {...props} />}></ProtectedRoute>

          {/* Create Property */}
          <ManagerGuard
            exact
            path="/add-property/create-property"
            render={(props) => (
              <AddPropertyStart {...props} />
            )}></ManagerGuard>
          <ManagerGuard
            exact
            path="/add-property/property-information"
            render={(props) => (
              <PropertyInformation {...props} />
          )}></ManagerGuard>
          <ManagerGuard
            exact
            path="/add-property/unit-information"
            render={(props) => <UnitInformation {...props} />}></ManagerGuard>
          <ManagerGuard
            exact
            path="/add-property/preview-property"
            render={(props) => (
              <PreviewProperty {...props} viewType="create-preview" />
            )}></ManagerGuard>
          <ProtectedRoute
            exact
            path="/personal-application/:step"
            render={(props) => (
              <PersonalApplication {...props} />
            )}></ProtectedRoute>
          <ProtectedRoute
            exact
            path="/conversations/:id"
            render={(props) => <Conversations {...props} />}></ProtectedRoute>
          <ProtectedRoute>
          <ManagerGuard
            path="/my-properties"
            render={(props) => <MyProperties {...props} />}></ManagerGuard>
          </ProtectedRoute>
        </Switch>
      </>
    );
  };

  return (
    <div className="app">
      <NotificationContainer />
      {loader && <Loader />}
      <Router history={history}>{router()}</Router>
    </div>
  );
}

export default App;
