import React, {useState, useRef, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import './styles.scss';
import {useOnClickOutside} from 'hooks/clickOutside';

function AdditionalFilteringTab({close_tab, returnAdditionalFilter, value}) {
  const [state, setState] = useState({
    square_feet: {
      min: 10,
      max: 100,
    },
    utilities: false,
    neighborhoods: [],
    unit_amenities: [],
    property_amenities: [],
  });


  useEffect(() => {
    const {
      square_feet,
      utilities,
      neighborhoods,
      unit_amenities,
      property_amenities,
    } = value;
    if (
      (square_feet,
      utilities,
      neighborhoods,
      unit_amenities,
      property_amenities)
    ) {
      setState({
        ...state,
        square_feet,
        utilities,
        neighborhoods,
        unit_amenities,
        property_amenities,
      });
    }
  }, [JSON.stringify(value)]);

  const neighborhoods = useSelector((state) => state.constants.neighborhoods);
  const unit_amenities_list = useSelector(
    (state) => state.constants.unit_amenities,
  );
  const property_amenities_list = useSelector(
    (state) => state.constants.property_amenities,
  );
  const ref = useRef();

  useOnClickOutside(ref, () => close_tab());

  const handleNeighborhoods = (neighborhood) => {
    const neighborhood_index = state.neighborhoods.indexOf(neighborhood);
    const copy_neighborhoods = [...state.neighborhoods];
    copy_neighborhoods.splice(neighborhood_index, 1);

    if (neighborhood_index > 0) {
      setState({
        ...state,
        neighborhoods: [...copy_neighborhoods],
      });
    } else {
      setState({
        ...state,
        neighborhoods: [...state.neighborhoods, neighborhood],
      });
    }
  };

  const handlePropertyAmenities = (amenities) => {
    const index_property_amenities = state.property_amenities.indexOf(
      amenities,
    );
    const copy_property_amenities = [...state.property_amenities];
    copy_property_amenities.splice(index_property_amenities, 1);
    if (index_property_amenities >= 0) {
      setState({
        ...state,
        property_amenities: copy_property_amenities,
      });
    } else {
      setState({
        ...state,
        property_amenities: [...state.property_amenities, amenities],
      });

    }
  };

  const handleUnitAmenities = (amenities) => {
    const index_unit_amenities = state.unit_amenities.findIndex((amenity) => amenities === amenity);
    const copy_unit_amenities = [...state.unit_amenities];
    if (index_unit_amenities >= 0) {
      setState({
        ...state,
        unit_amenities: copy_unit_amenities,
      });
    } else {
      setState({
        ...state,
        unit_amenities: [...state.unit_amenities, amenities],
      });
    }

  };

  const clear = () => {
    setState({
      ...state,
      square_feet: {
        min: '',
        max: '',
      },
      utilities: false,
      neighborhoods: [],
      unit_amenities: [],
      property_amenities: [],
    });
  };

  const setAdditionalFilter = () => {
    return returnAdditionalFilter({...state});
  };

  const handleSquareInputChange = ({target}) => {
    setState({
      ...state,
      square_feet: {
        ...state.square_feet,
        [target.name]: target.value < 0 ? 0 : +target.value,
      },
    });
  };

  return (
    <div ref={ref} className="additional-filtering-tab">
      <div className="additional-filtering-tab__scroll">
        <div className="additional-filtering-tab__item">
          <span>Square feet</span>
          <div className="additional-filtering-tab__item-square">
            <div className="input">
              <input
                type="number"
                name="min"
                id="square_feet_min"
                placeholder="Min"
                className="input-field"
                onChange={handleSquareInputChange}
                value={state.square_feet.min}
              />
            </div>
            <div className="input">
              <input
                type="number"
                name="max"
                placeholder="Max"
                id="square_feet_max"
                className="input-field"
                onChange={handleSquareInputChange}
                value={state.square_feet.max}
              />
            </div>
          </div>
        </div>
        <div className="additional-filtering-tab__item">
          <span>Utilities</span>
          <label className="input-checkbox">
            <span>Show only options with included utilities</span>
            <input
              onClick={() => setState({...state, utilities: !state.utilities})}
              checked={state.utilities}
              name="utilities"
              required
              type="checkbox"
            />
            <span className="checkmark"></span>
          </label>
        </div>
        <div className="additional-filtering-tab__item">
          <span>Neighborhoods</span>
          <div className="additional-filtering-tab__item-neighborhoods">
            {neighborhoods.map((item, key) => {
              return (
                <label key={key} className="input-checkbox">
                  <span>{item}</span>
                  <input
                    onChange={() => handleNeighborhoods(item)}
                    name="utilities"
                    checked={
                      state.neighborhoods.indexOf(item) >= 0 ? true : false
                    }
                    required
                    type="checkbox"
                  />
                  <span className="checkmark"></span>
                </label>
              );
            })}
          </div>
        </div>
        <div className="additional-filtering-tab__item">
          <span>Unit Amenities</span>
          <div className="amenities-list">
            {unit_amenities_list.map((item) => (
              <div key={item._id || item.name} className="amenity-option">
                <img
                  className={`amenity-image${
                    state.unit_amenities.indexOf(item.name) >= 0
                      ? ' selected'
                      : ''
                  }`}
                  src={`/images/${item.icon_name}${
                    state.unit_amenities.indexOf(item.name) >= 0
                      ? '_selected'
                      : ''
                  }.svg`}
                  onClick={() => handleUnitAmenities(item.name)}
                />
                <span>{item.name}</span>
              </div>
            ))}
          </div>
        </div>
        <div className="additional-filtering-tab__item">
          <span>Property Amenities</span>
          <div className="amenities-list">
            {property_amenities_list.map((item) => (
              <div key={item._id || item.name} className="amenity-option">
                <img
                  className={`amenity-image${
                    state.property_amenities.indexOf(item.name) >= 0
                      ? ' selected'
                      : ''
                  }`}
                  src={`/images/${item.icon_name}${
                    state.property_amenities.indexOf(item.name) >= 0
                      ? '_selected'
                      : ''
                  }.svg`}
                  onClick={() => handlePropertyAmenities(item.name)}
                />
                <span>{item.name}</span>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="additional-filtering-tab__actions">
        <button
          className="btn draft-button"
          name="draft"
          type="button"
          onClick={clear}>
          Clear
        </button>
        <button
          className="btn next-button"
          name="next"
          type="submit"
          onClick={setAdditionalFilter}>
          Done
        </button>
      </div>
    </div>
  );
}

export default AdditionalFilteringTab;
