import React from 'react';
import './styles.scss';
import Modal from 'react-modal';

function AlertModal({modal_visible, title, text, additional_text, cancelAction}) {
    return (
        <Modal
            isOpen={modal_visible}
            className="modal alert-modal"
            onRequestClose={cancelAction}>
            <h3 className="title">{title}</h3>
            <div className="info-text">
                <p>{text}</p>
                {additional_text && <p>{additional_text}</p>}
            </div>
            <div className="action-buttons">
                <button className="btn cancel-btn" onClick={cancelAction}>
                    Ok
                </button>
            </div>
        </Modal>
    );
}

export default AlertModal;