import React, {useState, useEffect} from 'react';
import './styles.scss';

function BooleanCheckbox({title, handleInputChange, name, value}) {
  const [state, setState] = useState({
    checked: true,
  });

  useEffect(() => {
    setState({
      ...state,
      checked: value
    })

  }, [value])

  const handleChecked = (e,type) => {
    if(type === 'No') {
      setState({
        ...state,
        checked: false
      })
    }
    else {
      setState({
        ...state,
        checked: true
      })
    }
    selectValue(type, name)
  }


  const selectValue = (type, name) => {
    let e = {
      target: {
        name: name,
        value: type === 'No' ? false : true,
      },
    };
    handleInputChange(e);
  };

  return (
    <div className="boolean-checkbox">
      <span className="title-checkbox">{title}</span>
      <div className="boolean-checkboxes">
        <label className="container">
          <span className="answer">Yes</span>
          <input
            onClick={(e) => handleChecked(e,'Yes')}
            checked={state.checked}
            type="checkbox"
          />
          <span className="checkmark"></span>
        </label>
        <label className="container">
          <span className="answer">No</span>
          <input
            onClick={(e) => handleChecked(e, 'No')}
            checked={!state.checked}
            type="checkbox"
          />
          <span className="checkmark"></span>
        </label>
      </div>
    </div>
  );
}

export default BooleanCheckbox;
