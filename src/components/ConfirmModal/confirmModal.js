import React from 'react';
import './styles.scss';
import Modal from 'react-modal';

function ConfirmModal({
  modal_visible,
  title,
  text,
  additional_text,
  confirmAction,
  cancelAction,
}) {
  return (
    <Modal
      isOpen={modal_visible}
      className="modal"
      onRequestClose={cancelAction}>
      <h3 className="title">{title}</h3>
      <div className="info-text">
        <p>{text}</p>
        {additional_text && <p>{additional_text}</p>}
      </div>
      <div className="action-buttons">
        <button className="btn confirm-btn" onClick={confirmAction}>
          Confirm
        </button>
        <button className="btn cancel-btn" onClick={cancelAction}>
          Cancel
        </button>
      </div>
    </Modal>
  );
}

export default ConfirmModal;
