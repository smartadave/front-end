import React, {useState, useEffect} from 'react';
import './styles.scss';
import {useDispatch, useSelector} from 'react-redux';

function Applications() {
  const auth_user = useSelector((state) => state.auth.user);
  const related_property = useSelector(
    (state) => state.conversations.currentConversations.related_property,
  );

  const [state, setState] = useState({
    related_property: [],
  });

  useEffect(() => {
    if (related_property) {
      setState({
        ...state,
        related_property: related_property,
      });
    }
  }, [related_property]);


  const renderApplicationsList = () => {
    return state.related_property.map(item => {
      return (
        <div key={item._id} className="applications-item">
          <div className="applications-item__status">
            {item.status === "Accept" && <div>
              <span className="accept">Accepted</span>
              <img src="/images/application_accepted.svg" />
            </div>}
            {item.canceled && <div>
              <span className="decline">Declined</span>
              <img src="/images/application_declined.svg"/>
            </div>}
            {item.status === "Pending" && <div>
            <span>Pending...</span>
            </div>}
          </div>
          <h4>{item.name}</h4>
          <h5>{item.address}</h5>
          {/* <h6>Reason of decline: Property isn’t available for rent</h6> */}
        </div>
      );
    })
  };

  return (
    <div className="chat-application">
      <h5 className="title">My Applications</h5>
      <div className="chat-application__list">
        {state.related_property.length > 0 && renderApplicationsList()}
      </div>
    </div>
  );
}

export default Applications;
