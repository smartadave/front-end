import React, {useRef, useState, useEffect} from 'react';
import './styles.scss';
import {useDispatch, useSelector} from 'react-redux';
import Message from 'components/Conversations/Message';
import PropertySelector from 'components/Conversations/PropertySelector';
import {sendMessage} from 'store/middleware/socketMiddleware';
import {useParams} from 'react-router-dom';
import {
  setCurrentConversation,
  setCurrentMessageList,
  uploadFiles,
} from 'store/actions/conversationActions';
import {isMeAuthor, returnInterlocutorName} from 'helpers/conversations';
import Modal from 'react-modal';

function ConversationsChat() {
  const dispatch = useDispatch();
  const auth_user = useSelector((state) => state.auth.user);
  const currentMessagesList = useSelector(
    (state) => state.conversations.currentMessagesList,
  );
  const related_property = useSelector(
    (state) => state.conversations.currentConversations.related_property,
  );
  const conversationsList = useSelector(state => state.conversations.conversationsList)
  const {id} = useParams();
  const fileRef = useRef(null);
  const scroll_to_bottom = useRef(null);

  const [state, setState] = useState({
    currentConversationId: '',
    message: '',
    messagesList: [],
    fullImageUrl: '',
    fullImage: false,
    related_property: [],
  });


  const saveFile = async (e) => {
    e.preventDefault();
    const file = e.target.files[0];
    console.log(file);
    const url = await dispatch(uploadFiles(file));
    console.log(url.data);
    if (url.data.length > 0) {
      createMessage(url.data[0], 'img');
    }
  };

  useEffect(() => {
    if (currentMessagesList) {
      setState({
        ...state,
        messagesList: currentMessagesList,
        related_property: related_property,
      });
    }
  }, [currentMessagesList, related_property]);

  useEffect(() => {
    if(state.currentConversationId.length > 1) {
      scroll_to_bottom.current.scrollIntoView({block: "end"});
    }
  }, [state.messagesList])

  useEffect(() => {
    if (id && id !== 'all') {
      setState({
        ...state,
        currentConversationId: id,
      });
      dispatch(setCurrentConversation(id));
      dispatch(setCurrentMessageList(id));
    }
  }, [id]);

  const createMessage = (message, message_type) => {
    const data = {
      room: state.currentConversationId,
      message: {
        msg_type: message_type,
        message: message,
        author: auth_user.id,
        date: new Date(),
      },
    };
    sendMessage(data);
  };
  const onSubmit = (e) => {
    e.preventDefault();
    createMessage(state.message, 'txt');
    setState({
      ...state,
      message: '',
    });
  };

  const handleInputChange = ({target}) => {
    setState({
      ...state,
      message: target.value,
    });
  };

  const renderMessages = () => {
    return state.messagesList.map((message, key) => {
      const my_message = isMeAuthor(message.author, auth_user.id);
      message.my_message = my_message;
      return (
        <Message
          setFullImageUrl={setFullImageUrl}
          key={message._id}
          data={message}
        />
      );
    });
  };

  const setFullImageUrl = (url) => {
    setState({
      ...state,
      fullImageUrl: url,
      fullImage: true,
    });
  };

  const addMessageFromKey = (e) => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      onSubmit(e)
    }
  };

  return (
    <div className="conversations-chat">
      <Modal
        isOpen={state.fullImage}
        style={customStyles}
        contentLabel="Example Modal">
        <button
          onClick={() => setState({...state, fullImage: false})}
          type="button"
          className="btn-link close-modal">
          <img src="/images/close.svg" />
        </button>
          <img className="full-media" src={state.fullImageUrl} />
      </Modal>
      {state.currentConversationId && (
        <>
          <header>
            <div>
              <h4>{returnInterlocutorName(conversationsList.find(element => element._id === state.currentConversationId).users, auth_user)}</h4>
            </div>
            {state.related_property.length > 0 && <PropertySelector related_property={state.related_property} />}
          </header>
          <div style={{height: 'calc(100% - 65px)', overflow: 'overlay'}}>
            <div className="conversations-chat__messages">
              {renderMessages()}
              <div ref={scroll_to_bottom} />
            </div>
          </div>
          <div className="conversations-chat__input-message">
            <form
              action="."
              onSubmit={onSubmit}
              // onKeyPress={this.handleMessageKeyPressEvent}
            >
              <div className="file-upload">
                <label htmlFor="file-input">
                  <img src="/images/attach_file.svg" />
                </label>
                <input
                  onChange={saveFile}
                  style={{width: '100%', marginTop: '15px'}}
                  type="file"
                  id="file-input"
                  ref={fileRef}
                />
              </div>

              <textarea
                value={state.message}
                onKeyDown={addMessageFromKey}
                onChange={handleInputChange}
                type="text"
              />
              <img
                style={{padding: '0 6px'}}
                src="/images/add_doc_to_sign.svg"
              />
              <button style={{border: 0, padding: 0}} type="submit">
                <img src="/images/send_message.svg" />
              </button>
            </form>
          </div>
        </>
      )}
    </div>
  );
}

export default ConversationsChat;


const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    border: 0,
    boxShadow: '0px 4px 24px rgba(0, 0, 0, 0.07)',
    transform: 'translate(-50%, -50%)',
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
  },
};