import React, {useState, useEffect} from 'react';
import './styles.scss';
import ConversationsListItem from 'components/Conversations/ConversationsListItem';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';

function ConversationsList() {
  const dispatch = useDispatch();
  const conversationsList = useSelector((state) => state.conversations.conversationsList);
  const [state, setState] = useState({
    chats_block: true,
    conversation_list: [],
  });

  useEffect(() => {
   setState({
     ...state,
     conversations_list: conversationsList
   })
  }, [conversationsList])

  return (
    <div className="conversations-list">
      <div className="search-input">
        <button className="search-icon">
          <img alt="search_icon" src="/images/search.svg" />
        </button>
        <input className="input-field" placeholder="Search" />
      </div>
      <header>
        <button
          style={{
            color: '#3B3B3B',
            fontFamily: state.chats_block
              ? 'Montserrat-Medium'
              : 'Montserrat-Regular',
          }}
          onClick={() => setState({...state, chats_block: true})}>
          Chats
        </button>
        <button
          style={{
            color: '#787ae3',
            fontFamily: !state.chats_block
              ? 'Montserrat-Medium'
              : 'Montserrat-Regular',
          }}
          onClick={() => setState({...state, chats_block: false})}>
          new request
        </button>
      </header>
      {conversationsList.map((message, key) => {
        if(message.users.length === 2) {
          return (
            <ConversationsListItem key={message._id} data={message} />
          )
        }
        })}
    </div>
  );
}

export default ConversationsList;
