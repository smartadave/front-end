import React from 'react';
import './styles.scss';
import {useParams} from 'react-router-dom';
import history from 'history.js';
import {useDispatch, useSelector} from 'react-redux';
import {returnInterlocutorName} from 'helpers/conversations';
import {deleteConversations} from 'store/actions/conversationActions';

function ConversationsListItem({data}) {
  const dispatch = useDispatch()
  const auth_user = useSelector((state) => state.auth.user);
  const {id} = useParams();

  const deleteConv = () => {
    history.push(`/conversations/all`);
    dispatch(deleteConversations(data._id))
  }

  return (
    <div
      onClick={() => history.push(`/conversations/${data._id}`)}
      className={`conversations-lis-item ${
        id == data._id ? 'message-list-active' : ''
      }`}>
      <div className="conversations-lis-item__left">
        <h4>{returnInterlocutorName(data.users, auth_user)}</h4>
        <h5 className={data.read ? 'unread-message' : ''}>
        {data.messages.length > 0 ? data.messages[data.messages.length - 1].message :  "Start communicating with this user."}
        </h5>
      </div>
      <div className="conversations-lis-item__right">
        <span className={data.read ? 'unread-circle' : ''}></span>
        <h5>2h</h5>
      </div>

      <button onClick={deleteConv} className="delete-conversation-button">
        <img src="/images/delete-black.svg"/>
      </button>
    </div>
  );
}

export default ConversationsListItem;
