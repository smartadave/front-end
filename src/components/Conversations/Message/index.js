import React from 'react';
import './styles.scss';


function Message({data, setFullImageUrl}) {

  const renderMessage = () => {
    if(data.msg_type === 'txt') {
     return (<span className="message__block__txt">{data.message}</span>)
    }
    if(data.msg_type === 'img') {
     return (
      <img
      onClick={() => setFullImageUrl(data.message)}
      alt="image_message"

      className="message__block__img"
      src={data.message}
    />
     )
    }
  }
  return (
    <>
    {data.msg_type === 'system' && <span className="message__block__system">{data.message}</span>}
    {data.msg_type !== 'system' && <div style={{alignSelf: data.my_message ? 'flex-end' : 'flex-start'}} className='message'>
      <div style={{backgroundColor: data.my_message ? '#F1F2FC' : '#F8F8F8'}} className="message__block">
        {renderMessage()}
      </div>
    </div>}
    </>
  );
}

export default Message;
