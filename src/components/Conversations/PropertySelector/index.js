import React, {useState, useRef, useEffect} from 'react';
import './styles.scss';
import {useOnClickOutside} from 'hooks/clickOutside';

function PropertySelector({related_property}) {
  const [state, setState] = useState({
    open_list: false,
    related_property: related_property,
  });

  useEffect(() => {
    if (related_property) {
      setState({
        ...state,
        related_property: related_property,
      });
    }
  }, [related_property]);

  const ref = useRef(null);
  useOnClickOutside(ref, () => {
    state.open_list && handleShowOptions(false);
  });

  const handleShowOptions = (show_options) => {
    setState({
      ...state,
      open_list: show_options,
    });
  };

  return (
    <div ref={ref} className="property-selector">
      <div
        onClick={() => handleShowOptions(true)}
        className="active-property property-selector__item">
        <img src={state.related_property[0].photo}/>
        <div style={{width: '100%'}}>
          <h5>related_property</h5>
          <h3>{state.related_property[0].name}</h3>
        </div>
      </div>
      {state.open_list && state.related_property.length > 1 && (
        <div className="property-selector__options">
          {state.related_property.map((item) => {
            return (
              <div
                key={item._id}
                style={{width: '100%', marginBottom: '8px'}}
                onClick={() => handleShowOptions(true)}
                className="active-property property-selector__item">
                <img src={item.photo}/>
                <div>
                  <h5>{item.name}</h5>
                  <h3>{item.address}</h3>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}

export default PropertySelector;
