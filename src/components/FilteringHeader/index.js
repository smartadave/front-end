import React, {useState, useEffect, useRef} from 'react';
import './styles.scss';
import Selector from 'components/Selector';
import RangeSelector from 'components/RangeSelector';
import AdditionalFilteringTab from 'components/AdditionalFilteringTab';
import {useDispatch, useSelector} from 'react-redux';
import {useOnClickOutside} from 'hooks/clickOutside';
import {createNewFilter, saveFilter} from 'store/actions/userActions';
import {getViewList, getMapViewList} from 'store/actions/propertyListActions';

function FilteringHeader() {
  const [state, setState] = useState({
    sort: '',
    filtering: {
      price_per_bedroom: true,
      price_per_entire_unit: false,
      filtering_type: [],
      bedrooms: {min: 1, max: 10},
      price: {min: 0, max: 10000},
      square_feet: {
        min: 10,
        max: 1000,
      },
      utilities: true,
      neighborhoods: [],
      unit_amenities: [],
      property_amenities: [],
    },
    show_more: false,
    range_data: null,
  });
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const filter_range = useSelector((state) => state.propertyList.filter_range);

  const ref = useRef();

  useEffect(() => {
    if (filter_range) {
      console.log('here', filter_range);
      setState({
        ...state,
        range_data: filter_range,
        filtering: {
          ...state.filtering,
          bedrooms: {
            min: filter_range.bedrooms.min,
            max: filter_range.bedrooms.max,
          },
          square_feet: {
            min: filter_range.area.min,
            max: filter_range.area.max,
          },
          price: {
            min: state.filtering.price_per_bedroom
              ? filter_range.by_bedroom.min_price
              : filter_range.entirely.min_price,
            max: state.filtering.price_per_bedroom
              ? filter_range.by_bedroom.max_price
              : filter_range.entirely.max_price,
          },
        },
      });
    }
  }, [filter_range]);

  useEffect(() => {
    dispatch(getViewList(state.filtering, 0, 5, true));
    dispatch(getMapViewList({filtering: state.filtering}));
    Object.keys(user.saved_filters).length > 0 && saveNewFilterSet();
    dispatch(saveFilter({...state.filtering}));
  }, [JSON.stringify(state.filtering)]);

  useEffect(() => {
    if (
      Object.keys(user.saved_filters).length > 0 &&
      JSON.stringify(user.saved_filters) !== JSON.stringify(state.filtering)
    ) {
      setState({
        ...state,
        filtering: {...state.filtering, ...user.saved_filters},
      });
    }
  }, []);

  const handlePricePerBedroom = (type) => {
    setState({
      ...state,
      filtering: {
        ...state.filtering,
        price: {
          min: state.filtering.price_per_bedroom
            ? filter_range.by_bedroom.min_price
            : filter_range.entirely.min_price,
          max: state.filtering.price_per_bedroom
            ? filter_range.by_bedroom.max_price
            : filter_range.entirely.max_price,
        },
        price_per_bedroom: type === 'price_per_bedroom' ? true : false,
        price_per_entire_unit: type === 'price_per_entire_unit' ? true : false,
      },
    });
  };

  const resetFiltering = () => {
    const default_filtering = {
      price_per_bedroom: true,
      price_per_entire_unit: false,
      filtering_type: [],
      price: {
        min: state.price_per_bedroom
          ? filter_range.by_bedroom.min_price
          : filter_range.entirely.min_price,
        max: state.price_per_bedroom
          ? filter_range.by_bedroom.max_price
          : filter_range.entirely.max_price,
      },
      bedrooms: {
        min: filter_range.bedrooms.min,
        max: filter_range.bedrooms.max,
      },
      square_feet: {
        min: filter_range.area.min,
        max: filter_range.area.max,
      },
      utilities: true,
      neighborhoods: [],
      unit_amenities: [],
      property_amenities: [],
    };
    setState({
      ...state,
      filtering: default_filtering,
    });
  };

  const returnFilteringType = (obj) => {
    setState({
      ...state,
      filtering: {
        ...state.filtering,
        filtering_type: obj,
      },
    });
  };

  const returnRange = (type, data) => {
    setState({
      ...state,
      filtering: {
        ...state.filtering,
        [type]: {
          min: data[0],
          max: data[1],
        },
      },
    });
  };

  const returnAdditionalFilter = (data) => {
    setState({
      ...state,
      show_more: false,
      filtering: {
        ...state.filtering,
        square_feet: data.square_feet,
        utilities: data.utilities,
        neighborhoods: data.neighborhoods,
        unit_amenities: data.unit_amenities,
        property_amenities: data.property_amenities,
      },
    });
  };

  const saveNewFilterSet = () => {
    dispatch(createNewFilter(user._id, state.filtering));
  };

  return (
    <>
      {filter_range && (
        <div className="filtering-tab">
          <div className="filtering-tab__main">
            <div className="filtering-tab__main-buttons">
              <button
                onClick={() => handlePricePerBedroom('price_per_bedroom')}
                className={`filtering-tab__main-button ${
                  state.filtering.price_per_bedroom ? 'active' : ''
                }`}>
                Price per bedroom
              </button>
              <button
                onClick={() => handlePricePerBedroom('price_per_entire_unit')}
                className={`filtering-tab__main-button ${
                  state.filtering.price_per_entire_unit ? 'active' : ''
                }`}>
                Price per entire unit
              </button>
            </div>
            <div className="filtering-tab__main-selectors">
              <RangeSelector
                returnRange={(type, data) => returnRange(type, data)}
                type="price"
                placeholder="Price"
                style={{width: '180px', padding: 0, marginRight: '8px'}}
                inputStyle={{padding: '8.5px 14px', width: '180px'}}
                optionsStyle={{top: '40px', width: '340px'}}
                rangeData={{
                  min: state.filtering.price_per_bedroom
                    ? filter_range.by_bedroom.min_price
                    : filter_range.entirely.min_price,
                  max: state.filtering.price_per_bedroom
                    ? filter_range.by_bedroom.max_price
                    : filter_range.entirely.max_price,
                  step: 10,
                }}
                value={{
                  min: state.filtering.price.min
                    ? state.filtering.price.min
                    : 0,
                  max: state.filtering.price.max
                    ? state.filtering.price.max
                    : 10000,
                }}
              />
              <RangeSelector
                returnRange={(type, data) => returnRange(type, data)}
                type="bedrooms"
                placeholder="Bedrooms"
                style={{width: '180px', padding: 0, marginRight: '8px'}}
                inputStyle={{padding: '8.5px 14px', width: '180px'}}
                optionsStyle={{top: '40px', width: '340px'}}
                rangeData={{
                  min: filter_range.bedrooms.min,
                  max: filter_range.bedrooms.max,
                  step: 1,
                }}
                value={{
                  min: state.filtering.bedrooms.min,
                  max: state.filtering.bedrooms.max,
                }}
              />
              <Selector
                style={{width: '120px', padding: 0, marginRight: '8px'}}
                inputStyle={{padding: '8.5px 14px', width: '120px'}}
                optionsStyle={{top: '40px', width: '220px'}}
                placeholder="Type"
                name="type"
                options={filtering_type}
                value={state.filtering.filtering_type}
                // imageType
                handleMultiInputChange={(obj) => returnFilteringType(obj)}
                multiselect
              />
              <button
                onClick={() =>
                  setState({
                    ...state,
                    show_more: !state.show_more && true,
                  })
                }
                className="filtering-tab__show-more">
                <span>More</span>
                <img
                  src={
                    state.show_more
                      ? '/images/arrow_drop_up.svg'
                      : '/images/arrow_drop_down.svg'
                  }
                />
              </button>
            </div>
          </div>
          <div ref={ref} className="filtering-tab__additional">
            <button onClick={() => resetFiltering()} className="my-filtering">
              <img src="/images/filter.svg" />
              <span>Reset Filters</span>
            </button>

            <Selector
              placeholder="Sorting"
              style={{padding: 0}}
              optionsStyle={{width: '200px', right: '20px', top: '110px'}}
              name="type"
              options={sorting_type}
              value={state.type}
              imageType
              img="/images/sort.svg"
              handleMultiInputChange={(obj) => returnFilteringType(obj)}
            />
          </div>
          {state.show_more && (
            <AdditionalFilteringTab
              value={state.filtering}
              returnAdditionalFilter={returnAdditionalFilter}
              close_tab={() =>
                setState({
                  ...state,
                  show_more: false,
                })
              }
            />
          )}
        </div>
      )}
    </>
  );
}

export default FilteringHeader;

const filtering_type = [
  {
    value: 'Apartment complex',
    name: 'Apartment complex',
  },
  {
    value: 'House',
    name: 'House',
  },
];

const sorting_type = [
  {
    value: 'Most recent',
    name: 'Most recent',
  },
  {
    value: 'Closest to Campus',
    name: 'Closest to Campus',
  },
  {
    value: 'Price (Low to High)',
    name: 'Price (Low to High)',
  },
  {
    value: 'Price (High to Low)',
    name: 'Price (High to Low)',
  },
];
