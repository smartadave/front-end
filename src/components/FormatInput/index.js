import React, {useEffect, useRef, useState} from 'react';

function PriceInput({
  format,
  name,
  id = name,
  required = false,
  placeholder = '',
  value,
  handleChange,
  className,
  label,
}) {
  const [state, setState] = useState({
    value: '',
    formatted_value: '',
    caret: 1,
    show_hint: false
  });

  const inputRef = useRef(null);

  useEffect(() => {
    if (!format) {
      console.error('Provide input format');
    }
  }, []);

  useEffect(() => {
    setState({
      ...state,
      value,
      formatted_value: formatValue(),
    });
  }, [value]);

  useEffect(() => {
    inputRef.current.selectionStart = state.caret;
    inputRef.current.selectionEnd = state.caret;
  }, [state]);

  const formatValue = () => {
    switch (format) {
      case 'price':
        return formatPrice(value);
      case 'phone':
        return formatPhone(value);
      default:
        return '';
    }
  };
  // PRICE formatters

  const formatPrice = (value) => {
    if (!value || value.length === 0) {
      return '';
    }
    const regex = /\d+/g;
    let newValue = value.toString().match(regex).join('');
    newValue = newValue.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return `$ ${newValue}`;
  };

  const handlePriceChange = ({target}) => {
    const filtered_value = target.value.match(/\d+/g);
    const value = filtered_value ? filtered_value.join('') : '';
    const caret = value.length === 1
        ? 3
        : value.length === 4
        ? 7
        : target.selectionEnd;

    setState({
      ...state,
      caret
    });

    const event = {
      target: {
        name: name,
        id: id,
        value,
      },
    };
    handleChange(event);
  };

  // PHONE formatters

  const formatPhone = (value) => {
    if (!value || value === '' || value === ' ') {
      return '';
    }
    const newValue = value.replace(/\D/g, '').substring(0, 10);
    const area = newValue.substring(0, 3);
    const middle = newValue.substring(3, 6);
    const last = newValue.substring(6, 10);

    if (newValue.length > 6) {
      return `(${area}) ${middle}-${last}`;
    } else if (newValue.length > 3) {
      return `(${area}) ${middle}`;
    } else if (newValue.length > 0) {
      return `(${area}`;
    } else return '';
  };

  const handlePhoneChange = ({target}) => {
    const regex = /\d+/g;
    const filtered_value = (target.value !== '' && target.value !== '(') ? target.value.match(regex) : '';
    const value = filtered_value
        ? filtered_value.join('')
        : '';
    const caret =
      value.length === 1
        ? 2
        : value.length === 4
        ? 7
        : value.length === 7
        ? 11
        : target.selectionEnd;
    setState({
      ...state,
      caret: caret,
    });

    const event = {
      target: {
        name: target.name,
        value,
      },
    };
    handleChange(event);
  };

  return format === 'price' ? (
    <div className={`input ${className ? className : ''}`}>
      <label htmlFor={name} className="input-label">
        {label}
      </label>
      <input
        ref={inputRef}
        type="text"
        name={name}
        id={id}
        className="input-field"
        required={required}
        placeholder={placeholder}
        value={state.formatted_value}
        onChange={(event) => handlePriceChange(event)}
      />
    </div>
  ) : format === 'phone' ? (
    <div className={`input ${className ? className : ''}`}>
      <label htmlFor={name} className="input-label">
        {label}
      </label>
      <input
        format="phone"
        ref={inputRef}
        type="text"
        name={name}
        id={id}
        className="input-field"
        required={required}
        placeholder={placeholder}
        value={state.formatted_value}
        onChange={(event) => handlePhoneChange(event)}
        onFocus={() => setState({...state, show_hint: true})}
        onBlur={() => setState({...state, show_hint: false})}
      />
      {state.show_hint && <span className="input-hint">US number format only (233) 4444-444</span>}
    </div>
  ) : null;
}

export default PriceInput;
