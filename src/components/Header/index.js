import React, {useState, useRef} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Link, useLocation} from 'react-router-dom';
import './styles.scss';
import {useOnClickOutside} from 'hooks/clickOutside';
import {logout} from 'store/actions/authActions';
import {useWindowSize} from 'hooks/windowSize';
import history from '../../history';

function Header() {
  const location = useLocation();
  const auth_user = useSelector((state) => state.auth.user);
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [isMenuOpen, handleMenu] = useState(false);
  const ref = useRef();
  const window_size = useWindowSize();

  const logOut = () => {
    dispatch(logout(user.id));
  };

  useOnClickOutside(ref, () => {
    isMenuOpen && handleMenu(false);
  });

  return (
    <div ref={ref} className="header">
      <Link to="../dashboard">
        <img
          alt="logo"
          src="/images/smarta-logo.svg"
          className="header__logo"
        />
      </Link>
      {auth_user && user._id && (
        <div className="user">
          <Link to="../conversations/all">
            <img
              alt="chat"
              src={
                location.pathname.search('conversation') >= 0
                  ? '/images/messages_outline-active.svg'
                  : '/images/messages_outline.svg'
              }
            />
          </Link>
          {auth_user.role === 'student' && (
            <Link to="../favorites">
              <div className="favorites">
                <img
                  alt="favourites"
                  className="favorites"
                  src="/images/favorite_border.svg"
                />
                <span>
                  {user.favouriteProperties &&
                  user.favouriteProperties.length === 0
                    ? ''
                    : user.favouriteProperties.length}
                </span>
              </div>
            </Link>
          )}
          {auth_user.role === 'manager' && window_size.width >= 480 && (
            <button
              className="btn"
              onClick={() => history.push('/add-property/create-property')}>
              <Link to="../add-property/create-property">Add a Property</Link>
            </button>
          )}
          <button onClick={() => handleMenu(!isMenuOpen)}>
            <span className="name">
              {user.firstName} {user.lastName}
            </span>
            <img
              alt="drop-down"
              src={
                !isMenuOpen
                  ? '/images/dropdown_header.svg'
                  : '/images/dropdown_header_cl.svg'
              }
            />
          </button>
        </div>
      )}
      {isMenuOpen && (
        <div
          className={`dropdown-menu ${
            auth_user.role === 'manager' ? 'dropdown-menu__manager' : ''
          }`}>
          <Link to="../personal-information">
            <button onClick={() => handleMenu(false)}>
              Personal Information
            </button>
          </Link>
          {auth_user.role !== 'manager' && (
            <Link to="../personal-application/applicationInformation">
              <button onClick={() => handleMenu(false)}>
                My Smarta Application
              </button>
            </Link>
          )}
          {auth_user.role === 'manager' && window_size.width <= 480 && (
            <Link to="../add-property/create-property">
              <button className="btn">Add a Property</button>
            </Link>
          )}
          {auth_user.role === 'manager' && (
            <Link to="../my-properties">
              <button onClick={() => handleMenu(false)}>My Properties</button>
            </Link>
          )}
          <button>Renter history</button>
          <Link to="../change-password">
            <button onClick={() => handleMenu(false)}>Change Password</button>
          </Link>
          <button
            onClick={() => {
              logOut();
              handleMenu(false);
            }}
            className="logout">
            Logout
          </button>
        </div>
      )}
    </div>
  );
}

export default Header;
