import React from 'react';
import './styles.scss';

function Loader() {
  return (
    <div className="loader-block">
      <div className="loader" id="loader-4">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  );
}

export default Loader;
