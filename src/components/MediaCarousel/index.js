import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';


const MediaCarousel = ({ mediaList }) => {
    return (
        <div className="media-carousel">
            <Carousel
                showArrows
                showThumbs={false}
                showIndicators={false}
                showStatus={false}>
                {mediaList.map(media => (
                    <div className="image-container">
                        <img
                            className="property-img"
                            src={media}
                            alt="property-photo" />
                    </div>
                ))}
            </Carousel>
            <span className="photos-count">{mediaList.length} photos</span>
        </div>
    );
};

export default MediaCarousel;