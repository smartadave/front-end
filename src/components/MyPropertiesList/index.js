import React, {useEffect, useState} from 'react';
import './styles.scss';
import MyPropertyCard from 'components/MyPropertyCard';

function MyPropertiesList({
  properties,
  drafts,
  showFullInfo,
  setShowFullInfo,
  windowSize,
  showList,
}) {
  const [state, setState] = useState({
    show_option: 'properties',
    property_to_view: '',
    display_properties: [],
    search_value: '',
  });

  useEffect(() => {
    if (properties.length < 0 && drafts.length > 0) {
      setState({
        ...state,
        show_option: 'drafts',
        display_properties: [...drafts],
      });
    } else {
      setState({
        ...state,
        show_option: 'properties',
        display_properties: [...properties],
      });
    }
  }, [properties, drafts]);

  const handleShowOption = ({target}) => {
    const display_properties = filterProperties(
      state.search_value,
      target.name,
    );
    setState({
      ...state,
      show_option: target.name,
      display_properties,
    });
  };

  const handleShowFullInfo = (property_id) => {
    const property = state.display_properties.find(
      (property) => property._id === property_id,
    );
    setShowFullInfo(property);
  };

  const handleSearchProperty = ({target}) => {
    const filtered_properties = filterProperties(
      target.value,
      state.show_option,
    );
    setState({
      ...state,
      search_value: target.value,
      display_properties: filtered_properties,
    });
  };

  const filterProperties = (value, show_option) => {
    if (value !== '') {
      const properties_to_filter =
        show_option === 'properties' ? [...properties] : [...drafts];
      return properties_to_filter.filter((property) =>
        property.propertyName.toLowerCase().includes(value.toLowerCase()),
      );
    } else {
      return show_option === 'properties' ? [...properties] : [...drafts];
    }
  };

  return (
    <div
      className={`my-properties-list ${windowSize.screen_type} ${
        showList ? 'active' : ''
      }`}>
      <div className="search-input">
        <button className="search-icon">
          <img alt="search_icon" src="/images/search.svg" />
        </button>
        <input
          className="input-field"
          placeholder="Search"
          value={state.search_value}
          onChange={(event) => handleSearchProperty(event)}
        />
      </div>
      <div style={{height: '100%'}} className="list">
        <div className="show-option-buttons">
          <button
            className={`property-option${
              state.show_option === 'properties' ? ' active' : ''
            }`}
            name="properties"
            onClick={(event) => handleShowOption(event)}>
            Properties
          </button>
          <button
            className={`property-option${
              state.show_option === 'drafts' ? ' active' : ''
            }`}
            name="drafts"
            onClick={(event) => handleShowOption(event)}>
            Drafts
          </button>
        </div>
        <div
          className="properties-wrapper"
          style={{overflowY: 'scroll', height: '85%'}}>
          <div className="properties">
            {state.display_properties.length > 0 ? (
              state.display_properties.map((property) => (
                <MyPropertyCard
                  key={property._id}
                  property={property}
                  showFullInfo={showFullInfo}
                  handleShowFullInfo={handleShowFullInfo}
                />
              ))
            ) : (
              <span className="no-properties">No {state.show_option}</span>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default MyPropertiesList;
