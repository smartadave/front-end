import React from 'react';
import './styles.scss';

function MyPropertyCard({
  property,
  showFullInfo,
  handleShowFullInfo = () => {},
}) {
  return (
    <div
      key={property._id}
      className={`my-property-card${
        showFullInfo && showFullInfo._id === property._id ? ' active' : ''
      }`}
      onClick={() => handleShowFullInfo(property._id)}>
      <div className="cover-image">
        <img src={property.photos[0] ? property.photos[0] : '/images/no-image.png'} alt="property_image" />
        <span
          className={`active-listing ${
            property.activeListing ? 'active' : 'inactive'
          }`}
        />
      </div>
      <div className="property-information">
        <p className="property-name">{property.propertyName}</p>
        <span className="property-address">
          {property.address.formatted_address}
        </span>
      </div>
    </div>
  );
}

export default MyPropertyCard;
