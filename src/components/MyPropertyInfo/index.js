import React, {useState, useRef, useEffect} from 'react';
import {useDispatch} from 'react-redux';
import './styles.scss';
import MyPropertyCard from 'components/MyPropertyCard';
import {useOnClickOutside} from '../../hooks/clickOutside';
import MyPropertyView from '../MyPropertyView';
import {
  changeActiveListing,
  deleteProperty,
} from '../../store/actions/myPropertiesActions';
import {formatPropertyByAl} from '../../helpers/createProperty';
import {NotificationManager} from 'react-notifications';
import {handleLoader} from '../../store/actions/loaderActions';
import ConfirmModal from '../ConfirmModal/confirmModal';

function MyPropertyInfo({
  property,
  setProperty,
  editInProgress,
  setEditInProgress,
  windowSize
}) {
  const [state, setState] = useState({
    show_more: false,
    property: {},
    show_modal: false,
    show_publish_modal: false,
  });

  const dispatch = useDispatch();

  const delete_ref = useRef(null);

  useEffect(() => {
    setState({
      ...state,
      property: JSON.parse(JSON.stringify(property)),
    });
  }, [property]);

  const showMoreHandler = () => {
    setState({
      ...state,
      show_more: !state.show_more,
      // edit_in_progress: false,
    });
  };

  const handleEditInProgress = (value) => {
    setEditInProgress(value);
  };

  const toggleActiveListing = async (
    activate,
    property_section,
    section_id,
  ) => {
    dispatch(handleLoader(true));
    const formatted_property = formatPropertyByAl(
      activate,
      state.property,
      property_section,
      section_id,
    );
    const listing_changed = await dispatch(
      changeActiveListing(formatted_property),
    );
    if (listing_changed) {
      setState({
        ...state,
        property: formatted_property,
      });
    } else {
      NotificationManager.error('Something went wrong');
    }
  };

  const toggleShowDeleteModal = () => {
    setState({
      ...state,
      show_modal: !state.show_modal,
    });
  };

  const deletePropertyHandler = async () => {
    setState({
      ...state,
      show_modal: false,
    });
    const isDeleted = await dispatch(
      deleteProperty(state.property._id, state.property.edit_type),
    );
    if (isDeleted) {
      NotificationManager.success('Property was deleted.');
      setProperty(null);
    } else {
      NotificationManager.error('Something went wrong.');
    }
  };

  const openPublishModal = () => {
    setState({
      ...state,
      show_publish_modal: true,
    });
  };

  const closePublishModal = () => {
    setState({
      ...state,
      show_publish_modal: false,
    });
  };

  useOnClickOutside(delete_ref, showMoreHandler);

  return state.property && Object.keys(state.property).length > 0 ? (
    <div className="my-property-info">
      <ConfirmModal
        modal_visible={state.show_modal}
        title={modalWording.title}
        text={modalWording.text}
        confirmAction={deletePropertyHandler}
        cancelAction={toggleShowDeleteModal}
      />
      <div className="property-info-header">
        <MyPropertyCard property={state.property} />
        <div className="header-actions">
          {!editInProgress &&
            (state.property.edit_type === 'properties' ? (
              <label className="input-toggle">
                Active listing
                <input
                  type="checkbox"
                  checked={state.property.activeListing}
                  onChange={() =>
                    toggleActiveListing(
                      !state.property.activeListing,
                      'property',
                    )
                  }
                />
                <span className="control" />
              </label>
            ) : (
              <button className="btn publish-btn" onClick={openPublishModal}>
                Publish Property
              </button>
            ))}
          <button
            className={`show-more${state.show_more ? ' active' : ''}`}
            onClick={showMoreHandler}>
            <img src="/images/more_vert.svg" alt="show_more" />
          </button>
          {state.show_more && (
            <button
              type="button"
              ref={delete_ref}
              className="delete-property"
              onClick={toggleShowDeleteModal}>
              <img src="/images/delete.svg" alt="delete_property" />
              Delete Property
            </button>
          )}
        </div>
      </div>
      <div className="my-property-view-wrapper">
        <MyPropertyView
          property={state.property}
          setProperty={setProperty}
          editInProgress={editInProgress}
          handleEdit={handleEditInProgress}
          toggleActiveListing={toggleActiveListing}
          show_publish_modal={state.show_publish_modal}
          closePublishModal={closePublishModal}
          windowSize={windowSize}
        />
      </div>
    </div>
  ) : (
    <div className="no-property-to-display">
      <span className="no-property-text">
        Choose Property to see all information about.
      </span>
    </div>
  );
}

export default MyPropertyInfo;

const modalWording = {
  title: 'Are you sure you want to delete this property?',
  text: `As an option you can remove it from active listing and it won't be shown for other users.`,
};
