import React, {useState} from 'react';
import './styles.scss';
import UploadMedia from '../UploadMedia';
import Modal from "react-modal";

function MyPropertyMedia({type, images, videos, links, returnMediaFiles, unitId}) {
  const [state, setState] = useState({
    modal_is_open: false,
    file_for_modal: {},
  });

  const returnMediaHandler = (media) => {
    returnMediaFiles(media, unitId);
  };

  return type === 'view' ? (
    <>
      <Modal
        isOpen={state.modal_is_open}
        style={customStyles}
        contentLabel="Example Modal">
        <button
          onClick={() => setState({...state, modal_is_open: false})}
          type="button"
          className="btn-link close-modal">
          <img src="/images/close.svg" />
        </button>
        {state.file_for_modal.type === 'image' && state.modal_is_open ? (
          <img className="full-media" src={state.file_for_modal.src} />
        ) : (
          <video autoPlay controls className="full-media">
            <source src={state.file_for_modal.src}></source>
          </video>
        )}
      </Modal>
      <div className="view-media">
        {images.map((img, key) => (
          <div key={key}>
            <img
              onClick={() =>
                setState({
                  ...state,
                  modal_is_open: true,
                  file_for_modal: {
                    type: 'image',
                    src: img,
                  },
                })
              }
              src={img}
            />
          </div>
        ))}
        {videos.map((video, key) => (
          <div key={key}>
            <video
              onClick={() =>
                setState({
                  ...state,
                  modal_is_open: true,
                  file_for_modal: {
                    type: 'video',
                    src: video,
                  },
                })
              }
              width={126}
              height={116}>
              <source src={video}></source>
            </video>
          </div>
        ))}
        {links.map((video, key) => (
          <div key={key}>
            <video
              onClick={() =>
                setState({
                  ...state,
                  modal_is_open: true,
                  file_for_modal: {
                    type: 'video',
                    src: video,
                  },
                })
              }
              width={126}
              height={116}>
              <source data-yt2html5={video}></source>
            </video>
          </div>
        ))}
        {images.length === 0 && videos.length === 0 && links.length === 0 && (
            <p className="no-data-added">
              You haven’t added any media yet.
            </p>
        )}
      </div>
    </>
  ) : (
    <UploadMedia
      photos={images}
      videos={videos}
      returnMediaFiles={returnMediaHandler}
    />
  );
}

export default MyPropertyMedia;

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    border: 0,
    boxShadow: '0px 4px 24px rgba(0, 0, 0, 0.07)',
    transform: 'translate(-50%, -50%)',
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
  },
};
