import React, {useEffect, useState} from 'react';
import './styles.scss';
import {useSelector, useDispatch} from 'react-redux';
import MyPropertyMedia from 'components/MyPropertyMedia';
import Amenities from 'components/PropertySections/Amenities';
import {
  createUnit,
  deleteUnit,
  publishDraft,
  updateProperty,
  updateUnit,
} from 'store/actions/myPropertiesActions';
import UnitsTable from 'components/UnitsTable';
import {
  formatPropertyAmenities,
  formatReqAmenitiesUtils,
  formatUnitAmenities,
  formatUtilities,
  handleUnitInputChangeHelper,
  initialUnit,
  setBedroomPriceHelper,
  setRentOptionHelper,
  toggleAmenityHelper,
  toggleSplitEvenlyHelper,
  toggleUtilitiesIncludedHelper,
  toggleUtilityHelper,
} from 'helpers/createProperty';
import {nanoid} from 'nanoid';
import {createFileForImages, createFileForVideos} from '../../helpers/media';
import {NotificationManager} from 'react-notifications';
import {handleLoader} from '../../store/actions/loaderActions';
import checkForm from '../../utilities/validator';
import ConfirmModal from '../ConfirmModal/confirmModal';
import {validateDraft} from '../../helpers/draftValidator';
import UnitsBlockMobile from '../UnitsBlockMobile';

function MyPropertyView({
  history,
  property,
  setProperty,
  editInProgress,
  handleEdit,
  toggleActiveListing,
  show_publish_modal,
  closePublishModal,
  windowSize,
}) {
  const property_amenities = useSelector(
    (state) => state.constants.property_amenities,
  );
  const unit_amenities = useSelector((state) => state.constants.unit_amenities);

  const dispatch = useDispatch();

  const [state, setState] = useState({
    property: {},
    type: 'view',
    edit_type: '',
  });

  useEffect(() => {
    const {edit_type, ...property_data} = JSON.parse(JSON.stringify(property));
    property_data.units = [...property_data.units].map((unit) => {
      const prev_unit_index = state.property.units
        ? state.property.units.findIndex(
            (prev_unit) => prev_unit._id === unit._id,
          )
        : -1;
      const init_unit_index = property.units.findIndex(
        (init_unit) => init_unit._id === unit._id,
      );
      const unitAmenities = formatUnitAmenities(
        unit,
        property.units[init_unit_index],
        unit_amenities,
      );
      return {
        ...unit,
        id: unit._id,
        expandUnitInfo:
          prev_unit_index >= 0
            ? state.property.units[prev_unit_index].expandUnitInfo
            : false,
        mode:
          prev_unit_index >= 0
            ? state.property.units[prev_unit_index].mode
            : 'view',
        unitAmenities,
      };
    });
    property_data.propertyAmenities = formatPropertyAmenities(
      state.type,
      property_data,
      property_amenities,
    );
    setState({
      ...state,
      property: property_data,
      edit_type,
    });
  }, [property]);

  useEffect(() => {
    if (
      !editInProgress &&
      state.type === 'edit' &&
      state.edit_type === 'drafts'
    ) {
    } else if (!editInProgress && state.type === 'edit') {
      savePropertyChanges();
    }
  }, [editInProgress]);

  const onSaveChanges = () => {
    handleEdit(false);
  };

  const savePropertyChanges = async () => {
    dispatch(handleLoader(true));
    const property_to_save = JSON.parse(JSON.stringify(state.property));
    const edit_type = state.edit_type;
    property_to_save.propertyAmenities = property_to_save.propertyAmenities
      .filter((amenity) => amenity.selected)
      .map((amenity) => amenity.name);
    const new_photos = property_to_save.photos.filter((photo) =>
      photo.includes('blob'),
    );
    const uploaded_photos =
      new_photos.length > 0 ? await createFileForImages(new_photos) : [];
    if (uploaded_photos) {
      const old_photos = property_to_save.photos.filter(
        (photo) => !photo.includes('blob'),
      );
      property_to_save.photos = uploaded_photos.data
        ? [...old_photos, ...uploaded_photos.data]
        : [...old_photos];
    } else {
      handleEdit(true);
      NotificationManager.error('Something went wrong.');
      dispatch(handleLoader(false));
      return;
    }
    setProperty(null);
    const is_updated = await dispatch(
      updateProperty(state.edit_type, property_to_save),
    );
    if (is_updated.status) {
      property_to_save.edit_type = edit_type;
      setProperty(property_to_save);
      NotificationManager.success(is_updated.message);
    } else {
      handleEdit(true);
      NotificationManager.error(is_updated.message);
    }
  };

  const handleChangeType = ({target}) => {
    handleEdit(true);
    const propertyAmenities = formatPropertyAmenities(
      target.name,
      property,
      property_amenities,
    );
    setState({
      ...state,
      type: target.name,
      property: {
        ...state.property,
        propertyAmenities,
      },
    });
  };

  const handleChangeUnitMode = (id) => {
    handleEdit(true);
    const units = JSON.parse(JSON.stringify(state.property.units));
    const unit_index = units.findIndex((unit) => unit._id === id);
    const init_index = property.units.findIndex((unit) => unit._id === id);
    units[unit_index].mode = 'edit';
    units[unit_index].unitAmenities = formatUnitAmenities(
      units[unit_index],
      property.units[init_index],
      unit_amenities,
    );
    units[unit_index].utilitiesList = formatUtilities(
      units[unit_index],
      property.units[init_index],
    );
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const handlePropertyInputChange = ({target}) => {
    setState({
      ...state,
      property: {
        ...state.property,
        [target.name]: target.value,
      },
    });
  };

  const handleUnitsInputChange = ({target}) => {
    const units = handleUnitInputChangeHelper(target, state.property.units);
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const toggleExpandUnitInfo = (unit_id) => {
    const units = JSON.parse(JSON.stringify(state.property.units));
    const unitIndex = units.findIndex((unit) => unit._id === unit_id);
    units[unitIndex].expandUnitInfo = !units[unitIndex].expandUnitInfo;
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const returnPropertyMediaFiles = async (media) => {
    if (media.images.length > 0) {
      setState({
        ...state,
        property: {
          ...state.property,
          photos: media.images,
        },
      });
    }
    if (media.videos.length > 0) {
      setState({
        ...state,
        property: {
          ...property,
          videos: media.videos,
        },
      });
    }
  };

  const returnUnitMediaFiles = async (media, unit_id) => {
    const units = [...state.property.units];
    const unitIndex = units.findIndex((unit) => unit._id === unit_id);
    if (media.images.length > 0) {
      units[unitIndex] = {
        ...units[unitIndex],
        photos: media.images,
      };
      setState({
        ...state,
        property: {
          ...state.property,
          units,
        },
      });
    }
    if (media.videos.length > 0) {
      units[unitIndex] = {
        ...units[unitIndex],
        videos: media.videos,
      };
      setState({
        ...state,
        property: {
          ...property,
          units,
        },
      });
    }
  };

  const togglePropertyAmenity = (amenity_id) => {
    const amenities = [...state.property.propertyAmenities];
    const updatedAmenity = amenities.find(
      (amenity) => amenity._id === amenity_id,
    );
    updatedAmenity.selected = !updatedAmenity.selected;
    setState({
      ...state,
      property: {
        ...state.property,
        propertyAmenities: amenities,
      },
    });
  };

  const toggleUtilitiesIncluded = (id) => {
    const units = toggleUtilitiesIncludedHelper(id, state.property.units);
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const toggleUtility = (name, id) => {
    const units = toggleUtilityHelper(name, id, state.property.units);
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const setRentOption = ({target}) => {
    const units = setRentOptionHelper(target, state.property.units);
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const toggleSplitEvenly = (id) => {
    const units = toggleSplitEvenlyHelper(id, state.property.units);
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const setBedroomPrice = ({target}) => {
    const units = setBedroomPriceHelper(target, state.property.units);
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const toggleAmenity = (item_id, unit_id) => {
    const units = toggleAmenityHelper(item_id, unit_id, state.property.units);
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const showMoreUnitHandler = (id) => {
    const units = [...state.property.units];
    const unit_index = state.property.units.findIndex(
      (unit) => unit._id === id,
    );
    units[unit_index].show_more = !state.property.units[unit_index].show_more;
    setState({
      ...state,
      property: {
        ...state.property,
        units,
      },
    });
  };

  const addUnitHandler = () => {
    const new_unit = JSON.parse(JSON.stringify(initialUnit));
    new_unit.id = nanoid();
    new_unit.expandUnitInfo = true;
    new_unit.mode = 'edit';
    new_unit.is_new_unit = true;
    setState({
      ...state,
      property: {
        ...state.property,
        units: [...state.property.units, new_unit],
      },
    });
  };

  const saveUnitChanges = async (e, unit_id) => {
    if (state.edit_type === 'properties' && !checkForm(e.target.form)) {
      NotificationManager.error('Please fill all mandatory fields');
      return;
    }
    const units = JSON.parse(JSON.stringify(state.property.units));
    const unit_index = units.findIndex((unit) => unit.id === unit_id);
    units[unit_index].unitAmenities = formatReqAmenitiesUtils(
      units[unit_index].unitAmenities,
    );
    units[unit_index].utilitiesList = formatReqAmenitiesUtils(
      units[unit_index].utilitiesList,
    );
    const new_photos = units[unit_index].photos.filter((photo) =>
      photo.includes('blob'),
    );
    const uploaded_photos =
      new_photos.length > 0 ? await createFileForImages(new_photos) : [];
    if (uploaded_photos) {
      const old_photos = units[unit_index].photos.filter(
        (photo) => !photo.includes('blob'),
      );
      units[unit_index].photos = uploaded_photos.data
        ? [...old_photos, ...uploaded_photos.data]
        : [...old_photos];
    } else {
      NotificationManager.error('Something went wrong.');
      return;
    }
    if (units[unit_index].is_new_unit) {
      const {is_new_unit, ...unit} = units[unit_index];
      const is_updated = await dispatch(
        createUnit(unit, state.property._id, state.edit_type),
      );
      if (is_updated.status) {
        units[unit_index]._id = is_updated._id;
        units[unit_index].id = is_updated._id;
        units[unit_index].mode = 'view';
        units[unit_index].unitAmenities = formatUnitAmenities(
          units[unit_index],
          units[unit_index],
          unit_amenities,
        );
        NotificationManager.success('Unit was created');
        handleEdit(false);
        setState({
          ...state,
          type: 'view',
          property: {
            ...state.property,
            units,
          },
        });
      } else {
        NotificationManager.error(is_updated.message);
        setState({
          ...state,
        });
      }
    } else {
      const is_updated = await dispatch(
        updateUnit(units[unit_index], state.property._id, state.edit_type),
      );
      if (is_updated.status) {
        NotificationManager.success('Unit changes saved');
        units[unit_index].mode = 'view';
        units[unit_index].unitAmenities = formatUnitAmenities(
          units[unit_index],
          units[unit_index],
          unit_amenities,
        );
        handleEdit(false);
        setState({
          ...state,
          property: {
            ...state.property,
            units,
          },
        });
      } else {
        NotificationManager.error(is_updated.message);
      }
    }
  };

  const deleteUnitHandler = async (unit_id) => {
    const units = [...state.property.units];
    const is_deleted = await dispatch(
      deleteUnit(unit_id, state.property._id, state.edit_type),
    );
    if (is_deleted.status) {
      const filtered_units = units.filter((unit) => unit._id !== unit_id);
      setState({
        ...state,
        property: {
          ...state.property,
          units: filtered_units,
        },
      });
      NotificationManager.success(is_deleted.message);
    } else {
      NotificationManager.error(is_deleted.message);
    }
  };

  const publishDraftHandler = async () => {
    if (validateDraft(state.property)) {
      closePublishModal();
      const published_draft = JSON.parse(JSON.stringify(state.property));
      published_draft.propertyAmenities = formatReqAmenitiesUtils(
        published_draft.propertyAmenities,
      );
      published_draft.units.forEach((unit) => {
        unit.unitAmenities = formatReqAmenitiesUtils(unit.unitAmenities);
      });
      const is_published = await dispatch(publishDraft(published_draft));
      if (is_published.status) {
        published_draft.edit_type = 'properties';
        setProperty(published_draft);
        NotificationManager.success(is_published.message);
      } else {
        NotificationManager.error(is_published.message);
      }
    } else {
      closePublishModal();
      NotificationManager.error('Please fill all mandatory fields');
    }
  };

  return (
    Object.keys(state.property).length > 0 && (
      <form className="my_property-form">
        <ConfirmModal
          modal_visible={show_publish_modal}
          title="Would you like to publish this draft?"
          confirmAction={publishDraftHandler}
          cancelAction={closePublishModal}
        />
        <div className="my-property-view">
          <div className="my-property-view__main-info">
            {state.type === 'view' ? (
              <>
                <h2 className="property-name">{state.property.propertyName}</h2>
                <button
                  type="button"
                  className="btn edit-btn"
                  name="edit"
                  onClick={handleChangeType}>
                  Edit Property Info
                </button>
              </>
            ) : (
              <>
                <div className="input property-name-input">
                  <label>Property name</label>
                  <input
                    type="text"
                    className="input-field"
                    name="propertyName"
                    id="propertyName"
                    value={state.property.propertyName}
                    onChange={handlePropertyInputChange}
                  />
                </div>
                <button
                  type="button"
                  className="btn save-btn"
                  name="view"
                  onClick={onSaveChanges}>
                  Save changes
                </button>
              </>
            )}
          </div>
          <div className="property-media">
            <p className="section-title media-title">Photos / Videos</p>
            <MyPropertyMedia
              type={state.type}
              images={state.property.photos}
              videos={state.property.videos}
              links={state.property.links}
              returnMediaFiles={returnPropertyMediaFiles}
            />
          </div>
          <div className="property-description">
            <p className="section-title description-title">
              Property description
            </p>
            {state.type === 'view' ? (
              state.property.propertyDescription !== '' ? (
                <p className="description-text">
                  {state.property.propertyDescription}
                </p>
              ) : (
                <p className="no-data-added">No description</p>
              )
            ) : (
              <textarea
                className="edit-description"
                name="propertyDescription"
                placeholder="Add your description here"
                required
                value={state.property.propertyDescription}
                onChange={handlePropertyInputChange}
              />
            )}
          </div>
          <div className="property-amenities">
            {state.property.propertyAmenities.length > 0 ? (
              <Amenities
                amenityType="Property"
                amenities={state.property.propertyAmenities}
                toggleAmenity={
                  state.type === 'edit' ? togglePropertyAmenity : () => {}
                }
                is_clickable={state.type === 'edit'}
              />
            ) : (
              <>
                <p className="section-title">Property Amenities</p>
                <p className="no-data-added">
                  You haven’t added any amenities to this property info yet.
                </p>
              </>
            )}
          </div>
        </div>
        <div className="units-view">
          {windowSize.width >= 768 ? (
            <UnitsTable
              editable={true}
              units={state.property.units}
              unitsType={state.property.unitsType}
              handleChangeUnitMode={handleChangeUnitMode}
              handleInputChange={handleUnitsInputChange}
              returnUnitMediaFiles={returnUnitMediaFiles}
              toggleExpandUnitInfo={toggleExpandUnitInfo}
              toggleActiveListing={toggleActiveListing}
              setBedroomPrice={setBedroomPrice}
              setRentOption={setRentOption}
              toggleAmenity={toggleAmenity}
              toggleSplitEvenly={toggleSplitEvenly}
              toggleUtilitiesIncluded={toggleUtilitiesIncluded}
              toggleUtility={toggleUtility}
              showMoreUnitHandler={showMoreUnitHandler}
              saveUnitChanges={saveUnitChanges}
              deleteUnit={deleteUnitHandler}
            />
          ) : (
            <UnitsBlockMobile
              editable={true}
              units={state.property.units}
              unitsType={state.property.unitsType}
              handleChangeUnitMode={handleChangeUnitMode}
              handleInputChange={handleUnitsInputChange}
              returnUnitMediaFiles={returnUnitMediaFiles}
              toggleExpandUnitInfo={toggleExpandUnitInfo}
              toggleActiveListing={toggleActiveListing}
              setBedroomPrice={setBedroomPrice}
              setRentOption={setRentOption}
              toggleAmenity={toggleAmenity}
              toggleSplitEvenly={toggleSplitEvenly}
              toggleUtilitiesIncluded={toggleUtilitiesIncluded}
              toggleUtility={toggleUtility}
              showMoreUnitHandler={showMoreUnitHandler}
              saveUnitChanges={saveUnitChanges}
              deleteUnit={deleteUnitHandler}
            />
          )}
          {state.property.unitsType === 'Multi' && (
            <button
              type="button"
              className="btn add-unit-btn"
              onClick={addUnitHandler}>
              Add unit
            </button>
          )}
        </div>
      </form>
    )
  );
}

export default MyPropertyView;
