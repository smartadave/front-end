import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import './styles.scss';
import history from 'history.js';
import {updateStep, updateUserApplication} from 'store/actions/personalApplicationActions';
import Selector from 'components/Selector';
import checkForm from 'utilities/validator';

function ApplicationInformation({create}) {
  const dispatch = useDispatch();
  const applicationInformation = useSelector(
    (state) => state.personalApplication.applicationInformation,
  );
  const personalApplication = useSelector(
    (state) => state.personalApplication
  );
  const user = useSelector((state) => state.user);
  const [state, setState] = useState({
    applicationInformation: {},
  });

  useEffect(() => {
    if (
      JSON.stringify(applicationInformation) !==
      JSON.stringify(state.applicationInformation)
    ) {
      setState({
        ...state,
        applicationInformation,
      });
    }
  }, [applicationInformation]);

  const renderQuestions = () => {
    return (
      state.applicationInformation.questions &&
      Object.keys(state.applicationInformation.questions[0]).map(
        (question, key) => {
          if (
            state.applicationInformation.questions[0][question].type ===
            'selector'
          ) {
            return (
              <Selector
                label={
                  state.applicationInformation.questions[0][question].question
                }
                name={question}
                key={key}
                options={states}
                value={
                  state.applicationInformation.questions[0][question].answer
                }
                handleInputChange={(e) => handleInputChange(e, 0)}
              />
            );
          } else {
            return (
              <div key={key} className="input name-block-input">
                <label htmlFor="last_name" className="input-label">
                  {state.applicationInformation.questions[0][question].question}
                </label>
                <input
                  type={
                    state.applicationInformation.questions[0][question].type
                  }
                  name={question}
                  id="last_name"
                  className="input-field"
                  required={
                    state.applicationInformation.questions[0][question].required
                  }
                  onChange={(e) => handleInputChange(e, 0)}
                  value={
                    state.applicationInformation.questions[0][question].answer
                  }
                />
              </div>
            );
          }
        },
      )
    );
  };

  const handleInputChange = (e, question_block) => {
    setState({
      ...state,
      applicationInformation: {
        ...state.applicationInformation,
        questions: {
          ...state.applicationInformation.questions,
          [question_block]: {
            ...state.applicationInformation.questions[question_block],
            [e.target.name]: {
              ...state.applicationInformation.questions[question_block][
                e.target.name
              ],
              answer: e.target.value,
            },
          },
        },
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      if(create) {
        dispatch(
          updateStep({
            step: 'applicationInformation',
            data: {...state.applicationInformation.questions},
          }),
        )
        history.push('./rentalHistory');
      }
      else {
        const data = personalApplication
        data.applicationInformation = state.applicationInformation
        dispatch(updateUserApplication(user._id, data))
      }
    }
  };

  return (
    <div className="application-information">
      {create && <h2 className="title">{state.applicationInformation.title}</h2>}

      <form
        className="application-information__form"
        name="application-information-data"
        noValidate="novalidate"
        onSubmit={onSubmit}
        autoComplete="off">
        {renderQuestions()}

        <div className="navigation">
          <button
            className="btn back-button"
            onClick={() => history.goBack()}
            type="button">
            &lt;&nbsp;&nbsp; Back
          </button>

          <button
            className="btn next-button"
            name="next"
            type="submit">
            {create ? 'Next \u00A0\u00A0>' : 'Save changes'}
          </button>
        </div>
      </form>
    </div>
  );
}

export default ApplicationInformation;

const states = [
  {value: 'Alabama', name: 'Alabama'},
  {value: 'Alaska', name: 'Alaska'},
  {value: 'Arizona', name: 'Arizona'},
  {value: 'Arkansas', name: 'Arkansas'},
  {value: 'California', name: 'California'},
  {value: 'Colorado', name: 'Colorado'},
  {value: 'Connecticut', name: 'Connecticut'},
  {value: 'Delaware', name: 'Delaware'},
  {value: 'Florida', name: 'Florida'},
  {value: 'Georgia', name: 'Georgia'},
  {value: 'Hawaii', name: 'Hawaii'},
  {value: 'Idaho', name: 'Idaho'},
  {value: 'Illinois', name: 'Illinois'},
  {value: 'Indiana', name: 'Indiana'},
  {value: 'Iowa', name: 'Iowa'},
  {value: 'Kansas', name: 'Kansas'},
  {value: 'Kentucky', name: 'Kentucky'},
  {value: 'Louisiana', name: 'Louisiana'},
  {value: 'Maine', name: 'Maine'},
  {value: 'Maryland', name: 'Maryland'},
  {value: 'Massachusetts', name: 'Massachusetts'},
  {value: 'Michigan', name: 'Michigan'},
  {value: 'Minnesota', name: 'Minnesota'},
  {value: 'Mississippi', name: 'Mississippi'},
  {value: 'Missouri', name: 'Missouri'},
  {value: 'Montana', name: 'Montana'},
  {value: 'Nebraska', name: 'Nebraska'},
  {value: 'Nevada', name: 'Nevada'},
  {value: 'New Hampshire', name: 'New Hampshire'},
  {value: 'New Jersey', name: 'New Jersey'},
  {value: 'New Mexico', name: 'New Mexico'},
  {value: 'New York', name: 'New York'},
  {value: 'North Carolina', name: 'North Carolina'},
  {value: 'North Dakota', name: 'North Dakota'},
  {value: 'Ohio', name: 'Ohio'},
  {value: 'Oklahoma', name: 'Oklahoma'},
  {value: 'Oregon', name: 'Oregon'},
  {value: 'Pennsylvania', name: 'Pennsylvania'},
  {value: 'Rhode Island', name: 'Rhode Island'},
  {value: 'South Carolina', name: 'South Carolina'},
  {value: 'South Dakota', name: 'South Dakota'},
  {value: 'Tennessee', name: 'Tennessee'},
  {value: 'Texas', name: 'Texas'},
  {value: 'Utah', name: 'Utah'},
  {value: 'Vermont', name: 'Vermont'},
  {value: 'Virginia', name: 'Virginia'},
  {value: 'Washington', name: 'Washington'},
  {value: 'West Virginia', name: 'West Virginia'},
  {value: 'Wisconsin', name: 'Wisconsin'},
  {value: 'Wyoming', name: 'Wyoming'},
];
