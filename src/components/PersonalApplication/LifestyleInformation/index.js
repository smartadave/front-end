import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import history from 'history.js';
import {
  updateStep,
  updateUserApplication,
} from 'store/actions/personalApplicationActions';
import './styles.scss';
import BooleanCheckbox from 'components/BooleanCheckbox';
import checkForm from 'utilities/validator';
import Selector from 'components/Selector';

function LifestyleInformation({create}) {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const personalApplication = useSelector((state) => state.personalApplication);
  const lifestyleInformation = useSelector(
    (state) => state.personalApplication.lifestyleInformation,
  );
  const [state, setState] = useState({
    lifestyleInformation: {},
  });

  useEffect(() => {
    if (
      JSON.stringify(lifestyleInformation) !==
      JSON.stringify(state.lifestyleInformation)
    ) {
      setState({
        ...state,
        lifestyleInformation,
      });
    }
  }, [lifestyleInformation]);

  const renderQuestions = (data, question_block) => {
    return Object.keys(data).map((question, key) => {
      if (data[question].type === 'boolean') {
        return (
          <BooleanCheckbox
            name={question}
            value={data[question].answer}
            handleInputChange={(e) => handleInputChange(e, question_block)}
            title={data[question].question}
          />
        );
      } else if (data[question].type === 'selector') {
        return (
          <div style={{maxWidth: '380px'}}>
            <Selector
              label={data[question].question}
              name={question}
              key={key}
              options={pets_types}
              value={data[question].answer}
              handleInputChange={(e) => handleInputChange(e, question_block)}
            />
          </div>
        );
      } else {
        return (
          <div className="input name-block-input">
            <label htmlFor="last_name" className="input-label">
              {data[question].question}
            </label>
            <input
              type={data[question].type}
              name={question}
              required={data[question].required}
              className="input-field"
              required
              onChange={(e) => handleInputChange(e, question_block)}
              value={data[question].answer}
            />
          </div>
        );
      }
    });
  };

  const handleInputChange = (e, question_block) => {
    setState({
      ...state,
      lifestyleInformation: {
        ...state.lifestyleInformation,
        questions: {
          ...state.lifestyleInformation.questions,
          [question_block]: {
            ...state.lifestyleInformation.questions[question_block],
            [e.target.name]: {
              ...state.lifestyleInformation.questions[question_block][
                e.target.name
              ],
              answer: e.target.value,
            },
          },
        },
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      if (create) {
        dispatch(
          updateStep({
            step: 'lifestyleInformation',
            data: {...state.lifestyleInformation.questions},
          }),
        );
        history.push('./otherInformation');
      } else {
        const data = personalApplication;
        data.lifestyleInformation = state.lifestyleInformation;
        dispatch(updateUserApplication(user._id, data));
      }
    }
  };

  return (
    <div className="lifestyle-information">
      {create && <h2 className="title">{state.lifestyleInformation.title}</h2>}

      <form
        className="lifestyle-information__form"
        name="lifestyle-information-data"
        noValidate="novalidate"
        onSubmit={onSubmit}
        autoComplete="off">
        {state.lifestyleInformation.questions &&
          renderQuestions(state.lifestyleInformation.questions[0], 0)}

        <div className="navigation">
          <button
            className="btn back-button"
            onClick={() => history.goBack()}
            type="button">
            &lt;&nbsp;&nbsp; Back
          </button>

          <button
            onClick={onSubmit}
            className="btn next-button"
            name="next"
            type="submit">
            {create ? 'Next \u00A0\u00A0>' : 'Save changes'}
          </button>
        </div>
      </form>
    </div>
  );
}

export default LifestyleInformation;

const pets_types = [
  {value: 'Cats', name: 'Cats'},
  {value: 'Dogs', name: 'Dogs'},
  {value: 'Other', name: 'Other'},
];
