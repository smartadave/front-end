import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import './styles.scss';
import history from 'history.js';
import {updateStep, updateUserApplication} from 'store/actions/personalApplicationActions';
import checkForm from 'utilities/validator';
import BooleanCheckbox from 'components/BooleanCheckbox';

function OtherInformation({create}) {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const personalApplication = useSelector(
    (state) => state.personalApplication
  );
  const otherInformation = useSelector(
    (state) => state.personalApplication.otherInformation,
  );
  const [state, setState] = useState({
    otherInformation: {},
  });

  useEffect(() => {
    if (
      JSON.stringify(otherInformation) !==
      JSON.stringify(state.otherInformation)
    ) {
      setState({
        ...state,
        otherInformation,
      });
    }
  }, [otherInformation]);

  const renderQuestions = (data, question_block) => {
    return Object.keys(data).map((question, key) => {
      if (data[question].type === 'boolean') {
        return (
          <div style={{marginBottom: '14px'}}>
            <BooleanCheckbox
              name={question}
              value={data[question].answer}
              handleInputChange={(e) =>
                handleInputChange(e, question_block, 'answer')
              }
              title={data[question].question}
            />
            <div className="input name-block-input">
              <label
                style={{fontFamily: 'Montserrat-Regular'}}
                htmlFor="last_name"
                className="input-label">
                Explain the reason
              </label>
              <input
                name={question}
                type="text"
                className="input-field"
                onChange={(e) =>
                  handleInputChange(e, question_block, 'long_answer')
                }
                value={data[question].long_answer}
              />
            </div>
          </div>
        );
      }
    });
  };

  const handleInputChange = (e, question_block, type) => {
    setState({
      ...state,
      otherInformation: {
        ...state.otherInformation,
        questions: {
          ...state.otherInformation.questions,
          [question_block]: {
            ...state.otherInformation.questions[question_block],
            [e.target.name]: {
              ...state.otherInformation.questions[question_block][
                e.target.name
              ],
              [type]: e.target.value,
            },
          },
        },
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      if (create) {
        dispatch(
          updateStep({
            step: 'otherInformation',
            data: {...state.otherInformation.questions},
          }),
        );
        history.push('./applicationPreview');
      } else {
        const data = personalApplication;
        data.otherInformation = state.otherInformation;
        dispatch(updateUserApplication(user._id, data));
      }
    }
  };

  return (
    <div className="other-information">
      {create && <h2 className="title">{state.otherInformation.title}</h2>}

      <form
        className="other-information__form"
        name="other-information-data"
        noValidate="novalidate"
        onSubmit={onSubmit}
        autoComplete="off">
        <div>
          {state.otherInformation.questions &&
            renderQuestions(state.otherInformation.questions[0], 0)}
        </div>
        <div className="navigation">
          <button
            className="btn back-button"
            onClick={() => history.goBack()}
            type="button">
            &lt;&nbsp;&nbsp; Back
          </button>

          <button className="btn next-button" name="next" type="submit">
          {create ? 'Next \u00A0\u00A0>' : 'Save changes'}
          </button>
        </div>
      </form>
    </div>
  );
}

export default OtherInformation;
