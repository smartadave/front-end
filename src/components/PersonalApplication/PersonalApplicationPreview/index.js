import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import './styles.scss';
import history from 'history.js';
import {createUserApplication} from 'store/actions/personalApplicationActions';

function PersonalApplicationPreview() {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const personalApplication = useSelector((state) => state.personalApplication);

  const renderPersonalApplication = () => {
    return Object.keys(personalApplication).map((item, key) => {
      if (item !== 'status') {
        return (
          <div key={key}>
            <h2 className="title">{personalApplication[item].title}</h2>
            {item === 'applicationInformation'
              ? returnPersonalInfo(personalApplication[item].questions[0])
              : returnOtherInfo(personalApplication[item].questions)}
          </div>
        );
      }
    });
  };

  const returnPersonalInfo = (data) => {
    return (
      <div style={{marginBottom: '36px'}}>
        {Object.keys(data).map((item, key) => {
          return (
            <div
              key={key}
              style={{
                display: 'flex',
                width: '460px',
              }}>
              <span style={{width: '50%'}} className="question">
                {data[item].question}:
              </span>
              <span className="answer">{data[item].answer}</span>
            </div>
          );
        })}
      </div>
    );
  };

  const returnOtherInfo = (data) => {
    return (
      <div style={{marginBottom: '36px'}}>
        {Object.keys(data[0]).map((item, key) => {
          return (
            <div
              key={key}
              style={{
                width: '460px',
                marginBottom: '8px',
                display: 'flex',
                flexDirection: 'column',
              }}>
              <span className="question">{data[0][item].question}:</span>

              <span className="answer">
                {typeof data[0][item].answer === 'boolean'
                  ? data[0][item].answer
                    ? 'Yes'
                    : 'No'
                  : data[0][item].answer}
              </span>
              {data[0][item].long_answer && (
                <span className="answer">{data[0][item].long_answer}</span>
              )}
            </div>
          );
        })}
        {data[1] &&
          Object.keys(data[1]).map((item, key) => {
            return (
              <div
                key={key}
                style={{
                  width: '460px',
                  marginBottom: '8px',
                  display: 'flex',
                  flexDirection: 'column',
                }}>
                <span className="question">{data[1][item].question}:</span>
                <span className="answer">{data[1][item].answer}</span>
              </div>
            );
          })}
      </div>
    );
  };

  const sendApplication = () => {
    dispatch(createUserApplication(user._id, personalApplication));
  };

  return (
    <div className="personal-application-preview">
      {renderPersonalApplication()}
      <div className="navigation">
        <button
          className="btn back-button"
          onClick={() => history.goBack()}
          type="button">
          &lt;&nbsp;&nbsp; Back
        </button>

        <button
          onClick={sendApplication}
          className="btn next-button"
          name="next"
          type="submit">
          {'Send Application'}
        </button>
      </div>
    </div>
  );
}

export default PersonalApplicationPreview;
