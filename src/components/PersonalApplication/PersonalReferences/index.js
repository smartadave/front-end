import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import './styles.scss';
import history from 'history.js';
import {
  updateStep,
  updateUserApplication,
} from 'store/actions/personalApplicationActions';
import checkForm from 'utilities/validator';

function PersonalReferences({create}) {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const personalApplication = useSelector((state) => state.personalApplication);
  const personalReferences = useSelector(
    (state) => state.personalApplication.personalReferences,
  );
  const [state, setState] = useState({
    personalReferences: {},
  });

  useEffect(() => {
    if (
      JSON.stringify(personalReferences) !==
      JSON.stringify(state.personalReferences)
    ) {
      setState({
        ...state,
        personalReferences,
      });
    }
  }, [personalReferences]);

  const renderQuestions = (data, question_block) => {
    return Object.keys(data).map((question, key) => {
      return (
        <div className="input name-block-input">
          <label htmlFor="last_name" className="input-label">
            {data[question].question}
          </label>
          <input
            type={data[question].type}
            name={question}
            className="input-field"
            required={data[question].required}
            onChange={(e) => handleInputChange(e, question_block)}
            value={data[question].answer}
          />
        </div>
      );
    });
  };

  const handleInputChange = (e, question_block) => {
    setState({
      ...state,
      personalReferences: {
        ...state.personalReferences,
        questions: {
          ...state.personalReferences.questions,
          [question_block]: {
            ...state.personalReferences.questions[question_block],
            [e.target.name]: {
              ...state.personalReferences.questions[question_block][
                e.target.name
              ],
              answer: e.target.value,
            },
          },
        },
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      if (create) {
        dispatch(
          updateStep({
            step: 'personalReferences',
            data: {...state.personalReferences.questions},
          }),
        );
        history.push('./lifestyleInformation');
      } else {
        const data = personalApplication;
        data.personalReferences = state.personalReferences;
        dispatch(updateUserApplication(user._id, data));
      }
    }
  };

  return (
    <div className="personal-references">
      {create && <h2 className="title">{state.personalReferences.title}</h2>}

      <form
        className="personal-references__form"
        name="personal-references-data"
        noValidate="novalidate"
        onSubmit={onSubmit}
        autoComplete="off">
        <div style={{flexDirection: 'row'}}>
          <div>
            {state.personalReferences.questions &&
              renderQuestions(state.personalReferences.questions[0], 0)}
          </div>
          <div className="line"></div>
          <div>
            {state.personalReferences.questions &&
              renderQuestions(state.personalReferences.questions[1], 1)}
          </div>
        </div>
        <div className="navigation">
          <button
            className="btn back-button"
            onClick={() => history.goBack()}
            type="button">
            &lt;&nbsp;&nbsp; Back
          </button>

          <button className="btn next-button" name="next" type="submit">
            {create ? 'Next \u00A0\u00A0>' : 'Save changes'}
          </button>
        </div>
      </form>
    </div>
  );
}

export default PersonalReferences;
