import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import BooleanCheckbox from 'components/BooleanCheckbox';
import './styles.scss';
import history from 'history.js';
import {
  updateStep,
  updateUserApplication,
} from 'store/actions/personalApplicationActions';
import checkForm from 'utilities/validator';

function RentalHistory({create}) {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const personalApplication = useSelector((state) => state.personalApplication);
  const rentalHistory = useSelector(
    (state) => state.personalApplication.rentalHistory,
  );
  const [state, setState] = useState({
    rentalHistory: {},
    questions_checkbox: {
      0: false,
      1: false,
    },
  });

  useEffect(() => {
    if (JSON.stringify(rentalHistory) !== JSON.stringify(state.rentalHistory)) {
      setState({
        ...state,
        rentalHistory,
      });
    }
  }, [rentalHistory]);

  const renderQuestions = (data, question_block) => {
    return Object.keys(data).map((question, key) => {
      if (data[question].type === 'boolean') {
        return (
          <BooleanCheckbox
            value={data[question].answer}
            name={question}
            handleInputChange={(e) => handleInputChange(e, question_block)}
            title={data[question].question}
          />
        );
      } else {
        return (
          <div className="input name-block-input">
            <label htmlFor="last_name" className="input-label">
              {data[question].question}
            </label>
            <input
              type={data[question].type}
              required={state.questions_checkbox[question_block]}
              name={question}
              className="input-field"
              onChange={(e) => handleInputChange(e, question_block)}
              value={data[question].answer}
            />
          </div>
        );
      }
    });
  };

  const handleInputChange = (e, question_block) => {
    setState({
      ...state,
      questions_checkbox: {
        ...state.questions_checkbox,
        [question_block]: e.target.value,
      },
      rentalHistory: {
        ...state.rentalHistory,
        questions: {
          ...state.rentalHistory.questions,
          [question_block]: {
            ...state.rentalHistory.questions[question_block],
            [e.target.name]: {
              ...state.rentalHistory.questions[question_block][e.target.name],
              answer: e.target.value,
            },
          },
        },
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      if (create) {
        dispatch(
          updateStep({
            step: 'rentalHistory',
            data: {...state.rentalHistory.questions},
          }),
        );
        history.push('./workHistory');
      } else {
        const data = personalApplication;
        data.rentalHistory = state.rentalHistory;
        dispatch(updateUserApplication(user._id, data));
      }
    }
  };

  return (
    <div className="rental-history">
      {create && <h2 className="title">{state.rentalHistory.title}</h2>}

      <form
        className="rental-history__form"
        name="rental-history-data"
        noValidate="novalidate"
        onSubmit={onSubmit}
        autoComplete="off">
        <div style={{flexDirection: 'row'}}>
          <div>
            {state.rentalHistory.questions &&
              renderQuestions(state.rentalHistory.questions[0], 0)}
          </div>
          <div className="line"></div>
          <div>
            {state.rentalHistory.questions &&
              renderQuestions(state.rentalHistory.questions[1], 1)}
          </div>
        </div>

        <div className="navigation">
          <button
            className="btn back-button"
            onClick={() => history.goBack()}
            type="button">
            &lt;&nbsp;&nbsp; Back
          </button>

          <button className="btn next-button" name="next" type="submit">
            {create ? 'Next \u00A0\u00A0>' : 'Save changes'}
          </button>
        </div>
      </form>
    </div>
  );
}

export default RentalHistory;
