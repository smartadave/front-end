import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import './styles.scss';
import history from 'history.js';
import {
  updateStep,
  updateUserApplication,
} from 'store/actions/personalApplicationActions';
import BooleanCheckbox from 'components/BooleanCheckbox';
import checkForm from 'utilities/validator';
import Selector from 'components/Selector';

function WorkHistory({create}) {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const personalApplication = useSelector((state) => state.personalApplication);
  const workHistory = useSelector(
    (state) => state.personalApplication.workHistory,
  );
  const [state, setState] = useState({
    workHistory: {},
    questions_checkbox: {
      0: false,
      1: false,
    },
  });

  useEffect(() => {
    if (JSON.stringify(workHistory) !== JSON.stringify(state.workHistory)) {
      setState({
        ...state,
        workHistory,
      });
    }
  }, [workHistory]);

  const renderQuestions = (data, question_block) => {
    return Object.keys(data).map((question, key) => {
      if (data[question].type === 'boolean') {
        return (
          <BooleanCheckbox
            value={data[question].answer}
            name={question}
            handleInputChange={(e) => handleInputChange(e, question_block)}
            title={data[question].question}
          />
        );
      } else if (data[question].type === 'selector') {
        return (
          <div style={{maxWidth: '380px'}}>
            <Selector
              label={data[question].question}
              name={question}
              key={key}
              options={pay_types}
              value={data[question].answer}
              handleInputChange={(e) => handleInputChange(e, 2)}
            />
          </div>
        );
      } else if (data[question].type === 'textarea') {
        return (
          <div style={{maxWidth: '100%'}} className="input name-block-input">
            <label htmlFor="last_name" className="input-label">
              {data[question].question}
            </label>
            <textarea
              type={data[question].type}
              name={question}
              className="input-field"
              required={state.questions_checkbox[question_block]}
              onChange={(e) => handleInputChange(e, question_block)}
              value={data[question].answer}
            />
          </div>
        );
      } else {
        return (
          <div className="input name-block-input">
            <label htmlFor="last_name" className="input-label">
              {data[question].question}
            </label>
            <input
              type={data[question].type}
              name={question}
              className="input-field"
              required={state.questions_checkbox[question_block]}
              onChange={(e) => handleInputChange(e, question_block)}
              value={data[question].answer}
            />
          </div>
        );
      }
    });
  };

  const handleInputChange = (e, question_block) => {
    setState({
      ...state,
      questions_checkbox: {
        ...state.questions_checkbox,
        [question_block]: e.target.value,
      },
      workHistory: {
        ...state.workHistory,
        questions: {
          ...state.workHistory.questions,
          [question_block]: {
            ...state.workHistory.questions[question_block],
            [e.target.name]: {
              ...state.workHistory.questions[question_block][e.target.name],
              answer: e.target.value,
            },
          },
        },
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      if (create) {
        dispatch(
          updateStep({
            step: 'workHistory',
            data: {...state.workHistory.questions},
          }),
        );
        history.push('./personalReferences');
      } else {
        const data = personalApplication;
        data.workHistory = state.workHistory;
        dispatch(updateUserApplication(user._id, data));
      }
    }
  };

  return (
    <div className="work-history">
      {create && <h2 className="title">{state.workHistory.title}</h2>}

      <form
        className="work-history__form"
        name="work-history-data"
        noValidate="novalidate"
        onSubmit={onSubmit}
        autoComplete="off">
        <div style={{flexDirection: 'row'}}>
          <div>
            {state.workHistory.questions &&
              renderQuestions(state.workHistory.questions[0], 0)}
          </div>
          <div className="line"></div>
          <div>
            {state.workHistory.questions &&
              renderQuestions(state.workHistory.questions[1], 1)}
          </div>
        </div>

        <div style={{width: '100%'}}>
          {state.workHistory.questions &&
            renderQuestions(state.workHistory.questions[2], 2)}
        </div>

        <div className="navigation">
          <button
            className="btn back-button"
            onClick={() => history.goBack()}
            type="button">
            &lt;&nbsp;&nbsp; Back
          </button>

          <button className="btn next-button" name="next" type="submit">
            {create ? 'Next \u00A0\u00A0>' : 'Save changes'}
          </button>
        </div>
      </form>
    </div>
  );
}

export default WorkHistory;

const pay_types = [
  {value: 'Work', name: 'Work'},
  {value: 'Parents', name: 'Parents'},
  {value: 'Loan', name: 'Loan'},
  {value: 'Scholarship', name: 'Scholarship'},
  {value: 'Savings', name: 'Savings'},
  {value: 'Other', name: 'Other'},
];
