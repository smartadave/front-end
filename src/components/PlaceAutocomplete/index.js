import React, {useState, useEffect, useRef} from 'react';
import './styles.scss';

let autoComplete;

const loadScript = (url, callback) => {
  let ready_script = document.getElementById('placeAutoComplete');

  let script = ready_script ? ready_script : document.createElement('script');

  if (!ready_script) {
    script.type = 'text/javascript';
    script.id = 'placeAutoComplete';
  }

  if (script.readyState) {
    script.onreadystatechange = function () {
      if (script.readyState === 'loaded' || script.readyState === 'complete') {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else {
    script.onload = () => callback();
  }

  script.src = url;
  document.getElementsByTagName('head')[0].appendChild(script);
};

function handleScriptLoad(updateQuery, autoCompleteRef) {
  autoComplete = new window.google.maps.places.Autocomplete(
    autoCompleteRef.current,
    {types: ['address'], componentRestrictions: {country: 'us'}},
  );
  autoComplete.setFields(['formatted_address', 'geometry', 'name']);
  autoComplete.addListener('place_changed', () =>
    handlePlaceSelect(updateQuery),
  );
}

async function handlePlaceSelect(updateQuery) {
  const addressObject = autoComplete.getPlace();
  const query = addressObject.formatted_address;
  updateQuery(addressObject);
}

function PlaceAutocomplete({
  required,
  placeholder,
  name,
  returnAddressObject,
  value,
  disabled,
}) {
  const [query, setQuery] = useState('');
  const autoCompleteRef = useRef(null);
  const [placeObj, setPlaceOjb] = useState(null);

  useEffect(() => {
    loadScript(
      `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_PLACE_API_KEY}&libraries=places`,
      () => handleScriptLoad(setPlaceAndQuery, autoCompleteRef),
    );
  }, []);

  useEffect(() => {
    if (value.formatted_address && value.geometry) {
      setQuery(value.formatted_address);
    }
  }, [value]);

  useEffect(() => {
    if (placeObj) {
      // placeObj.geometry.location.lat = placeObj.geometry.location.lat()
      // placeObj.geometry.location.lng = placeObj.geometry.location.lng()
      returnAddressObject(placeObj);
    }
  }, [placeObj]);

  const setPlaceAndQuery = async (data) => {
    setQuery(data.formatted_address);
    let lat = await data.geometry.location.lat();
    let lng = await data.geometry.location.lng();
    data.geometry.location = {
      lat,
      lng,
    };
    setPlaceOjb(data);
  };

  const enterAddress = (e) => {
    !placeObj && setPlaceOjb(null);
    setQuery(e.target.value);
  };

  return (
    <div className="input property-name-input">
      <label htmlFor="address" className="input-label">
        Address
      </label>
      <input
        type="text"
        disabled={disabled}
        ref={autoCompleteRef}
        required={required}
        name={name}
        placeholder={placeholder}
        id="address"
        className="input-field"
        onChange={(event) => enterAddress(event)}
        value={query}
      />
    </div>
  );
}

export default PlaceAutocomplete;
