import React, {useRef, useState} from 'react';
import {Marker, InfoWindow} from '@react-google-maps/api';
import {useOnClickOutside} from 'hooks/clickOutside';
import PropertyCard from 'components/PropertyCard';
import {getMapCardInfo} from 'store/actions/propertyListActions';

function Point({icon, property}) {
  const [state, setState] = useState({
    openInfo: false,
    propertyInfo: {},
  });

  const ref = useRef(null);

  useOnClickOutside(ref, () => setState({...state, openInfo: false}));

  const openInfoHandler = async (id) => {
    let propertyInfo = {};
    let fetchError = false;
    try {
      propertyInfo = await getMapCardInfo(id);
    } catch (error) {
      fetchError = true;
    } finally {
      setState({
        ...state,
        propertyInfo,
        fetchError,
        openInfo: true,
      });
    }
  };

  return (
    <Marker
      className="property-point"
      icon={icon}
      position={property.coordinates}
      onClick={() => openInfoHandler(property._id)}>
      {state.openInfo && (
        <InfoWindow>
          <div ref={ref}>
            {state.propertyInfo._id && <PropertyCard property={state.propertyInfo} mapCard={true} />}
          </div>
        </InfoWindow>
      )}
    </Marker>
  );
}

export default Point;
