import React, {useState, useEffect} from 'react';
import './styles.scss';
import {useDispatch} from 'react-redux';
import {Prompt} from 'react-router';
import Modal from 'react-modal';
import {clearStore} from 'store/actions/propertyActions';

function PromptModal({when, navigate, shouldBlockNavigation, text}) {
  const [modalVisible, setModalVisible] = useState(false);
  const [lastLocation, setLastLocation] = useState(null);
  const [confirmedNavigation, setConfirmedNavigation] = useState(false);
  const dispatch = useDispatch();
  const closeModal = () => {
    setModalVisible(false);
  };

  useEffect(() => {
    const is_saving_draft = JSON.parse(sessionStorage.getItem('draft'));
    if (is_saving_draft) {
      sessionStorage.removeItem('draft');
      handleConfirmNavigationClick();
    }
  }, [shouldBlockNavigation]);
  const handleBlockedNavigation = (nextLocation) => {
    if (!confirmedNavigation && shouldBlockNavigation(nextLocation)) {
      setModalVisible(true);
      setLastLocation(nextLocation);
      return false;
    }
    return true;
  };
  const handleConfirmNavigationClick = () => {
    setModalVisible(false);
    setConfirmedNavigation(true);
  };
  useEffect(() => {
    if (confirmedNavigation && lastLocation) {
      // Navigate to the previous blocked location with your navigate function
      dispatch(clearStore());
      navigate(lastLocation.pathname);
    }
  }, [confirmedNavigation, lastLocation]);
  return (
    <>
      <Prompt when={when} message={handleBlockedNavigation} />
      <Modal
        isOpen={modalVisible}
        style={{
          overlay: {
            zIndex: 100,
          },
        }}
        onRequestClose={closeModal}
        className="modal leave-confirm-modal">
        <h3 className="title">If you leave all your data will be lost</h3>
        <div className="info-text">
          {/*<p>You can save property as a draft and continue later</p>*/}
          {text && <p>{text}</p>}
          <p>Would you like to leave?</p>
        </div>
        <div className="action-buttons">
          <button
            className="btn confirm-btn"
            onClick={handleConfirmNavigationClick}>
            Leave
          </button>
          <button className="btn cancel-btn" onClick={closeModal}>
            Cancel
          </button>
        </div>
      </Modal>
    </>
  );
}

export default PromptModal;
