import React, {useState, useEffect} from 'react';
import './styles.scss';
import ToolTip from 'components/Tooltip';
import {useDispatch, useSelector} from 'react-redux';
import {handleFavoritesProperty} from 'store/actions/userActions';
import {isFavoriteProperty} from 'helpers/property';
import {createChatWithUsers} from 'store/actions/conversationActions';

function PropertyCard({property, mapCard, setMapCenter}) {
  const auth_user = useSelector((state) => state.auth.user);
  const user = useSelector((state) => state.user);
  const filter = useSelector((state) => state.propertyList.filter);

  const [state, setState] = useState({
    like: false,
    likeHover: false,
    property: {},
  });

  const dispatch = useDispatch();

  useEffect(() => {
    if (JSON.stringify(property) !== JSON.stringify(state.property)) {
      setState({
        ...state,
        property: property,
      });
    }
  }, [JSON.stringify(property)]);

  useEffect(() => {
    if (user.favouriteProperties) {
      setState({
        ...state,
        property: property,
        like: isFavoriteProperty(user.favouriteProperties, property._id),
      });
    }
  }, [user.favouriteProperties]);

  const bedroomsNumber =
    property.bedroomsRange[0] === property.bedroomsRange[1]
      ? property.bedroomsRange[0]
      : property.bedroomsRange.join('-');
  const bathroomsNumber =
    property.bathroomsRange[0] === property.bathroomsRange[1]
      ? property.bathroomsRange[0]
      : property.bathroomsRange.join('-');
  const price = property.priceMin
    ? `From $ ${property.priceMin}`
    : `$ ${property.price}`;

  const toggleLike = async (type) => {
    dispatch(
      handleFavoritesProperty(state.property._id, type, state.property),
    ).then((resp) => {
      if (resp) {
        setState({
          ...state,
          like: type === 'remove' ? false : true,
        });
      }
    });
  };

  const toggleHover = () => {
    setState({
      ...state,
      likeHover: !state.likeHover,
    });
  };

  const showMore = (e) => {
    e.stopPropagation();
    if (e.target.className !== 'save-to-fav-button') {
      sessionStorage.setItem('filter', JSON.stringify(filter));
      window.open(`property?id=${state.property._id}`, '_blank');
    }
  };

  const sendMessage = (e) => {
    e.stopPropagation();
    const data = {
        name: "Conversation",
        description: "Chat",
        is_user: true,
        is_private: true,
        users: [
          property.user_id, auth_user.id
        ],
        messages: []
    }
    dispatch(createChatWithUsers(data))
  };

  const showOnMap = (e) => {
    e.stopPropagation();
    const newPlace = {
      id: state.property._id,
      coordinates: {
        ...state.property.address.geometry.location,
      },
    };
    setMapCenter(newPlace);
  };

  return (
    <>
      {state.property._id && (
        <div
          className={`property-card${mapCard ? ' map-card' : ' list-card'}`}
          onClick={showMore}>
          <div className="card-header">
            <div className="main-info">
              <h4>{state.property.propertyName}</h4>
              <p>{state.property.address.formatted_address}</p>
            </div>
            {auth_user.role === 'student' && (
              <div className="actions">
                <ToolTip
                  content={`${
                    !state.like ? 'Add to' : 'Remove from'
                  } favourites`}>
                  <img
                    onClick={() => toggleLike(state.like ? 'remove' : 'add')}
                    className="save-to-fav-button"
                    src={
                      state.like
                        ? '/images/heart.svg'
                        : state.likeHover
                        ? '/images/heart.svg'
                        : '/images/unheart.svg'
                    }
                    onMouseEnter={toggleHover}
                    onMouseLeave={toggleHover}
                  />
                </ToolTip>
              </div>
            )}
          </div>
          <div className="card-content">
            <div className="card-image">
              <img
                src={
                  state.property.photo
                    ? state.property.photo
                    : 'https://q4g9y5a8.rocketcdn.me/wp-content/uploads/2020/02/home-banner-2020-02-min.jpg'
                }
              />
              <span className="photos-count">
                {state.property.photosLength} photos
              </span>
            </div>
            <div className="property-info">
              <span>{state.property.date}</span>
              <p className="price">{price}</p>
              <p className="rooms">
                {bedroomsNumber} bedrooms, {bathroomsNumber} bathrooms
              </p>
              {state.property.propertyAmenities.length > 0 && (
                <span className="amenities">
                  {state.property.propertyAmenities.join(', ')}
                </span>
              )}
              {auth_user.role === 'student' && (
                <button className="btn send-message-btn" onClick={sendMessage}>
                  Send Message
                </button>
              )}
            </div>
          </div>
          {!mapCard && (
            <button
              className="btn-link show-on-map-btn"
              type="button"
              id={state.property._id}
              name="show-on-map"
              onClick={showOnMap}>
              Show on map
            </button>
          )}
        </div>
      )}
    </>
  );
}

export default PropertyCard;
