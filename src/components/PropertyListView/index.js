import React, {useState, useEffect, useRef, useCallback} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import './styles.scss';
import PropertyCard from 'components/PropertyCard';
import {getViewList} from 'store/actions/propertyListActions';
import Loader from '../Loader';

function PropertyListView({setMapCenter, favorites_view, showContent}) {
  const [state, setState] = useState({
    list: [],
    loading: false,
    hasMore: true,
    reachedBottom: 0,
    start_of: 5,
    finish_of: 10,
  });
  const propertyList = useSelector((state) => state.propertyList);
  const dispatch = useDispatch();
  const listWrapper = useRef(null);
  const loader = useRef(null);

  useEffect(() => {
    if (!favorites_view) {
      setState({
        ...state,
        list: [...propertyList.list],
        hasMore: propertyList.list.length !== propertyList.count,
        loading: false,
      });
    }
  }, [propertyList.list]);

  useEffect(() => {
    if (favorites_view) {
      setState({
        ...state,
        list: propertyList.favorites_list.list,
        hasMore: false,
        loading: false,
      });
    }
  }, [propertyList.favorites_list.list]);

  useEffect(() => {
    if (!favorites_view) {
      var options = {
        root: null,
        rootMargin: '20px',
        threshold: 0.5,
      };
      // initialize IntersectionObserver
      // and attaching to Load More div
      const observer = new IntersectionObserver(handleObserver, options);
      if (loader.current) {
        observer.observe(loader.current);
      }

      return () => observer.disconnect();
    }
  }, [propertyList]);

  const handleObserver = useCallback(
    (entities) => {
      const target = entities[0];
      if (
        target.isIntersecting &&
        propertyList.start_of &&
        propertyList.finish_of
      ) {
        dispatch(
          getViewList(
            propertyList.filter.filtering,
            propertyList.start_of,
            propertyList.finish_of,
            false,
          ),
        );
      }
    },
    [propertyList],
  );

  return (
    <div
      className={`property-list-view ${showContent ? 'visible': 'hidden'}`}
      ref={listWrapper}>
      {state.list.length > 0 &&
        state.list.map((property) => (
          <PropertyCard
            key={property._id}
            property={property}
            setMapCenter={setMapCenter}
          />
        ))}
      {state.hasMore && (
        <div ref={loader}>
          <Loader />
        </div>
      )}
    </div>
  );
}

export default PropertyListView;
