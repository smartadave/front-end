import React, {Fragment, useCallback, useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import './styles.scss';
import {GoogleMap, useJsApiLoader, Marker} from '@react-google-maps/api';
import Point from 'components/Point';

function PropertyMapView({mapCenter, favorites_view, showContent}) {
  const propertyList = useSelector((state) => state.propertyList);

  const [state, setState] = useState({
  
  });
  const [map, setMap] = useState(null);

  useEffect(() => {
    if(!favorites_view) {
      setState({
        ...state,
        campus: propertyList.campus,
        properties: propertyList.list,
        mapProperties: propertyList.mapList,
      });
    }
  }, [propertyList]);

  useEffect(() => {
    if(favorites_view) {
      setState({
        ...state,
        campus: propertyList.campus,
        properties: propertyList.favorites_list.list,
        mapProperties: propertyList.favorites_list.mapList,
      });
    }
  }, [propertyList.favorites_list.mapList, propertyList.favorites_list.list]);

  const {isLoaded, loadError} = useJsApiLoader({
    googleMapsApiKey: process.env.REACT_APP_PLACE_API_KEY,
  });

  const onLoad = (map) => {
    const bounds = new window.google.maps.LatLngBounds();
    // bounds.extend(state.campus.coordinates);
    // console.log(state.campus.coordinates);
    // // state.properties.map(property => {
    // //     bounds.extend(property.address.location);
    // //     return property._id;
    // // });
    // map.fitBounds(bounds);
    setMap(map);
  };

  const onUnmount = useCallback((map) => {
    setMap(null);
  }, []);

  const markerHandler = (id) => {
    const properties = [...state.properties];
    const propertyIndex = properties.findIndex(
      (property) => property._id === id,
    );
    const updatedProperties = properties.map((property) => {
      const isClicked = property._id === id;
      return {
        ...property,
        showInfo: isClicked,
      };
    });
    setState({
      ...state,
      properties: updatedProperties,
    });
  };

  return (
    isLoaded && (
      <div className={`property-map-view ${showContent ? 'visible': 'hidden'}`}>
        <Fragment>
          <div className="rentals-available"></div>
          <GoogleMap
            mapContainerClassName="google-map"
            center={mapCenter.coordinates}
            zoom={14}
            streetView
            onLoad={onLoad}
            onUnmount={onUnmount}>
            <Marker
              className="property-marker"
              icon="/images/school_marker.svg"
              position={state.campus.coordinates}
            />
            {state.mapProperties.map((property) => (
              <Point
                key={property._id}
                icon={`/images/place${
                  property._id === mapCenter.id ? '_show' : ''
                }.svg`}
                property={property}
              />
            ))}
          </GoogleMap>
        </Fragment>
      </div>
    )
  );
}

export default React.memo(PropertyMapView);
