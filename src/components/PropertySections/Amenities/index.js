import React from 'react';
import './styles.scss';

function Amenities({
  amenityType,
  unitId,
  amenities,
  toggleAmenity = function () {},
  is_clickable,
}) {
  return (
    <div className="amenities">
      <p className="amenities-title">{amenityType} Amenities</p>
      <div className="amenities-list">
        {amenities.map((item) => (
          <div key={item._id || item.name} className="amenity-option">
            <img
              className={`amenity-image${item.selected ? ' selected' : ''}${
                is_clickable ? ' clickable' : ''
              }`}
              src={`/images/${item.icon_name}${
                item.selected ? '_selected' : ''
              }.svg`}
              onClick={() => toggleAmenity(item._id, unitId)}
            />
            <span>{item.name}</span>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Amenities;
