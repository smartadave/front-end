import React, { Fragment } from 'react';
import Selector from 'components/Selector';

function BedroomsBathrooms({ unitInfo, handleInputChange }) {

  let bedrooms = [
    { name: '1 Bedroom', value: '1' },
    { name: '2 Bedrooms', value: '2' },
    { name: '3 Bedrooms', value: '3' },
    { name: '4 Bedrooms', value: '4' },
    { name: '5 Bedrooms', value: '5' },
    { name: '6 Bedrooms', value: '6' }
  ]
  let bathrooms = [
    { name: '1 Bathroom', value: '1' },
    { name: '2 Bathrooms', value: '2' },
    { name: '3 Bathrooms', value: '3' },
    { name: '4 Bathrooms', value: '4' },
    { name: '5 Bathrooms', value: '5' },
    { name: '6 Bathrooms', value: '6' }
  ]

  return (
    <Fragment>
      <Selector
        label="Bedrooms"
        name="bedrooms"
        className="bedrooms"
        id={unitInfo.id}
        options={bedrooms}
        value={unitInfo.bedrooms}
        handleInputChange={handleInputChange}
        isRequired={true}
      />

      <Selector
        label="Bathrooms"
        name="bathrooms"
        className="bathrooms"
        id={unitInfo.id}
        options={bathrooms}
        value={unitInfo.bathrooms}
        handleInputChange={handleInputChange}
        isRequired={true}
      />
    </Fragment>
  );
}

export default BedroomsBathrooms;
