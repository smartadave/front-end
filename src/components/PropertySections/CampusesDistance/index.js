import React, {useEffect, useState} from 'react';
import './styles.scss';
import {distanceCalculate} from 'utilities/distanceCalculate';

function CampusesDistance({
  propertyAddress,
  previewMode,
  campuses,
  title,
  distance,
  returnDistance,
}) {
  const [state, setState] = useState({
    distance: [],
  });

  useEffect(() => {
    if (distance && Object.keys(distance).length !== 0) {
      setState({
        ...state,
        distance,
      });
    }
  }, [distance]);

  useEffect(() => {
    if (distance.length === 0 && state.distance.length > 0) {
      returnDistance(state.distance);
    }
  }, [state.distance]);

  useEffect(() => {
    if (Object.keys(propertyAddress).length !== 0 && campuses.length > 0) {
      calculateDistanceForCampuses(
        `${propertyAddress.geometry.location.lat},${propertyAddress.geometry.location.lng}`,
      );
    }
  }, [propertyAddress]);

  const calculateDistanceForCampuses = async (endPoint) => {
    let distance = await Promise.all(
      campuses.map(async (campus) => {
        let resp = {
          id: campus.id,
          campus: campus.campus,
          campus_distance: {},
        };
        const campus_distance = await distanceCalculate(
          `${campus.coordinates[0].lat},${campus.coordinates[0].lng}`,
          endPoint,
        );
        resp.campus_distance = campus_distance;
        return resp;
      }),
    );
    setState({
      ...state,
      distance,
    });
  };

  return (
    <div className="campuses-distance">
      <span className={previewMode ? 'preview-title' : ''}>{title}</span>
      <div className="campuses-list">
        {state.distance.map((campus) => {
          return (
            <div className="campuses-item">
              <span className="campuses-item__title">{campus.campus}</span>
              <div className="distances-list">
                <div className="distance">
                  <img alt="" src="/images/map_distance.svg" />
                  <span>{campus.campus_distance['WAY']}</span>
                </div>
                <div className="distance">
                  <img alt="" src="/images/walk_eta.svg" />
                  <span>{campus.campus_distance['WALKING']}</span>
                </div>
                <div className="distance">
                  <img alt="" src="/images/drive_eta.svg" />
                  <span>{campus.campus_distance['DRIVING']}</span>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default CampusesDistance;
