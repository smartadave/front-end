import React from 'react';
import './styles.scss';

function NavigationButtons({
  currentStep,
  onSubmit,
  saveToDraft,
  goToPreviousStep,
}) {
  return (
    <div className="navigation-buttons">
      {(currentStep === 3 || currentStep === 4) && (
        <button
          className="btn back-button"
          type="button"
          onClick={() => goToPreviousStep()}>
          &lt;&nbsp;&nbsp; Back
        </button>
      )}
      <button
        className="btn draft-button"
        name="draft"
        type="button"
        onClick={(event) => saveToDraft(event)}>
        Save as Draft and Complete Later
      </button>
      <button
        className="btn next-button"
        name="next"
        type="submit"
        onClick={(event) => onSubmit(event)}>
        {currentStep === 4 ? 'Publish' : 'Next \u00A0\u00A0>'}
      </button>
    </div>
  );
}

export default NavigationButtons;
