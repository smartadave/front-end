import React, {Fragment} from 'react';
import PlaceAutocomplete from 'components/PlaceAutocomplete';
import Selector from 'components/Selector';

function PropertyMainInfo({
  propertyInfo,
  returnAddressObject,
  handleInputChange,
  addressDisable,
}) {
  let property_type_obj = [
    {
      name: 'Apartment complex',
      value: 'Apartment complex',
    },
    {name: 'House', value: 'House'},
  ];

  return (
    <Fragment>
      <PlaceAutocomplete
        value={propertyInfo.address}
        returnAddressObject={(data) => returnAddressObject(data)}
        name="address"
        required={true}
        disabled={addressDisable}
        placeholder="Start typing..."
      />
      <Selector
        label="Property type"
        name="propertyType"
        className="property-type"
        options={property_type_obj}
        value={propertyInfo.propertyType}
        handleInputChange={handleInputChange}
      />
      <div style={{marginRight: 0}} className="input property-name-input">
        <label htmlFor="property_name" className="input-label">
          Property name (optional)
        </label>
        <input
          type="text"
          name="propertyName"
          id="property_name"
          className="input-field"
          placeholder="Start typing..."
          value={propertyInfo.propertyName}
          onChange={(event) => handleInputChange(event)}
        />
      </div>
    </Fragment>
  );
}

export default PropertyMainInfo;
