import React from 'react';
import './styles.scss';
import BedroomsBathrooms from 'components/PropertySections/BedroomsBathrooms';
import FormatInput from 'components/FormatInput';

function UnitMainInfo({
  unitInfo,
  handleInputChange,
  toggleUtilitiesIncluded,
  toggleUtility,
  setRentOption,
  toggleSplitEvenly,
  setBedroomPrice,
}) {
  return (
    <div className="unit-main-info">
      <div className="input unit-name-input">
        <label htmlFor="unit-name" className="input-label">
          Unit #/Name
        </label>
        <input
          type="text"
          name="unitName"
          id={unitInfo.id}
          className="input-field"
          required
          placeholder="1234"
          value={unitInfo.unitName}
          onChange={(event) => handleInputChange(event)}
        />
      </div>
      <div className="input unit-square-input">
        <label htmlFor="unit-square" className="input-label">
          Sq. Feet
        </label>
        <input
          type="text"
          name="area"
          id={unitInfo.id}
          className="input-field"
          required
          placeholder="Sq. Ft."
          value={unitInfo.area}
          onChange={(event) => handleInputChange(event)}
        />
      </div>
      <BedroomsBathrooms
        unitInfo={unitInfo}
        handleInputChange={handleInputChange}
      />
      <span className="vl"/>
      <FormatInput
        format="price"
        className="unit-price-input"
        label="Price"
        name="price"
        id={unitInfo.id}
        required={true}
        placeholder="$/mo"
        value={unitInfo.price}
        handleChange={handleInputChange}
      />
      <div className="utilities-checkbox">
        <label className="input-checkbox">
          <input
            name="utilitiesIncluded"
            type="checkbox"
            required
            checked={unitInfo.utilitiesIncluded}
            onChange={() => toggleUtilitiesIncluded(unitInfo.id)}
          />
          <span className="checkmark" />
          Utilities included
        </label>
      </div>
      {unitInfo.utilitiesIncluded && (
        <div className="utilities-container">
          <p>Utilities included</p>
          <div className="utilities-list">
            {unitInfo.utilitiesList.map((utility) => (
              <label key={utility.name} className="input-checkbox">
                <input
                  name={utility.name}
                  type="checkbox"
                  className={`${utility.selected ? 'utility-checked' : ''}`}
                  checked={utility.selected}
                  onChange={() => toggleUtility(utility.name, unitInfo.id)}
                />
                <span className="checkmark" />
                {utility.name}
              </label>
            ))}
          </div>
        </div>
      )}
      {+unitInfo.bedrooms >= 2 && (
        <div className="rent-options-container">
          <p>Rent Options</p>
          <div className="rent-options">
            <div className="radio-buttons">
              <label className="input-checkbox">
                <input
                  type="checkbox"
                  name="rentOptions"
                  id={unitInfo.id}
                  value="By Bedroom"
                  checked={unitInfo.rentOptions === 'By Bedroom'}
                  onChange={(event) => setRentOption(event)}
                />
                <span className="checkmark" />
                Allow rent by bedroom
              </label>
              {unitInfo.rentOptions === 'By Bedroom' && (
                <label className="input-toggle">
                  split the costs evenly
                  <input
                    type="checkbox"
                    checked={unitInfo.splitTheCosts}
                    onChange={() => toggleSplitEvenly(unitInfo.id)}
                  />
                  <span className="control" />
                </label>
              )}
            </div>
            {!unitInfo.splitTheCosts && unitInfo.rentOptions === 'By Bedroom' && (
              <div className="price-options">
                {unitInfo.price_per_bedroom.map((bedroom) => (
                  <FormatInput
                    key={bedroom.name}
                    format="price"
                    className="bedroom-price-input"
                    label={bedroom.name}
                    name={bedroom.name}
                    id={unitInfo.id}
                    required={true}
                    placeholder="$/mo"
                    value={bedroom.price}
                    handleChange={setBedroomPrice}
                  />
                ))}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
}

export default UnitMainInfo;
