import React, {useState, useRef, useEffect} from 'react';
import {Range} from 'rc-slider';
import 'rc-slider/assets/index.css';
import './styles.scss';
import {useOnClickOutside} from 'hooks/clickOutside';

const RangeSelector = ({
  isRequired,
  placeholder,
  style,
  inputStyle,
  bottomStyle,
  optionsStyle,
  value,
  returnRange,
  type,
  rangeData,
}) => {
  const [state, setState] = useState({
    show_range: false,
    value: value,
    rangeData: rangeData,
  });

  useEffect(() => {
    setState({
      ...state,
      rangeData: rangeData
    })
  }, [rangeData])
  
  useEffect(() => {
    if(JSON.stringify(state.value) !== JSON.stringify(value)) {
      setState({
        ...state,
        value: value
      })
    }
    
  }, [value])

  const ref = useRef(null);
  useOnClickOutside(ref, () => {state.show_range && handleShowOptions(false)});

  const handleShowOptions = (show_range) => {
    setState({
      ...state,
      show_range: show_range,
    });
  };

  return (
    <div ref={ref} style={style} className="range-selector">
      <input
        onClick={() => handleShowOptions(!state.show_range)}
        placeholder={placeholder ? placeholder : 'Select one...'}
        className="input-field"
        readOnly={true}
        style={inputStyle}
        required={isRequired}
        value={`${state.value.min} -  ${state.value.max} ${placeholder ? type === 'price' ? "$" : placeholder : ''}`}
      />
      <button type="button" style={bottomStyle} onClick={() => handleShowOptions(!state.show_range)}>
        <img
          src={
            state.show_range
              ? '/images/arrow_drop_up.svg'
              : '/images/arrow_drop_down.svg'
          }
        />
      </button>

      {state.show_range && (
        <div style={optionsStyle} className="range">
          <span>{type === 'price' && "$"} {state.rangeData.min}</span>
          <Range
            step={state.rangeData.step}
            onChange={(value) => setState({...state, value: {min: value[0], max: value[1]}})}
            onAfterChange={(value) => {
              returnRange(type, value);
            }}
            defaultValue={[state.rangeData.min, state.rangeData.max]}
            min={state.rangeData.min}
            max={state.rangeData.max}
          />
          <span>{type === 'price' && "$"} {state.rangeData.max}</span>
        </div>
      )}
    </div>
  );
};
export default RangeSelector;
