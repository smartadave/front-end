import React, {useState, useRef, useEffect} from 'react';
import './styles.scss';
import {useOnClickOutside} from 'hooks/clickOutside';

const Selector = ({
  label,
  name,
  value,
  id = name,
  handleInputChange,
  options,
  placeholder,
  multiselect,
  handleMultiInputChange,
  isRequired = false,
  style,
  inputStyle,
  optionsStyle,
  imageType,
  img,
  className,
}) => {
  const [state, setState] = useState({
    show_options: false,
    new_value: '',
    multiselect_value: [],
  });

  useEffect(() => {
    if (
      multiselect &&
      JSON.stringify(state.multiselect_value) !== JSON.stringify(value)
    ) {
      setState({
        ...state,
        multiselect_value: value,
      });
    }
  }, [value]);

  useEffect(() => {
    if (value && !multiselect) {
      returnValidName(value);
    }
  }, [value]);
  const ref = useRef(null);

  useOnClickOutside(ref, () => {
    state.show_options && handleShowOptions(false);
  });

  const handleShowOptions = (show_options) => {
    setState({
      ...state,
      show_options: show_options,
    });
  };

  const selectValue = (value) => {
    let e = {
      target: {
        name: name,
        value: value,
        id: id,
      },
    };
    handleInputChange(e);
    handleShowOptions(false);
  };

  const returnValidName = (value) => {
    return Object.keys(options).map((key) => {
      if (options[key].value === value.toString()) {
        setState({
          ...state,
          new_value: options[key].name,
        });
      }
    });
  };

  const selectMultiValue = (value) => {
    const value_index = state.multiselect_value.findIndex(
      (item) => item.value == value.value,
    );
    const new_multiselect_array = [...state.multiselect_value];

    if (value_index >= 0) {
      new_multiselect_array.splice(value_index, 1);
      setState({
        ...state,
        multiselect_value: new_multiselect_array,
      });
    } else {
      setState({
        ...state,
        multiselect_value: [...state.multiselect_value, value],
      });
    }
  };

  useEffect(() => {
    if (handleMultiInputChange) {
      handleMultiInputChange(state.multiselect_value);
    }
  }, [state.multiselect_value]);

  return (
    <div
      ref={ref}
      style={style}
      className={`${imageType ? 'selector' : 'selector select-block'}${
        className ? ` ${className}` : ''
      }`}>
      {!imageType ? (
        <>
          {label && (
            <label htmlFor={name + id} className="input-label">
              {label}
            </label>
          )}
          <input
            style={inputStyle}
            onClick={() => handleShowOptions(!state.show_options)}
            placeholder={placeholder ? placeholder : 'Select one...'}
            className="input-field"
            name={name}
            value={
              !multiselect
                ? state.new_value
                : state.multiselect_value.map((item) => item.name)
            }
            id={name + id}
            readOnly={true}
            required={isRequired}
          />
          <button
            className={
              !label
                ? 'selector-handle-button without-label'
                : 'selector-handle-button'
            }
            type="button"
            onClick={() => handleShowOptions(!state.show_options)}>
            <img
              src={
                state.show_options
                  ? '/images/arrow_drop_up.svg'
                  : '/images/arrow_drop_down.svg'
              }
            />
          </button>
        </>
      ) : (
        <button
          className="image-type-selector-button"
          onClick={() => handleShowOptions(!state.show_options)}>
          <img src={img} />
          <span>{placeholder}</span>
        </button>
      )}

      {state.show_options && (
        <div style={optionsStyle} className="options">
          <ul>
            {options.map((option) => {
              if (!multiselect) {
                return (
                  <li
                    value={option.value}
                    onClick={() => selectValue(option.value)}>
                    <span
                      className={
                        option.name === state.new_value ? 'selected' : ''
                      }>
                      {option.name}
                    </span>
                    {option.name === state.new_value && (
                      <img src="/images/check.svg" />
                    )}
                  </li>
                );
              } else
                return (
                  <li className="li-multiselect" value={option.value}>
                    <span>{option.name}</span>
                    <label className="input-checkbox">
                      <input
                        onChange={() => selectMultiValue(option)}
                        checked={state.multiselect_value.find((item) => {
                          return item.value === option.value;
                        })}
                        name="terms_of_use"
                        type="checkbox"
                      />
                      <span className="checkmark"></span>
                    </label>
                  </li>
                );
            })}
          </ul>
        </div>
      )}
    </div>
  );
};
export default Selector;
