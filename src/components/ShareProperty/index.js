import React, {useRef, useState} from 'react';
import './styles.scss';
import {useDispatch} from 'react-redux';
import checkForm from 'utilities/validator';
import {NotificationManager} from 'react-notifications';
import {shareProperty} from 'store/actions/propertyActions';

function ShareProperty({propertyId, userId, closeWindow}) {
  const [email, setEmail] = useState('');

  const urlRef = useRef(null);
  const dispatch = useDispatch();

  const onShareProperty = async (e) => {
    e.preventDefault();
    if (!checkForm(e.target.form)) {
      return;
    }
    const shared = await dispatch(shareProperty(propertyId, userId, email));
    if (shared.status) {
      closeWindow();
      NotificationManager.success(shared.message);
    } else {
      NotificationManager.error(shared.message);
    }
  };

  const copyToClipboard = () => {
    console.log(urlRef.current);
    urlRef.current.select();
    document.execCommand('copy');
    NotificationManager.success('Link copied to clipboard.');
  };
  return (
    <form className="share-property">
      <h4>Share Property</h4>
      <div className="input">
        <label htmlFor="first_name" className="input-label">
          Email
        </label>
        <input
          type="email"
          required
          name="email"
          id="email"
          className="input-field"
          onChange={(event) => setEmail(event.target.value)}
          value={email}
        />
      </div>
      <div className="share-actions">
        <button
          type="button"
          className="btn-link copy-link"
          onClick={copyToClipboard}>
          <img
            className="copy-img"
            src="/images/attach_file.svg"
            alt="attach"
          />
          Copy to clipboard
        </button>
        <button
          type="submit"
          className="btn btn-share"
          onClick={onShareProperty}>
          Share
        </button>
      </div>
      <input
        readOnly
        ref={urlRef}
        type="text"
        value={window.location.href}
        className="share-url"
      />
    </form>
  );
}

export default ShareProperty;
