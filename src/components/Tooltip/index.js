import React, { useState } from 'react';
import './styles.scss';

function ToolTip({ children, content }) {
    const [showTip, setShowTip] = useState(false);

    return (
        <div
            className="tooltip-wrapper"
            onMouseEnter={() => setShowTip(true)}
            onMouseLeave={() => setShowTip(false)}
        >
            {children}
            {showTip && (
                <div className="tooltip" >
                    {content}
                </div>
            )}
        </div>
    );
};

export default ToolTip;
