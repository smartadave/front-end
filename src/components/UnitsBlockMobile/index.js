import React from 'react';
import './styles.scss';
import {formatPrice, formatPriceRange} from 'helpers/format';
import Amenities from 'components/PropertySections/Amenities';
import MyPropertyMedia from 'components/MyPropertyMedia';
import UnitMainInfo from 'components/PropertySections/UnitMainInfo';
import MediaCarousel from '../MediaCarousel';
import {getCookie} from '../../utilities/cookie';

function UnitsBlockMobile({
  viewType,
  propertyId,
  unitsType,
  units,
  showRentOptions,
  editable,
  handleChangeUnitMode = () => {},
  returnUnitMediaFiles = () => {},
  handleInputChange = () => {},
  toggleExpandUnitInfo,
  toggleActiveListing,
  setBedroomPrice = () => {},
  setRentOption = () => {},
  toggleAmenity = () => {},
  toggleSplitEvenly = () => {},
  toggleUtilitiesIncluded = () => {},
  toggleUtility = () => {},
  showMoreUnitHandler = () => {},
  saveUnitChanges = () => {},
  deleteUnit = () => {},
  sendRequestToApplyToProperty = () => {},
}) {
  const auth_user = getCookie('user');

  return (
    <div className="units-mobile">
      {units.map((unit) => (
        <div className={`unit-section ${unit.mode}`}>
          {unit.mode === 'view' ? (
            <>
              <div className="unit-header">
                <p className="unit-name">Unit {unit.unitName}</p>
                {editable && (
                  <label className="input-toggle">
                    Available for rent
                    <input
                      type="checkbox"
                      checked={unit.activeListing}
                      onChange={() =>
                        toggleActiveListing(
                          !unit.activeListing,
                          'unit',
                          unit.id,
                        )
                      }
                    />
                    <span className="control" />
                  </label>
                )}
              </div>
              <div className="unit-info">
                <div className="unit-bnb">
                  <span className="bedrooms">
                    {unit.bedrooms} bedroom{unit.bedrooms > 1 ? `'s` : ''}
                  </span>
                  <span className="bathrooms">
                    {unit.bathrooms} bathroom{unit.bathrooms > 1 ? `'s` : ''}
                  </span>
                </div>
                {unit.area && (
                  <span className="unit-area">{unit.area} Sq ft</span>
                )}
                <span className="price">
                  Rent:
                  {showRentOptions === 'Bedrooms' ||
                  (editable && unit.rentOptions === 'By Bedroom')
                    ? ` ${formatPriceRange(unit.price_per_bedroom)}`
                    : ` ${formatPrice(unit.price)}`}
                </span>
              </div>
              {unitsType === 'Multi' && (
                <button
                  type="button"
                  className="btn-link expand-unit"
                  onClick={() => toggleExpandUnitInfo(unit.id)}>
                  Show {unit.expandUnitInfo ? 'less' : 'more'}
                  <img
                    src={`/images/arrow_drop_${
                      unit.expandUnitInfo ? 'up' : 'down'
                    }.svg`}
                    alt=""
                  />
                </button>
              )}
              {unitsType === 'Multi' && unit.expandUnitInfo && (
                <div className="expanded-unit">
                  {unit.mode === 'view' && editable && (
                    <div className="expanded-unit__header">
                      <div className="sq-feet">
                        <span className="title">Sq. feet</span>
                        <span className="area">{unit.area} Sq ft</span>
                      </div>
                      <div className="unit-actions">
                        <button
                          type="button"
                          name="edit"
                          className="btn edit-btn"
                          onClick={() => handleChangeUnitMode(unit.id)}>
                          Edit Unit Info
                        </button>
                        <button
                          type="button"
                          className={`show-more${
                            unit.show_more ? ' active' : ''
                          }`}
                          onClick={() => showMoreUnitHandler(unit.id)}>
                          <img src="/images/more_vert.svg" alt="show_more" />
                        </button>
                        {unit.show_more && (
                          <button
                            type="button"
                            className="delete-property"
                            onClick={() => deleteUnit(unit.id)}>
                            <img src="/images/delete.svg" alt="delete_unit" />
                            Delete Unit
                          </button>
                        )}
                      </div>
                    </div>
                  )}
                  {editable ? (
                    <div className="expanded-unit__media">
                      <p className="section-title media-title">
                        Photos / Videos
                      </p>
                      <MyPropertyMedia
                        type={unit.mode}
                        images={unit.photos}
                        videos={unit.videos}
                        links={unit.links}
                        returnMediaFiles={returnUnitMediaFiles}
                        unitId={unit.id}
                      />
                    </div>
                  ) : (
                    <MediaCarousel mediaList={unit.photos} />
                  )}
                  {unit.rentOptions === 'By Bedroom' && unit.mode === 'view' && (
                    <div className="bedrooms-prices">
                      <p>Prices</p>
                      {unit.price_per_bedroom.map((bedroom, i) => (
                        <div
                          key={i}
                          className={`bedroom${
                            editable ? ' editable' : ' view'
                          }`}>
                          <span className="bedroom__name">{bedroom.name}</span>
                          <span className="bedroom__price">
                            {formatPrice(bedroom.price)}
                          </span>
                          {editable && (
                            <label className="input-toggle">
                              Available for rent
                              <input
                                type="checkbox"
                                checked={bedroom.activeListing_per_bedroom}
                                onChange={() =>
                                  toggleActiveListing(
                                    !bedroom.activeListing_per_bedroom,
                                    'bedroom',
                                    bedroom._id,
                                  )
                                }
                              />
                              <span className="control" />
                            </label>
                          )}
                        </div>
                      ))}
                    </div>
                  )}
                  {unit.utilitiesList.length > 0 && unit.mode === 'view' && (
                    <div className="utilities">
                      <p className="section-title utilities-title">
                        Utilities included
                      </p>
                      <span>{unit.utilitiesList.join(', ')}</span>
                    </div>
                  )}
                  {unit.unitAmenities.length > 0 && (
                    <Amenities
                      amenityType="Unit"
                      unitId={unit.id}
                      amenities={unit.unitAmenities}
                      is_clickable={unit.mode === 'edit'}
                    />
                  )}
                  <div className="unit-expanded__description">
                    <p className="section-title description-title">
                      Unit description
                    </p>
                    <span className="unitDescription">
                      {unit.unitDescription}
                    </span>
                  </div>
                  {(viewType === 'create-preview' ||
                    auth_user.role === 'student') && (
                    <button
                      onClick={() =>
                        sendRequestToApplyToProperty('unit', unit._id)
                      }
                      className="btn apply-btn"
                      type="button">
                      Apply For This Unit
                    </button>
                  )}
                </div>
              )}
            </>
          ) : (
            <form>
              <div className="unit-actions">
                <button
                  type="button"
                  className="btn save-btn"
                  onClick={(event) => saveUnitChanges(event, unit.id)}>
                  Save changes
                </button>
              </div>
              <UnitMainInfo
                unitInfo={unit}
                handleInputChange={handleInputChange}
                toggleUtilitiesIncluded={toggleUtilitiesIncluded}
                toggleUtility={toggleUtility}
                setRentOption={setRentOption}
                toggleSplitEvenly={toggleSplitEvenly}
                setBedroomPrice={setBedroomPrice}
              />
              {unitsType === 'Multi' && (
                <>
                  <div className="unit-media">
                    <p className="section-title unit-media">Photos / Videos</p>
                    <MyPropertyMedia
                      type={unit.mode}
                      images={unit.photos}
                      videos={unit.videos}
                      links={unit.links}
                      returnMediaFiles={returnUnitMediaFiles}
                      unitId={unit.id}
                    />
                  </div>
                  <Amenities
                    amenityType="Unit"
                    unitId={unit.id}
                    amenities={unit.unitAmenities}
                    is_clickable={unit.mode === 'edit'}
                    toggleAmenity={toggleAmenity}
                  />
                  <div className="unit-description">
                    <p className="section-title description-title">
                      Unit description
                    </p>
                    <textarea
                      className="edit-description"
                      name="unitDescription"
                      id={unit.id}
                      value={unit.unitDescription}
                      onChange={handleInputChange}
                    />
                  </div>
                </>
              )}
            </form>
          )}
        </div>
      ))}
      {units.length > 0 && units[0].mode === 'view' && unitsType === 'Single' && (
        <div className="single-unit-info-mobile">
          {units[0].utilitiesList.length > 0 && (
            <div className="utilities">
              <p className="section-title utilities-title">Utilities included</p>
              <span>{units[0].utilitiesList.join(', ')}</span>
            </div>
          )}
          {(showRentOptions === 'Bedrooms' ||
            units[0].rentOptions === 'By Bedroom') && (
            <div className="bedrooms-prices">
              <p>Prices</p>
              {units[0].price_per_bedroom.map((bedroom, i) => (
                <div
                  key={i}
                  className={`bedroom${editable ? ' editable' : ' view'}`}>
                  <span className="bedroom__name">{bedroom.name}</span>
                  <span className="bedroom__price">
                    {formatPrice(bedroom.price)}
                  </span>
                  {editable && (
                    <label className="input-toggle">
                      Available for rent
                      <input
                        type="checkbox"
                        checked={bedroom.activeListing_per_bedroom}
                        onChange={() =>
                          toggleActiveListing(
                            !bedroom.activeListing_per_bedroom,
                            'bedroom',
                            bedroom._id,
                          )
                        }
                      />
                      <span className="control" />
                    </label>
                  )}
                </div>
              ))}
            </div>
          )}
        </div>
      )}
      {unitsType === 'Single' &&
        !editable &&
        (auth_user.role === 'student' || viewType === 'create-preview') && (
          <button
            onClick={() => sendRequestToApplyToProperty('property', propertyId)}
            className="btn apply-btn"
            type="button">
            Apply For This Property
          </button>
        )}
    </div>
  );
}

export default UnitsBlockMobile;
