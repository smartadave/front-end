import React, {Fragment} from 'react';
import './styles.scss';
import MediaCarousel from 'components/MediaCarousel';
import {formatPrice, formatPriceRange} from 'helpers/format';
import Amenities from 'components/PropertySections/Amenities';
import MyPropertyMedia from 'components/MyPropertyMedia';
import UnitMainInfo from 'components/PropertySections/UnitMainInfo';
import {getCookie} from '../../utilities/cookie';

function UnitsTable({
  viewType,
  propertyId,
  sendRequestToApplyToProperty = () => {},
  unitsType,
  units,
  showRentOptions,
  editable,
  handleChangeUnitMode = () => {},
  returnUnitMediaFiles = () => {},
  handleInputChange = () => {},
  toggleExpandUnitInfo,
  toggleActiveListing,
  setBedroomPrice = () => {},
  setRentOption = () => {},
  toggleAmenity = () => {},
  toggleSplitEvenly = () => {},
  toggleUtilitiesIncluded = () => {},
  toggleUtility = () => {},
  showMoreUnitHandler = () => {},
  saveUnitChanges = () => {},
  deleteUnit = () => {},
}) {
  const auth_user = getCookie('user');

  return (
    <>
      <table
        className={`units-table${unitsType === 'Single' ? ' single' : ''}${
          editable ? ' editable' : ''
        }`}>
        <tbody>
          {editable && <th>Unit#/Name</th>}
          <th>Beds</th>
          <th>Baths</th>
          {!editable && <th>Sq.feet</th>}
          <th>Rent {showRentOptions === 'Bedrooms' && 'for Bedroom'}</th>
          {unitsType === 'Multi' && (
            <th>{!editable ? 'Unit#/Name' : ' '}</th>
          )}
          {unitsType === 'Single' && editable && <th />}
          {units.length > 0 &&
            units.map((unit) => (
              <Fragment>
                <tr key={unit.id} onClick={() => toggleExpandUnitInfo(unit.id)}>
                  {editable && <td>{unit.unitName}</td>}
                  <td>
                    {unit.bedrooms} bedroom{+unit.bedrooms > 1 && 's'}
                  </td>
                  <td>
                    {unit.bathrooms} bathroom
                    {+unit.bathrooms > 1 && 's'}
                  </td>
                  {!editable && <td>{unit.area} Sq ft</td>}
                  <td>
                    {showRentOptions === 'Bedrooms' ||
                    (editable && unit.rentOptions === 'By Bedroom')
                      ? `${formatPriceRange(unit.price_per_bedroom)}`
                      : `${formatPrice(unit.price)}`}
                  </td>
                  {unitsType === 'Multi' && !editable && (
                    <td className="unit-name">
                      {unit.unitName}
                      <img
                        alt=""
                        src={`/images/arrow_drop_${
                          unit.expandUnitInfo ? 'up' : 'down'
                        }.svg`}
                      />
                    </td>
                  )}
                  {editable && (
                    <td className="unit-active-listing">
                      <label className="input-toggle">
                        Available for rent
                        <input
                          type="checkbox"
                          checked={unit.activeListing}
                          onChange={() =>
                            toggleActiveListing(
                              !unit.activeListing,
                              'unit',
                              unit.id,
                            )
                          }
                        />
                        <span className="control" />
                      </label>
                      {unitsType === 'Multi' && (
                        <img
                          alt=""
                          src={`/images/arrow_drop_${
                            unit.expandUnitInfo ? 'up' : 'down'
                          }.svg`}
                        />
                      )}
                    </td>
                  )}
                </tr>
                {unitsType === 'Multi' && unit.expandUnitInfo && (
                  <tr>
                    <td colSpan="5">
                      <div className="expanded-unit-info">
                        {!editable && (
                          <div className="unit-left-block">
                            <MediaCarousel mediaList={unit.photos} />
                            {(viewType === 'create-preview' ||
                              auth_user.role === 'student') && (
                              <button
                                onClick={() =>
                                  sendRequestToApplyToProperty('unit', unit._id)
                                }
                                className="btn apply-btn"
                                type="button">
                                Apply For This Unit
                              </button>
                            )}
                          </div>
                        )}
                        <div className="unit-main-block">
                          {unit.mode === 'view' && editable && (
                            <div className="unit-header">
                              <div className="sq-feet">
                                <span className="title">Sq. feet</span>
                                <span className="area">{unit.area} Sq ft</span>
                              </div>
                              <div className="unit-actions">
                                <button
                                  type="button"
                                  name="edit"
                                  className="btn edit-btn"
                                  onClick={() => handleChangeUnitMode(unit.id)}>
                                  Edit Unit Info
                                </button>
                                <button
                                  type="button"
                                  className={`show-more${
                                    unit.show_more ? ' active' : ''
                                  }`}
                                  onClick={() => showMoreUnitHandler(unit.id)}>
                                  <img
                                    src="/images/more_vert.svg"
                                    alt="show_more"
                                  />
                                </button>
                                {unit.show_more && (
                                  <button
                                    type="button"
                                    className="delete-property"
                                    onClick={() => deleteUnit(unit.id)}>
                                    <img
                                      src="/images/delete.svg"
                                      alt="delete_unit"
                                    />
                                    Delete Unit
                                  </button>
                                )}
                              </div>
                            </div>
                          )}
                          {editable && unit.mode === 'edit' && (
                            <div className="unit-actions">
                              <button
                                type="button"
                                name="save"
                                className="btn save-btn"
                                onClick={(event) =>
                                  saveUnitChanges(event, unit.id)
                                }>
                                Save changes
                              </button>
                            </div>
                          )}
                          {unit.mode === 'edit' && (
                            <UnitMainInfo
                              unitInfo={unit}
                              handleInputChange={handleInputChange}
                              toggleUtilitiesIncluded={toggleUtilitiesIncluded}
                              toggleUtility={toggleUtility}
                              setRentOption={setRentOption}
                              toggleSplitEvenly={toggleSplitEvenly}
                              setBedroomPrice={setBedroomPrice}
                            />
                          )}
                          {editable && (
                            <div className="unit-media">
                              <p className="section-title media-title">
                                Photos / Videos
                              </p>
                              <MyPropertyMedia
                                type={unit.mode}
                                images={unit.photos}
                                videos={unit.videos}
                                links={unit.links}
                                returnMediaFiles={returnUnitMediaFiles}
                                unitId={unit.id}
                              />
                            </div>
                          )}
                          {unit.rentOptions === 'By Bedroom' &&
                            unit.mode === 'view' && (
                              <div className="bedrooms-prices">
                                <p>Prices</p>
                                {unit.price_per_bedroom.map((bedroom, i) => (
                                  <div
                                    key={i}
                                    className={`bedroom${
                                      editable ? ' editable' : ' view'
                                    }`}>
                                    <span className="bedroom__name">
                                      {bedroom.name}
                                    </span>
                                    <span className="bedroom__price">
                                      {formatPrice(bedroom.price)}
                                    </span>
                                    {editable && (
                                      <label className="input-toggle">
                                        Available for rent
                                        <input
                                          type="checkbox"
                                          checked={
                                            bedroom.activeListing_per_bedroom
                                          }
                                          onChange={() =>
                                            toggleActiveListing(
                                              !bedroom.activeListing_per_bedroom,
                                              'bedroom',
                                              bedroom._id,
                                            )
                                          }
                                        />
                                        <span className="control" />
                                      </label>
                                    )}
                                  </div>
                                ))}
                              </div>
                            )}
                          {unit.utilitiesList.length > 0 &&
                            unit.mode === 'view' && (
                              <div className="utilities">
                                <p>Utilities included</p>
                                <span>{unit.utilitiesList.join(', ')}</span>
                              </div>
                            )}
                          {unit.unitAmenities.length > 0 && (
                            <Amenities
                              amenityType="Unit"
                              unitId={unit.id}
                              amenities={unit.unitAmenities}
                              is_clickable={unit.mode === 'edit'}
                              toggleAmenity={toggleAmenity}
                            />
                          )}
                          {
                            <div className="unit-description">
                              <p>Unit description</p>
                              {unit.mode === 'edit' ? (
                                <textarea
                                  className="edit-description"
                                  name="unitDescription"
                                  id={unit.id}
                                  placeholder="Add your description here"
                                  required
                                  value={unit.unitDescription}
                                  onChange={handleInputChange}
                                />
                              ) : (
                                <span>{unit.unitDescription}</span>
                              )}
                            </div>
                          }
                        </div>
                      </div>
                    </td>
                  </tr>
                )}
              </Fragment>
            ))}
        </tbody>
      </table>
      {unitsType === 'Single' &&
        !editable &&
        (auth_user.role === 'student' || viewType === 'create-preview') && (
          <button
            onClick={() => sendRequestToApplyToProperty('property', propertyId)}
            className="btn apply-btn"
            type="button">
            Apply For This Property
          </button>
        )}
      {units.length > 0 &&
        units[0].mode === 'view' &&
        unitsType ===
          'Single' /*&&
        (showRentOptions === 'Bedrooms' ||
          units[0].rentOptions === 'By Bedroom')*/ && (
          <div className="single-unit-info">
            {units[0].mode === 'view' && editable && (
              <div className="unit-header">
                <div className="sq-feet">
                  <span className="title">Sq. feet</span>
                  <span className="area">{units[0].area} Sq ft</span>
                </div>
                <div className="unit-actions">
                  <button
                    type="button"
                    name="edit"
                    className="btn edit-btn"
                    onClick={() => handleChangeUnitMode(units[0].id)}>
                    Edit Unit Info
                  </button>
                </div>
              </div>
            )}
            {units[0].utilitiesList.length > 0 && (
              <div className="utilities">
                <p>Utilities included</p>
                <span>{units[0].utilitiesList.join(', ')}</span>
              </div>
            )}
            {(showRentOptions === 'Bedrooms' ||
              units[0].rentOptions === 'By Bedroom') && (
              <div className="bedrooms-prices">
                <p>Prices</p>
                {units[0].price_per_bedroom.map((bedroom, i) => (
                  <div
                    key={i}
                    className={`bedroom${editable ? ' editable' : ' view'}`}>
                    <span className="bedroom__name">{bedroom.name}</span>
                    <span className="bedroom__price">
                      {formatPrice(bedroom.price)}
                    </span>
                    {editable && (
                      <label className="input-toggle">
                        Available for rent
                        <input
                          type="checkbox"
                          checked={bedroom.activeListing_per_bedroom}
                          onChange={() =>
                            toggleActiveListing(
                              !bedroom.activeListing_per_bedroom,
                              'bedroom',
                              bedroom._id,
                            )
                          }
                        />
                        <span className="control" />
                      </label>
                    )}
                  </div>
                ))}
              </div>
            )}
          </div>
        )}
      {units.length > 0 && units[0].mode === 'edit' && unitsType === 'Single' && (
        <>
          <div className="unit-header">
            <div className="unit-actions">
              <button
                type="button"
                name="edit"
                className="btn edit-btn"
                onClick={(event) => saveUnitChanges(event, units[0].id)}>
                Save changes
              </button>
            </div>
          </div>
          <UnitMainInfo
            unitInfo={units[0]}
            handleInputChange={handleInputChange}
            toggleUtilitiesIncluded={toggleUtilitiesIncluded}
            toggleUtility={toggleUtility}
            setRentOption={setRentOption}
            toggleSplitEvenly={toggleSplitEvenly}
            setBedroomPrice={setBedroomPrice}
          />
        </>
      )}
    </>
  );
}

export default UnitsTable;
