import React, {useState, useRef, useEffect} from 'react';
import './styles.scss';
import Modal from 'react-modal';
import {NotificationManager} from 'react-notifications';
import checkForm from 'utilities/validator';
import {useWindowSize} from 'hooks/windowSize';

// import {uploadPhotosToS3} from 'store/actions/propertyActions';
// import {useDispatch} from 'react-redux';
// import {createFile} from 'helpers/media';

function UploadMedia({returnMediaFiles, photos, videos}) {
  const [state, setState] = useState({
    images: [],
    videos: [],
    links: [],
    link: '',
    drag_on: false,
    modal_is_open: false,
    file_for_modal: {},
    width: 0
  });
  const fileInput = useRef(null);
  const ref = useRef(null)

  const window_size = useWindowSize();

  useEffect(() => {
    setState({
      ...state,
      width: ref.current ? ref.current.offsetWidth : 0
    })
  }, [window_size]);

  useEffect(() => {
    if (photos.length > 0) {
      setState({
        ...state,
        images: [...photos],
      });
    }
  }, [photos]);

  useEffect(() => {
    if (videos.length > 0) {
      setState({
        ...state,
        videos: [...videos],
      });
    }
  }, [videos]);


  const dragOver = (e) => {
    e.preventDefault();
  };

  const dragLeave = (e) => {
    e.preventDefault();
  };

  const dragEnter = (e) => {
    setState({
      ...state,
      drag_on: !state.drag_on,
    });
  };

  const fileDrop = (e, type) => {
    let images = [];
    let videos = [];
    e.preventDefault();

    const files = type === 'input' ? fileInput.current.files : e.dataTransfer.files;

    Object.keys(files).forEach((key) => {
      let media_file = files[key];
      if (media_file.type.search('video') >= 0) {
        media_file.size / 1048576 < 50
          ? videos.push(URL.createObjectURL(media_file))
          : NotificationManager.error('Video cannot be larger than 50 mb');
      }
      if (media_file.type.search('image') >= 0) {
        media_file.size / 1048576 < 3
          ? images.push(URL.createObjectURL(media_file))
          : NotificationManager.error('Image cannot be larger than 3 mb');
      }
    });
    
    setState({
      ...state,
      videos: [...state.videos, ...videos],
      images: [...state.images, ...images],
      drag_on: false,
    });
    returnMediaFiles({
      videos: [...state.videos, ...videos],
      images: [...state.images, ...images],
    })
  };

  const triggerFileInput = () => {
    fileInput.current.click();
  };

  const renderMediaInput = () => {
    return (
        <input
          type="file"
          name="file"
          name="file"
          id="file"
          multiple
          className="hidden"
          ref={fileInput}
          onChange={(e) => fileDrop(e, 'input')}
          value={state.link}
        />
    );
  };

  const deleteMediaFromList = (type, key) => {
    const removed_array = state[type]
    removed_array.splice(key,1)

    setState({
      ...state,
      [type]: removed_array
    });
  };

  const renderMediaFiles = () => {
    return (
      <div style={{width: state.width}} className="upload-media__preview">
        {state.images.map((img, key) => (
          <div>
            <img
              onClick={() =>
                setState({
                  ...state,
                  modal_is_open: true,
                  file_for_modal: {
                    type: 'image',
                    src:img,
                  },
                })
              }
              src={img}
            />
            <img
              onClick={() => deleteMediaFromList('images', key)}
              className="delete-media"
              src="/images/close.svg"
            />
          </div>
        ))}
        {state.videos.map((video, key) => (
          <div>
            <video
              onClick={() =>
                setState({
                  ...state,
                  modal_is_open: true,
                  file_for_modal: {
                    type: 'video',
                    src: video,
                  },
                })
              }
              width={126}
              height={116}>
              <source src={video}></source>
            </video>
            <img
              onClick={() => deleteMediaFromList('videos', key)}
              className="delete-media"
              src="/images/close.svg"
            />
          </div>
        ))}
        {state.links.map((video, key) => (
          <div>
            <video
              onClick={() =>
                setState({
                  ...state,
                  modal_is_open: true,
                  file_for_modal: {
                    type: 'video',
                    src: video,
                  },
                })
              }
              width={126}
              height={116}>
              <source data-yt2html5={video}></source>
            </video>
            <img
              onClick={() => deleteMediaFromList('links', key)}
              className="delete-media"
              src="/images/close.svg"
            />
          </div>
        ))}
      </div>
    );
  };

  const handleInputChange = ({target}) => {
    setState({
      ...state,
      link: target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      setState({
        ...state,
        links: [...state.links, state.link],
        link: '',
      });
    }
  };
  return (
    <>
      <Modal
        isOpen={state.modal_is_open}
        style={customStyles}
        contentLabel="Example Modal">
        <button
          onClick={() => setState({...state, modal_is_open: false})}
          type="button"
          className="btn-link close-modal">
          <img src="/images/close.svg" />
        </button>
        {state.file_for_modal.type === 'image' && state.modal_is_open ? (
          <img className="full-media" src={state.file_for_modal.src} />
        ) : (
          <video autoPlay controls className="full-media">
            <source src={state.file_for_modal.src}></source>
          </video>
        )}
      </Modal>
      <div className="upload-media">
        <div
          ref={ref}
          onDragOver={dragOver}
          onDragEnter={dragEnter}
          onDragLeave={dragLeave}
          onDrop={(e) => fileDrop(e, 'd&d')}
          className={`upload-media__drag-n-drop ${
            state.drag_on ? 'drag_on' : ''
          }`}>
          <div>
            <img src="/images/drag_n_drop.svg" />
            <p>
              Drop files here or{' '}
              <button onClick={triggerFileInput} type="button" className="btn-link">
              {renderMediaInput()}
                Upload
              </button>
            </p>
          </div>
          <p>
            Photos must be in jpg, gif or png formats, and no larger than 3 Mb
            in size
          </p>
          <p>
            Videos must be in mp4, webm, ogg, mov formats, and no larger than 50
            Mb in size
          </p>
        </div>
        {/* <div className="upload-media__additional-links">
          <form
            className="upload-media__form"
            name="upload-media"
            noValidate="novalidate"
            onSubmit={onSubmit}
            autoComplete="off">
              <div className="media-input">
                <input
                  type="url"
                  name="media_link"
                  id="media_link"
                  className="input-field"
                  placeholder="Youtube and Vimeo video's are supported"
                  required
                  onChange={handleInputChange}
                  value={state.link}
                />
              </div>
              <input
                type="submit"
                value="Add Video"
                className="btn-link input-field__button"
              />
          </form>
        </div> */}
        {renderMediaFiles()}
      </div>
    </>
  );
}

export default UploadMedia;

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    border: 0,
    boxShadow: '0px 4px 24px rgba(0, 0, 0, 0.07)',
    transform: 'translate(-50%, -50%)',
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
  },
};
