export const returnInterlocutorName = (users, user) => {
  let name = ""
  if(users.length >= 2) {
    Object.keys(users).forEach(user_key => {
      if(users[user_key]._id !== user.id) {

        const first_name = users[user_key]['managerId'] ? users[user_key]['managerId'].firstName : users[user_key]['studentId'].firstName
        let last_name = users[user_key]['managerId'] ? users[user_key]['managerId'].lastName : users[user_key]['studentId'].lastName
        name = `${first_name} ${last_name}`
       
      }
    })
  }
  return name
}

export const isMeAuthor = (author_id, user_id) => {
  return author_id === user_id
}