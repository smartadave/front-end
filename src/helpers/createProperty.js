export const onLeaveCreateProperty = (location) => {
  const acceptable_routes = [
    '/add-property/property-information',
    '/add-property/unit-information',
    '/add-property/preview-property',
  ];
  const isAcceptableRoute = acceptable_routes.some(
    (route) => route === location.pathname,
  );
  return isAcceptableRoute;
};

export const formPricePerBedroomArray = (number, start) => {
  return new Array(number).fill(null).map((bedroom, i) => ({
    name: `Bedroom ${start + i}`,
    price: '',
  }));
};

export const calculateBedroomsPrice = (
  splitTheCosts,
  bedroomsArray,
  price,
  bedrooms,
) => {
  const price_per_bedroom = bedroomsArray.map((bedroom) => {
    return {
      name: bedroom.name,
      price: splitTheCosts ? Math.round(price / bedrooms) : '',
    };
  });
  return price_per_bedroom;
};

export const formatPropertyByAl = (
  activate,
  data,
  property_section,
  section_id,
) => {
  const property = JSON.parse(JSON.stringify(data));
  if (property_section === 'property') {
    property.activeListing = activate;
    property.units = [...property.units].map((unit) => ({
      ...unit,
      activeListing: activate,
      price_per_bedroom:
        unit.price_per_bedroom.length > 0
          ? unit.price_per_bedroom.map((bedroom) => ({
              ...bedroom,
              activeListing_per_bedroom: activate,
            }))
          : [],
    }));
  } else if (property_section === 'unit') {
    const unit_index = property.units.findIndex(
      (unit) => unit._id === section_id,
    );
    property.units[unit_index] = {
      ...property.units[unit_index],
      activeListing: activate,
      price_per_bedroom:
        property.units[unit_index].price_per_bedroom.length > 0
          ? property.units[unit_index].price_per_bedroom.map((bedroom) => ({
              ...bedroom,
              activeListing_per_bedroom: activate,
            }))
          : [],
    };
  } else if (property_section === 'bedroom') {
    const unit_index = property.units.findIndex((unit) =>
      unit.price_per_bedroom.some((bedroom) => bedroom._id === section_id),
    );
    const bedroom_index = property.units[
      unit_index
    ].price_per_bedroom.findIndex((bedroom) => bedroom._id === section_id);
    property.units[unit_index].price_per_bedroom[
      bedroom_index
    ].activeListing_per_bedroom = !property.units[unit_index].price_per_bedroom[
      bedroom_index
    ].activeListing_per_bedroom;
    const disable_unit = property.units[unit_index].price_per_bedroom.every(
      (bedroom) => bedroom.activeListing_per_bedroom === false,
    );
    property.units[unit_index].activeListing = !disable_unit;
  }
  const disable_property = property.units.every(
    (unit) => unit.activeListing === false,
  );
  property.activeListing = !disable_property;
  return property;
};

export const handleUnitInputChangeHelper = (target, units_data) => {
  const units = [...units_data];
  const unitIndex = units_data.findIndex((unit) => unit.id === target.id);
  units[unitIndex][target.name] = target.value;

  if (target.name === 'bedrooms') {
    if (units[unitIndex].price_per_bedroom.length === 0) {
      units[unitIndex].price_per_bedroom = formPricePerBedroomArray(
        +target.value,
        1,
      );
    } else if (units[unitIndex].price_per_bedroom.length < +target.value) {
      const difference =
        Number(target.value) - units[unitIndex].price_per_bedroom.length;
      const newBedrooms = formPricePerBedroomArray(
        difference,
        units[unitIndex].price_per_bedroom.length + 1,
      );
      units[unitIndex].price_per_bedroom = [
        ...units[unitIndex].price_per_bedroom,
        ...newBedrooms,
      ];
    } else if (units[unitIndex].price_per_bedroom.length > +target.value) {
      units[unitIndex].price_per_bedroom = units[
        unitIndex
      ].price_per_bedroom.slice(0, +target.value);
    }
    units[unitIndex].price_per_bedroom = calculateBedroomsPrice(
      units[unitIndex].splitTheCosts,
      [...units[unitIndex].price_per_bedroom],
      +units[unitIndex].price,
      +target.value,
    );
  }

  if (
    target.name === 'price' &&
    units[unitIndex].price_per_bedroom.length > 0
  ) {
    units[unitIndex].price_per_bedroom = calculateBedroomsPrice(
      units[unitIndex].splitTheCosts,
      [...units[unitIndex].price_per_bedroom],
      +target.value,
      units[unitIndex].bedrooms,
    );
  }
  return units_data;
};

export const toggleUtilitiesIncludedHelper = (id, units_data) => {
  const units = [...units_data];
  const unitIndex = units.findIndex((unit) => unit.id === id);
  const utilitiesIncluded = !units[unitIndex].utilitiesIncluded;
  units[unitIndex].utilitiesIncluded = utilitiesIncluded;
  if (!utilitiesIncluded) {
    units[unitIndex].utilitiesList.forEach(
      (utility) => (utility.selected = false),
    );
  }
  return units;
};

export const toggleUtilityHelper = (name, id, units_data) => {
  const units = [...units_data];
  const unitIndex = units.findIndex((unit) => unit.id === id);
  const utilityIndex = units[unitIndex].utilitiesList.findIndex(
    (utility) => utility.name === name,
  );
  units[unitIndex].utilitiesList[utilityIndex].selected = !units[unitIndex]
    .utilitiesList[utilityIndex].selected;
  return units;
};

export const setRentOptionHelper = (target, units_data) => {
  const units = [...units_data];
  const unitIndex = units.findIndex((unit) => unit.id === target.id);
  const new_value =
    target.value !== units_data[unitIndex].rentOptions
      ? target.value
      : 'Entirely';
  units[unitIndex].rentOptions = new_value;
  units[unitIndex].price_per_bedroom =
    target.value === 'By Bedroom'
      ? formPricePerBedroomArray(Number(units_data[unitIndex].bedrooms), 1)
      : [];
  return units;
};

export const toggleSplitEvenlyHelper = (id, units_data) => {
  const units = [...units_data];
  const unitIndex = units.findIndex((unit) => unit.id === id);
  units[unitIndex].splitTheCosts = !units[unitIndex].splitTheCosts;
  units[unitIndex].price_per_bedroom = calculateBedroomsPrice(
    units[unitIndex].splitTheCosts,
    [...units[unitIndex].price_per_bedroom],
    +units[unitIndex].price,
    +units[unitIndex].bedrooms,
  );
  return units_data;
};

export const setBedroomPriceHelper = (target, units_data) => {
  const units = [...units_data];
  const unitIndex = units.findIndex((unit) => unit.id === target.id);
  const bedroomIndex = units[unitIndex].price_per_bedroom.findIndex(
    (bedroom) => bedroom.name === target.name,
  );
  units[unitIndex].price_per_bedroom[bedroomIndex].price = target.value;
  return units;
};

export const toggleAmenityHelper = (item_id, unit_id, units_data) => {
  const units = [...units_data];
  const unitIndex = units.findIndex((unit) => unit.id === unit_id);
  const itemIndex = units[unitIndex].unitAmenities.findIndex(
    (item) => item._id === item_id,
  );
  units[unitIndex].unitAmenities[itemIndex].selected = !units[unitIndex]
    .unitAmenities[itemIndex].selected;
  return units;
};

export const formatPropertyAmenities = (type, property, property_amenities) => {
  return type === 'view'
    ? [...property.propertyAmenities].map((amenity) => ({
        name: amenity,
        icon_name: amenity.toLowerCase().split(' ').join('_'),
      }))
    : [...property_amenities].map((amenity) => ({
        ...amenity,
        selected: property.propertyAmenities.some((el) => el === amenity.name),
      }));
};

export const formatUnitAmenities = (unit_data, init_data, unit_amenities) => {
  const unit = JSON.parse(JSON.stringify(unit_data));
  if (unit_data.mode === 'edit') {
    unit.unitAmenities = unit_amenities.map((amenity) => ({
          ...amenity,
          selected: init_data.unitAmenities.some((el) => el === amenity.name),
        }));
  } else {
    unit.unitAmenities =
        unit.unitAmenities.length > 0
          ? [...unit.unitAmenities].map((amenity) => ({
              name: amenity,
              icon_name: amenity.toLowerCase().split(' ').join('_'),
            }))
          : [];
  }
  return unit.unitAmenities;
};

export const formatUtilities = (unit_data, init_data) => {
  const unit = JSON.parse(JSON.stringify(unit_data));
  if (unit_data.mode === 'view') {
   unit.utilitiesList = [...init_data.utilitiesList];
  } else {
    unit.utilitiesList = [...initial_utilities].map((utility) => ({
      ...utility,
      selected: init_data.utilitiesList.some(
          (el) => el === utility.name,
      ),
    }));
  }
  return unit.utilitiesList;
};

export const formatReqAmenitiesUtils = (array) => {
  return [...array]
      .filter((el) => el.selected)
      .map((el) => el.name);
};

const initial_utilities = [
  {name: 'Air conditioning', selected: false},
  {name: 'Trash removal', selected: false},
  {name: 'Electricity', selected: false},
  {name: 'Gas', selected: false},
  {name: 'Cable', selected: false},
  {name: 'Sewer', selected: false},
  {name: 'Heat', selected: false},
  {name: 'Water', selected: false},
];

export const initialUnit = {
  unitName: '',
  area: '',
  bedrooms: '',
  bathrooms: '',
  price: '',
  rentOptions: 'Entirely',
  splitTheCosts: false,
  price_per_bedroom: [],
  utilitiesIncluded: false,
  utilitiesList: [
    {name: 'Air conditioning', selected: false},
    {name: 'Trash removal', selected: false},
    {name: 'Electricity', selected: false},
    {name: 'Gas', selected: false},
    {name: 'Cable', selected: false},
    {name: 'Sewer', selected: false},
    {name: 'Heat', selected: false},
    {name: 'Water', selected: false},
  ],
  photos: [],
  videos: [],
  links: [],
  unitAmenities: [],
  unitDescription: '',
  // showMedia: false,
  // showAmenities: false,
  // showDescription: false,
  // showDescError: false,
};
