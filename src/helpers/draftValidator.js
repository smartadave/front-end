/*
    PROPERTY

    media: length > 0;
    description: length > 0;

    UNITS

      unitName: length > 0,
      area: length > 0,
      bedrooms: length > 0,
      bathrooms: length > 0,
      price: length > 0,
      rentOptions: 'Entirely',
      splitTheCosts: false, // if true check price_per_bedroom price length
      price_per_bedroom: [],
      utilitiesIncluded: false, if true check utilities length > 0
      utilitiesList: [
        {name: 'Air conditioning', selected: false},
        {name: 'Trash removal', selected: false},
        {name: 'Electricity', selected: false},
        {name: 'Gas', selected: false},
        {name: 'Cable', selected: false},
        {name: 'Sewer', selected: false},
        {name: 'Heat', selected: false},
        {name: 'Water', selected: false},
      ],
      photos: [], length > 0
      videos: [], length > 0
      links: [], length > 0
      unitAmenities: [],
      unitDescription: '', length > 0

*/

export const validateDraft = (draft) => {
  const {units, ...property} = draft;
  // Property validation
  const media = [...property.photos, ...property.videos, property.links];
  if (media.length === 0) {
    return false;
  }
  if (property.propertyDescription.length === 0) {
    return false;
  }
  // Units validation
  if (validateUnits(units, property.unitsType)) {
    return false;
  }
  return true;
};

const validateUnits = (units, unitsType) => {
  const units_validation = [...units].map((unit) => {
    const validation_result = {
      name: unit.unitName,
      is_valid: true
    };
    if (
        unit.unitName.length === 0 ||
        unit.area.length === 0 ||
        unit.price.length === 0
    ) {
      validation_result.is_valid = false;
      return validation_result;
    }
    if (unit.utilitiesIncluded && unit.utilitiesList.length === 0) {
      validation_result.is_valid = false;
      return validation_result;
    }
    if (unit.rentOptions === 'By Bedroom' && !unit.splitTheCosts) {
      const is_price_completed = unit.price_per_bedroom.every((bedroom) => bedroom.price.length > 0);
      if (!is_price_completed) {
        validation_result.is_valid = false;
        return validation_result;
      }
    }
    if (unitsType === 'Multi') {
      const media = [...unit.photos, ...unit.videos, unit.links];
      if (media.length === 0) {
        validation_result.is_valid = false;
        return validation_result;
      }
    }
    return validation_result;
  });

  return units_validation.some((unit) => unit.is_valid === false);
};