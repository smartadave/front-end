export const formatPrice = (value) => {
  const newValue = value
    ? value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    : '';
  return `$ ${newValue}`;
};

export const formatPriceRange = (value) => {
  const minPrice = formatPrice(Math.min(...value.map((el) => +el.price)));
  const maxPrice = formatPrice(Math.max(...value.map((el) => +el.price)));
  return `${minPrice} - ${maxPrice}`;
};
