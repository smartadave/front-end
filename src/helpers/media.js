import http from 'utilities/http';

export const createFileForImages = (links) => {
  return new Promise(async (res, reject) => {
    if (links.length === 0) {
      return  res(false);
    }
    let formData = new FormData();

    const getValidData = new Promise((resolve, rej) => {
      const not_upload_files = links.map((link) => {
        if (link.indexOf('blob') === 0) {
          return link;
        }
      });

      not_upload_files.map(async (link, key) => {
        if (link.indexOf('blob') === 0) {
          fetch(link).then((response) => {
            response.blob().then((data) => {
              let file = new File([data], link);
              formData.append('upload', file);
              if (key === not_upload_files.length - 1) {
                resolve(formData);
              }
            });
          });
        }
      });
    });

    getValidData.then((resp) => {
      if (resp) {
        return http
          .post('s3/upload/photo', formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          })
          .then((data) => {
            res(data.data);
          })
          .catch((error) => {
            reject(false);
          });
      }
    });
  });
};

export const createFileForVideos = (links) => {
  return new Promise(async (res, reject) => {
    let formData = new FormData();

    const getValidData = new Promise((resolve, rej) => {
      links.map(async (link, key) => {
        if (link.indexOf('blob') === 0) {
          fetch(link).then((response) => {
            response.blob().then((data) => {
              let file = new File([data], link);
              formData.append('upload', file);
              if (key === links.length - 1) {
                resolve(formData);
              }
            });
          });
        }
      });
    });

    getValidData.then((resp) => {
      if (resp) {
        return http
          .post('s3-upload/photo', formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          })
          .then((data) => {
            res(data.data);
          })
          .catch((error) => {
            reject(false);
          });
      }
    });
  });
};
