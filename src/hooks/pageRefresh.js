import {useEffect} from 'react';

export const useRefresh = (skipRefresh) => {
  useEffect(() => {
    if (skipRefresh) {
      return;
    }
    const reloadHandler = (event) => {
      const e = event || window.event;
      e.preventDefault();
      if (e) {
        e.returnValue = '';
      }
      return '';
    };
    window.addEventListener('beforeunload', reloadHandler);

    return () => {
      window.removeEventListener('beforeunload', reloadHandler);
    };
  }, [skipRefresh]);
};
