import {useState, useEffect} from 'react';

export const useWindowSize = () => {
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
    screen_type: '',
  });
  useEffect(() => {
    function handleResize() {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
        screen_type: returnScreenType(window.innerWidth),
      });
    }

    function returnScreenType(width) {
      if (width <= 480) {
        return 'mobile';
      } else if (width <= 768) {
        return 'tablet';
      } else if (width <= 1024) {
        return 'sm-desktop';
      } else if (width <= 1200) {
        return 'desktop';
      } else {
        return 'lg-desktop';
      }
    }

    window.addEventListener('resize', handleResize);
    handleResize();
    return () => window.removeEventListener('resize', handleResize);
  }, []);
  return windowSize;
};
