import React from 'react';
import ReactDOM from 'react-dom';
import './styles/app.scss';
import {Provider} from 'react-redux';
import store from './store';
import App from './App';
import reportWebVitals from './reportWebVitals';

let dotenv = require('dotenv');
let dotenvExpand = require('dotenv-expand');
let myEnv = dotenv.config();
dotenvExpand(myEnv);

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
        <App />
      </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

reportWebVitals();
