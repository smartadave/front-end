import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import './styles.scss';
import PropertyMainInfo from 'components/PropertySections/PropertyMainInfo';
import BedroomsBathrooms from 'components/PropertySections/BedroomsBathrooms';
import checkForm from 'utilities/validator';
import {
  initPropertyState,
  submitFirstStep,
  updateSteps,
} from 'store/actions/propertyActions';
import Selector from 'components/Selector';
import {onLeaveCreateProperty} from '../../../helpers/createProperty';
import PromptModal from 'components/PromptModal';
import {useRefresh} from 'hooks/pageRefresh';

function AddPropertyStart({history}) {
  const [state, setState] = useState({
    unitsType: 'Single',
    address: {},
    propertyType: 'Apartment complex',
    propertyName: '',
    bedrooms: '',
    bathrooms: '',
    campuses: [],
    showModal: false,
  });

  const campuses = useSelector((state) => state.constants.campuses);
  const _campuses =
    campuses &&
    campuses.map((campus) => {
      campus.value = campus.id;
      campus.name = campus.campus;
      return campus;
    });

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(initPropertyState());
  }, []);

  useRefresh();

  const onSubmit = (e) => {
    e.preventDefault();
    const data = {...state};
    if (checkForm(e.target.form)) {
      const update_step = {
        step1: {
          active: false,
          completed: true,
        },
      };
      dispatch(submitFirstStep(data));
      dispatch(updateSteps(update_step));
      history.push('/add-property/property-information');
    }
  };

  const selectUnitsType = (unitsType) => {
    setState({
      ...state,
      unitsType,
    });
  };

  const handleInputChange = ({target}) => {
    setState({
      ...state,
      [target.name]: target.value,
    });
  };

  const handleChangeCampuses = (campuses) => {
    setState({
      ...state,
      campuses: campuses,
    });
  };

  const returnAddressObject = (obj) => {
    setState({
      ...state,
      address: obj,
    });
  };

  return (
    <>
      <PromptModal
        when={state.showModal}
        navigate={(path) => {
          history.push(path);
        }}
        shouldBlockNavigation={(location) => {
          return !onLeaveCreateProperty(location);
        }}
      />
      <div className="add-property-start">
        <header>
          <h1>Add your property</h1>
          <div className="unit-options-btns">
            <button
              className={`btn single-unit-btn ${
                state.unitsType === 'Single' ? 'active' : ''
              }`}
              onClick={() => selectUnitsType('Single')}>
              Single Unit
            </button>
            <button
              className={`btn single-multi-btn ${
                state.unitsType === 'Multi' ? 'active' : ''
              }`}
              onClick={() => selectUnitsType('Multi')}>
              Multi Units
            </button>
          </div>
        </header>
        <form
          className="add-property-start__form"
          noValidate="novalidate"
          autoComplete="off">
          <PropertyMainInfo
            propertyInfo={state}
            handleInputChange={handleInputChange}
            returnAddressObject={returnAddressObject}
          />
          <Selector
            label="School Campuses"
            name="campuses"
            options={_campuses}
            value={state.campuses}
            handleMultiInputChange={handleChangeCampuses}
            multiselect
          />
          {state.unitsType === 'Single' && (
            <div className="bnb-inputs">
              <BedroomsBathrooms
                unitInfo={state}
                handleInputChange={handleInputChange}
              />
            </div>
          )}
          <button
            className="btn add-property-btn"
            type="submit"
            onClick={onSubmit}>
            Add My Property
          </button>
        </form>
      </div>
    </>
  );
}

export default AddPropertyStart;
