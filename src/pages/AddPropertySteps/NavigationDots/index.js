import React from 'react';
import './styles.scss';

function NavigationDots({steps, unitsType}) {
  const {step3, ...filteredSteps} = steps;
  const updatedSteps = unitsType === 'Single' ? filteredSteps : steps;
  return (
    <div className="navigation-dots">
      <ul>
        {Object.keys(updatedSteps).map((step, i) => (
          <li
            className={`${steps[step].completed ? 'completed' : ''}${
              steps[step].active ? ' active' : ''
            }`}
            key={i}
          />
        ))}
      </ul>
    </div>
  );
}

export default NavigationDots;
