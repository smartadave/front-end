import React, {useState, useEffect, useRef} from 'react';
import './styles.scss';
import {useSelector, useDispatch} from 'react-redux';
import NavigationButtons from 'components/PropertySections/NavigationButtons';
import {
  publishProperty,
  saveToDrafts,
  updateSteps,
} from 'store/actions/propertyActions';
import MediaCarousel from 'components/MediaCarousel';
import Amenities from 'components/PropertySections/Amenities';
import CampusesDistance from 'components/PropertySections/CampusesDistance';
import {nanoid} from 'nanoid';
import {getPropertyFullInfo} from 'store/actions/propertyListActions';
import {getUserInfo} from 'store/actions/userActions';
import {useLocation} from 'react-router-dom';
import ToolTip from 'components/Tooltip';
import Selector from 'components/Selector';
import {handleFavoritesProperty} from 'store/actions/userActions';
import {isFavoriteProperty} from 'helpers/property';
import NavigationDots from 'pages/AddPropertySteps/NavigationDots';
import {onLeaveCreateProperty} from 'helpers/createProperty';
import PromptModal from 'components/PromptModal';
import {useRefresh} from 'hooks/pageRefresh';
import ConfirmModal from 'components/ConfirmModal/confirmModal';
import {createFileForImages} from 'helpers/media';
import UnitsTable from 'components/UnitsTable';
import ShareProperty from 'components/ShareProperty';
import {useOnClickOutside} from 'hooks/clickOutside';
import {getCookie} from 'utilities/cookie';
import {applyToProperty} from 'store/middleware/socketMiddleware';
import {createChatWithUsers} from 'store/actions/conversationActions';
import {useWindowSize} from 'hooks/windowSize';
import UnitsBlockMobile from 'components/UnitsBlockMobile';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function PreviewProperty({history, viewType = 'list-view'}) {
  const [state, setState] = useState({
    unitsType: '',
    address: '',
    coordinates: '',
    propertyType: '',
    propertyName: '',
    units: [],
    photos: [],
    videos: [],
    links: [],
    propertyAmenities: [],
    propertyDescription: '',
    neighbourhood: '',
    distance: [],
    showOptions: 'Meet My Criteria',
    like: false,
    likeHover: false,
    showRentOptions: 'Entire units',
    isLoaded: false,
    skipRefresh: false,
    show_drafts_modal: false,
    show_publish_modal: false,
    block_navigation: true,
    show_share: false,
    property_manager_id: '',
    property_id: '',
  });

  const query = useQuery();
  const propertyStore = useSelector((state) => state.property);
  const user = useSelector((state) => state.user);
  const auth_user = getCookie('user');
  const dispatch = useDispatch();
  const share_ref = useRef(null);
  const window_size = useWindowSize();

  useOnClickOutside(share_ref, () => {
    setState({
      ...state,
      show_share: false,
    });
  });

  useEffect(() => {
    initState();
  }, []);

  useEffect(() => {
    const in_progress = propertyStore.in_progress;
    if (viewType === 'create-preview' && !in_progress) {
      window.location = '/add-property/create-property';
    }
  }, [state.isLoaded]);

  useEffect(() => {
    if (
      state.isLoaded &&
      user.favouriteProperties &&
      user.favouriteProperties.length > 0
    ) {
      setState({
        ...state,
        like: isFavoriteProperty(user.favouriteProperties, query.get('id')),
      });
    }
  }, [state.isLoaded]);

  useEffect(() => {
    if (viewType === 'create-preview') {
      const unlisten = history.listen((location, action) => {
        if (action === 'POP') {
          const update_step = {
            step4: {
              active: false,
              completed: propertyStore.steps.step4.completed,
            },
          };
          dispatch(updateSteps(update_step));
        }
      });

      return () => {
        unlisten();
      };
    }
  });

  useRefresh(viewType === 'list-view');

  const initState = async () => {
    const {units, ...property} =
      viewType === 'create-preview'
        ? propertyStore
        : await dispatch(getPropertyFullInfo(query.get('id')));
    property.address = property.address.formatted_address;
    property.propertyName =
      property.propertyName.length === 0
        ? propertyStore.address.name
        : property.propertyName;
    property.propertyAmenities = [...property.propertyAmenities].map(
      (amenity) => ({
        name: amenity,
        icon_name: amenity.toLowerCase().split(' ').join('_'),
      }),
    );
    property.showRentOptions =
      units.length === 1 && units[0].rentOptions === 'By Bedroom'
        ? 'Bedrooms'
        : 'Entire units';

    if (viewType === 'list-view') {
      property.listed_by = await dispatch(
        getUserInfo(property.userId, 'manager'),
      );
      property.filter_criteria = JSON.parse(
        sessionStorage.getItem('filter'),
      ).filtering;
    } else {
      property.listed_by = user;
    }

    const unitsData = JSON.parse(JSON.stringify(units));

    unitsData.forEach((unit) => {
      unit.id = viewType === 'create-preview' ? nanoid() : unit._id;
      unit.mode = 'view';
      unit.unitAmenities =
        unit.unitAmenities.length > 0
          ? [...unit.unitAmenities].map((amenity) => ({
              name: amenity,
              icon_name: amenity.toLowerCase().split(' ').join('_'),
            }))
          : [...unit.unitAmenities];
      unit.expandUnitInfo = false;
      //TODO Remove after applying upload images for units
      // unit.photos = [...property.photos];
    });
    setState({
      ...state,
      ...property,
      units: [...unitsData],
      isLoaded: true,
      skip_refresh: viewType === 'list-view',
    });
    if (viewType === 'create-preview') {
      const update_step = {
        step4: {
          active: true,
          completed: propertyStore.steps.step4.completed,
        },
      };
      dispatch(updateSteps(update_step));
    }
  };

  const toggleHover = () => {
    setState({
      ...state,
      likeHover: !state.likeHover,
    });
  };

  const showOptionsHandler = ({target}) => {
    setState({
      ...state,
      showOptions: target.value,
    });
  };

  const toggleExpandUnitInfo = (unit_id) => {
    const units = [...state.units];
    const unitIndex = units.findIndex((unit) => unit.id === unit_id);
    units[unitIndex].expandUnitInfo = !units[unitIndex].expandUnitInfo;
    setState({
      ...state,
      units,
    });
  };

  const toggleShowRentOptions = ({target}) => {
    setState({
      ...state,
      [target.name]: target.value,
    });
  };

  const goToPreviousStepHandler = () => {
    const update_step = {
      step4: {
        active: false,
        completed: propertyStore.steps.step4.completed,
      },
    };
    dispatch(updateSteps(update_step));
    history.goBack();
  };

  const saveToDraftsHandler = () => {
    setState({
      ...state,
      show_drafts_modal: true,
      block_navigation: false,
    });
  };

  const saveToDraft = () => {
    const {current_step, steps, ...data} = propertyStore;
    data.propertyName =
      data.propertyName.length > 0 ? data.propertyName : data.address.name;
    createFileForImages(data.photos).then((resp) => {
      if (resp) {
        data.photos = [...resp.data];
        dispatch(saveToDrafts(data, history));
      } else dispatch(saveToDrafts(data, history));
    });
  };

  const cancelSaveToDraft = () => {
    setState({
      ...state,
      show_drafts_modal: false,
      block_navigation: true,
    });
  };

  const publishPropertyHandler = () => {
    setState({
      ...state,
      show_publish_modal: true,
      block_navigation: false,
      skipRefresh: true,
    });
  };

  const onPublishProperty = () => {
    const {current_step, steps, ...data} = propertyStore;
    data &&
      createFileForImages(data.photos).then((resp) => {
        if (resp) {
          data.photos = [...resp.data];
          data.propertyName =
            data.propertyName.length > 0
              ? data.propertyName
              : data.address.name;
          dispatch(publishProperty(data, history));
        }
      });
  };

  const cancelPublishProperty = () => {
    setState({
      ...state,
      show_publish_modal: false,
      block_navigation: true,
    });
  };

  const formUnitsToDisplay = () => {
    let filtered_units = JSON.parse(JSON.stringify(state.units));
    if (state.showOptions === 'Meet My Criteria' && viewType === 'list-view') {
      //bedrooms: {min: Num, max: Num}
      // price: {min: Num, max: Num}
      // square_feet: {min: Num, max: Num}
      // utilities: Bool
      // unit_amenities: [String]
      filtered_units = [...filtered_units].filter((unit) => {
        return (
          unit.bedrooms >= state.filter_criteria.bedrooms.min &&
          unit.bedrooms <= state.filter_criteria.bedrooms.max &&
          unit.price >= state.filter_criteria.price.min &&
          unit.price <= state.filter_criteria.price.max &&
          unit.area >= state.filter_criteria.square_feet.min &&
          unit.area <= state.filter_criteria.square_feet.max
        );
      });
      if (state.filter_criteria.utilities) {
        filtered_units = [...filtered_units].filter(
          (unit) => unit.utilitiesIncluded,
        );
      }
      if (state.filter_criteria.unit_amenities.length > 0) {
        const acceptable_units = [...state.units].map((unit) => {
          const unit_amenities = unit.unitAmenities.map(
            (amenity) => amenity.name,
          );
          const includes_amenities = state.filter_criteria.unit_amenities.every(
            (amenity) => unit_amenities.includes(amenity),
          );
          if (includes_amenities) {
            return unit.id;
          }
        });
        filtered_units = [...filtered_units].filter((unit) =>
          acceptable_units.includes(unit.id),
        );
      }
    }
    return state.showRentOptions === 'Bedrooms'
      ? [...filtered_units].filter((unit) => unit.rentOptions === 'By Bedroom')
      : filtered_units;
  };

  const unitsToDisplay = state.isLoaded && formUnitsToDisplay();

  const toggleLike = async (type) => {
    if (viewType === 'list-view') {
      dispatch(handleFavoritesProperty(query.get('id'), type)).then((resp) => {
        if (resp) {
          setState({
            ...state,
            like: type === 'remove' ? false : true,
          });
        }
      });
    } else {
      setState({
        ...state,
        like: type === 'remove' ? false : true,
      });
    }
  };

  const sendRequestToApplyToProperty = (type, id) => {
    const data = {
      _id: id,
      type: type,
      room: {
        name: 'Conversation',
        description: 'Chat',
        is_user: true,
        is_private: true,
        users: [state.userId, auth_user.id],
        messages: [],
      },
    };
    applyToProperty(data);
  };

  const sendMessageToManager = () => {
    if (viewType === 'list-view') {
      const data = {
        name: 'Conversation',
        description: 'Chat',
        is_user: true,
        is_private: true,
        users: [state.userId, auth_user.id],
        messages: [],
      };
      dispatch(createChatWithUsers(data));
    }
  };

  return (
    state.isLoaded && (
      <>
        {viewType === 'create-preview' && (
          <>
            <PromptModal
              when={viewType === 'create-preview'}
              text="You can save property as a draft and continue later"
              navigate={(path) => {
                history.push(path);
              }}
              shouldBlockNavigation={(location) => {
                return (
                  state.block_navigation && !onLeaveCreateProperty(location)
                );
              }}
            />
            <ConfirmModal
              modal_visible={
                state.show_drafts_modal || state.show_publish_modal
              }
              title={`Would you like to 
            ${
              state.show_publish_modal
                ? 'publish this property'
                : 'save this property as draft'
            } 
            and leave?`}
              text={
                state.show_drafts_modal &&
                'You can continue property creation process from My Properties at any time'
              }
              confirmAction={
                state.show_publish_modal ? onPublishProperty : saveToDraft
              }
              cancelAction={
                state.show_publish_modal
                  ? cancelPublishProperty
                  : cancelSaveToDraft
              }
            />
          </>
        )}
        {state.isLoaded && (
          <div
            className={viewType === 'create-preview' ? 'content-wrapper' : ''}>
            {viewType === 'create-preview' && (
              <NavigationDots
                unitsType={state.unitsType}
                steps={propertyStore.steps}
              />
            )}
            <div className={`listing-preview wrapper-container ${viewType}`}>
              {viewType === 'create-preview' && (
                <header>
                  <h1>Listing Preview</h1>
                </header>
              )}
              <div className="listing-preview__content">
                <div className="content-header">
                  <div className="main-info">
                    <h2>
                      {state.propertyName.length > 0
                        ? state.propertyName
                        : state.address.name}
                    </h2>
                    <p>{state.address}</p>
                  </div>
                  {(auth_user.role === 'student' ||
                    viewType === 'create-preview') && (
                    <div className="actions">
                      <ToolTip content="Share property">
                        <img
                          className="share-button"
                          alt="share"
                          src="/images/share.svg"
                          onClick={() =>
                            setState({
                              ...state,
                              show_share: true,
                            })
                          }
                        />
                      </ToolTip>
                      {viewType === 'list-view' && state.show_share && (
                        <div ref={share_ref}>
                          <ShareProperty
                            propertyId={state._id}
                            userId={user._id}
                            closeWindow={() => {
                              setState({
                                ...state,
                                show_share: false,
                              });
                            }}
                          />
                        </div>
                      )}
                      <ToolTip content="Save to favourites">
                        <img
                          onClick={() =>
                            toggleLike(state.like ? 'remove' : 'add')
                          }
                          className="save-to-fav-button"
                          alt="save_to_fav"
                          src={
                            state.like
                              ? '/images/heart.svg'
                              : state.likeHover
                              ? '/images/heart.svg'
                              : '/images/unheart.svg'
                          }
                          onMouseEnter={toggleHover}
                          onMouseLeave={toggleHover}
                        />
                      </ToolTip>
                      <div className="author-block">
                        <button onClick={sendMessageToManager} className="btn">
                          Message Author
                        </button>
                        <span className="author">
                          listed by: {state.listed_by.firstName}{' '}
                          {state.listed_by.lastName.split('')[0]}.
                        </span>
                      </div>
                    </div>
                  )}
                </div>
                <MediaCarousel mediaList={state.photos} />
                <div className="units-info">
                  {state.unitsType === 'Multi' && (
                    <div
                      className={`options-controls${
                        window_size.width < 768 ? ' mobile' : ''
                      }`}>
                      <div className="rent-options">
                        <button
                          className={`rent-option${
                            state.showRentOptions === 'Entire units'
                              ? ' active'
                              : ''
                          }`}
                          name="showRentOptions"
                          type="button"
                          value="Entire units"
                          onClick={toggleShowRentOptions}>
                          Entire units
                        </button>
                        <button
                          className={`rent-option${
                            state.showRentOptions === 'Bedrooms'
                              ? ' active'
                              : ''
                          }`}
                          name="showRentOptions"
                          type="button"
                          value="Bedrooms"
                          onClick={toggleShowRentOptions}>
                          Bedrooms
                        </button>
                      </div>
                      <div className="select-block">
                        <label
                          htmlFor="criteria"
                          className="select-block__label">
                          Show options:
                        </label>
                        <Selector
                          name="criteria"
                          value={state.showOptions}
                          handleInputChange={showOptionsHandler}
                          options={[
                            {
                              name: 'Meet My Criteria',
                              value: 'Meet My Criteria',
                            },
                            {name: 'All Available', value: 'All Available'},
                          ]}
                        />
                      </div>
                    </div>
                  )}
                  {window_size.width >= 768 ? (
                    <UnitsTable
                      viewType={viewType}
                      propertyId={state._id}
                      type="view"
                      showRentOptions={state.showRentOptions}
                      editable={false}
                      units={unitsToDisplay}
                      sendRequestToApplyToProperty={
                        sendRequestToApplyToProperty
                      }
                      unitsType={state.unitsType}
                      toggleExpandUnitInfo={toggleExpandUnitInfo}
                    />
                  ) : (
                    <UnitsBlockMobile
                      viewType={viewType}
                      propertyId={state._id}
                      type="view"
                      showRentOptions={state.showRentOptions}
                      editable={false}
                      units={unitsToDisplay}
                      sendRequestToApplyToProperty={
                        sendRequestToApplyToProperty
                      }
                      unitsType={state.unitsType}
                      toggleExpandUnitInfo={toggleExpandUnitInfo}
                    />
                  )}
                </div>
                <CampusesDistance
                  propertyAddress={propertyStore.address}
                  campuses={propertyStore.campuses}
                  previewMode
                  distance={state.distance}
                  title="Distance to Campus"
                />
                <div className="property-description">
                  <h3>About {state.propertyName}</h3>
                  <span>{state.propertyDescription}</span>
                </div>
                {state.propertyAmenities.length > 0 && (
                  <Amenities
                    amenityType="Property"
                    amenities={state.propertyAmenities}
                  />
                )}
              </div>
              {viewType === 'create-preview' && (
                <NavigationButtons
                  currentStep={4}
                  goToPreviousStep={goToPreviousStepHandler}
                  saveToDraft={saveToDraftsHandler}
                  onSubmit={publishPropertyHandler}
                />
              )}
            </div>
          </div>
        )}
      </>
    )
  );
}

export default PreviewProperty;
