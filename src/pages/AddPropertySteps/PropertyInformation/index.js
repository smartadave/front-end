import React, {useEffect, useState} from 'react';
import './styles.scss';
import {useDispatch, useSelector} from 'react-redux';
import PropertyMainInfo from 'components/PropertySections/PropertyMainInfo';
import UnitMainInfo from 'components/PropertySections/UnitMainInfo';
import UploadMedia from 'components/UploadMedia';
import Amenities from 'components/PropertySections/Amenities';
import NavigationButtons from 'components/PropertySections/NavigationButtons';
import checkForm from 'utilities/validator';
import {
  fetchAmenities,
  saveToDrafts,
  submitPropertyInfoStep,
  updateSteps,
} from 'store/actions/propertyActions';
import {NotificationManager} from 'react-notifications';
import CampusesDistance from 'components/PropertySections/CampusesDistance';
import NavigationDots from 'pages/AddPropertySteps/NavigationDots';
import PromptModal from 'components/PromptModal';
import {onLeaveCreateProperty} from 'helpers/createProperty';
import {useRefresh} from 'hooks/pageRefresh';
import ConfirmModal from 'components/ConfirmModal/confirmModal';
import {createFileForImages} from 'helpers/media';

function PropertyInformation({history}) {
  const propertyStore = useSelector((state) => state.property);
  const dispatch = useDispatch();

  const [state, setState] = useState({
    unitsType: 'Single',
    address: {},
    coordinates: '',
    propertyType: '',
    unit: {
      id: 1,
      unitName: '',
      area: '',
      bedrooms: 1,
      bathrooms: 1,
      price: '',
      rentOptions: 'Entirely',
      splitTheCosts: false,
      price_per_bedroom: [],
      utilitiesIncluded: false,
      utilitiesList: [
        {name: 'Air conditioning', selected: false},
        {name: 'Trash removal', selected: false},
        {name: 'Electricity', selected: false},
        {name: 'Gas', selected: false},
        {name: 'Cable', selected: false},
        {name: 'Sewer', selected: false},
        {name: 'Heat', selected: false},
        {name: 'Water', selected: false},
      ],
      unitAmenities: [],
      unitDescription: ' ',
      photos: [],
      videos: [],
      links: [],
    },
    propertyName: '',
    photos: [],
    videos: [],
    links: [],
    campuses: [],
    propertyAmenities: [],
    propertyDescription: '',
    neighbourhood: '',
    distance: [],
    show_modal: false,
    isLoaded: false,
    block_navigation: true,
  });

  const initState = async () => {
    const {steps, current_step, ...storeData} = propertyStore;
    const storeUnit = {...storeData.units[0]};
    const unit = {...state.unit};
    const propertyAmenities = await dispatch(fetchAmenities('property'));
    propertyAmenities.forEach(
      (amenity) =>
        (amenity.selected = propertyStore.propertyAmenities.some(
          (name) => name === amenity.name,
        )),
    );
    const utilitiesList =
      Object.entries(storeUnit).length > 2
        ? unit.utilitiesList.map((utility) => ({
            name: utility.name,
            selected: storeUnit.utilitiesList.some((el) => el === utility.name),
          }))
        : unit.utilitiesList;
    const update_step = {
      step2: {
        active: true,
        completed: propertyStore.steps.step2.completed,
      },
    };
    setState({
      ...state,
      ...storeData,
      propertyAmenities,
      unit: {
        ...unit,
        ...storeUnit,
        utilitiesList,
      },
      isLoaded: true,
    });
    dispatch(updateSteps(update_step));
  };

  useEffect(() => {
    const in_progress = propertyStore.in_progress;
    if (!in_progress && state.isLoaded) {
      window.location = '/add-property/create-property';
    }
  }, [state.isLoaded]);

  useEffect(() => {
    initState();
  }, []);

  useEffect(() => {
    if (Object.keys(state.address).length !== 0) {
      const data = formData();
      dispatch(submitPropertyInfoStep(data));
    }
  }, [state]);

  useEffect(() => {
    const unlisten = history.listen((location, action) => {
      if (!onLeaveCreateProperty(location)) {
        return setState({
          ...state,
          show_modal: true,
        });
      }
      if (action === 'POP' || action === 'PUSH') {
        const update_step = {
          step2: {
            active: false,
            completed:
              action === 'PUSH' ? true : propertyStore.steps.step2.completed,
          },
        };
        dispatch(updateSteps(update_step));
      }
    });

    return () => {
      unlisten();
    };
  });

  useRefresh();

  const handlePropertyInputChange = ({target}) => {
    let price_per_bedroom = [];
    if (target.name === 'price' && state.unit.price_per_bedroom.length > 0) {
      price_per_bedroom = calculateBedroomsPrice(
        state.unit.splitTheCosts,
        [...state.unit.price_per_bedroom],
        target.value,
        state.unit.bedrooms,
      );
    }

    setState({
      ...state,
      [target.name]: target.value,
    });
  };

  const handleUnitInputChange = ({target}) => {
    let price_per_bedroom = [...state.unit.price_per_bedroom];
    if (target.name === 'bedrooms') {
      if (price_per_bedroom.length === 0) {
        price_per_bedroom = formPricePerBedroomArray(+target.value, 1);
      } else if (price_per_bedroom.length < +target.value) {
        const difference = Number(target.value) - price_per_bedroom.length;
        const newBedrooms = formPricePerBedroomArray(
          difference,
          price_per_bedroom.length + 1,
        );
        price_per_bedroom = [...price_per_bedroom, ...newBedrooms];
      } else if (price_per_bedroom.length > +target.value) {
        price_per_bedroom = price_per_bedroom.slice(0, +target.value);
      }
      price_per_bedroom = calculateBedroomsPrice(
        state.unit.splitTheCosts,
        [...price_per_bedroom],
        +state.unit.price,
        +target.value,
      );
    }
    if (target.name === 'price' && price_per_bedroom.length > 0) {
      price_per_bedroom = calculateBedroomsPrice(
        state.unit.splitTheCosts,
        [...price_per_bedroom],
        +target.value,
        +state.unit.bedrooms,
      );
    }
    setState({
      ...state,
      unit: {
        ...state.unit,
        [target.name]: target.value,
        price_per_bedroom,
      },
    });
  };

  const toggleUtilitiesIncluded = () => {
    const utilitiesIncluded = !state.unit.utilitiesIncluded;
    const utilities = [...state.unit.utilitiesList].map((el) => ({...el}));
    if (!utilitiesIncluded) {
      utilities.forEach((utility) => (utility.selected = false));
    }
    setState({
      ...state,
      unit: {
        ...state.unit,
        utilitiesIncluded: !state.unit.utilitiesIncluded,
        utilitiesList: utilities,
      },
    });
  };

  const toggleUtility = (name) => {
    const utilities = [...state.unit.utilitiesList];
    const utilityIndex = utilities.findIndex(
      (utility) => utility.name === name,
    );
    utilities[utilityIndex].selected = !utilities[utilityIndex].selected;
    setState({
      ...state,
      unit: {
        ...state.unit,
        utilitiesList: utilities,
      },
    });
  };

  const setRentOption = ({target}) => {
    const new_value =
      target.value !== state.unit.rentOptions ? target.value : 'Entirely';
    const price_per_bedroom =
      new_value === 'By Bedroom'
        ? formPricePerBedroomArray(Number(state.unit.bedrooms), 1)
        : [];
    setState({
      ...state,
      unit: {
        ...state.unit,
        rentOptions: new_value,
        price_per_bedroom,
      },
    });
  };

  const toggleSplitEvenly = () => {
    const splitTheCosts = !state.unit.splitTheCosts;
    const price_per_bedroom = calculateBedroomsPrice(
      splitTheCosts,
      [...state.unit.price_per_bedroom],
      +state.unit.price,
      +state.unit.bedrooms,
    );
    setState({
      ...state,
      unit: {
        ...state.unit,
        splitTheCosts,
        price_per_bedroom,
      },
    });
  };

  const setBedroomPrice = ({target}) => {
    const price_per_bedroom = [...state.unit.price_per_bedroom];
    const updatedBedroom = price_per_bedroom.find(
      (bedroom) => bedroom.name === target.name,
    );
    updatedBedroom.price = target.value;
    setState({
      ...state,
      unit: {
        ...state.unit,
        price_per_bedroom,
      },
    });
  };

  const toggleAmenity = (id) => {
    const amenities = [...state.propertyAmenities];
    const updatedAmenity = amenities.find((amenity) => amenity._id === id);
    updatedAmenity.selected = !updatedAmenity.selected;
    setState({
      ...state,
      propertyAmenities: amenities,
    });
  };

  const formPricePerBedroomArray = (number, start) => {
    return new Array(number).fill(null).map((bedroom, i) => ({
      name: `Bedroom ${start + i}`,
      price: '',
    }));
  };

  const calculateBedroomsPrice = (
    splitTheCosts,
    bedroomsArray,
    price,
    bedrooms,
  ) => {
    const price_per_bedroom = bedroomsArray.map((bedroom) => {
      return {
        name: bedroom.name,
        price: splitTheCosts ? Math.round(price / bedrooms) : '',
      };
    });
    return price_per_bedroom;
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target.form)) {
      const data = formData();
      const update_step = {
        step2: {
          completed: true,
          active: false,
        },
      };
      dispatch(submitPropertyInfoStep(data));
      dispatch(updateSteps(update_step));
      history.push(
        state.unitsType === 'Multi'
          ? '/add-property/unit-information'
          : '/add-property/preview-property',
      );
    } else {
      NotificationManager.error('Please fill all mandatory fields');
    }
  };

  const saveToDraftHandler = () => {
    setState({
      ...state,
      show_modal: true,
      block_navigation: false,
    });
  };

  const saveToDraft = () => {
    const data = formData();
    createFileForImages(data.photos).then((resp) => {
      if (resp) {
        data.photos = [...resp.data];
        dispatch(saveToDrafts(data, history));
      } else dispatch(saveToDrafts(data, history));
    });
  };

  const cancelSaveToDraft = () => {
    setState({
      ...state,
      show_modal: false,
      block_navigation: true,
    });
  };

  const formData = () => {
    const {
      show_modal,
      isLoaded,
      unit,
      propertyAmenities,
      ...propertyData
    } = state;
    const amenities = [...propertyAmenities]
      .filter((amenity) => amenity.selected)
      .map((amenity) => amenity.name);
    const {id, ...unitData} = unit;
    const utilitiesList = [...unitData.utilitiesList]
      .filter((utility) => utility.selected)
      .map((utility) => utility.name);
    const units =
      propertyData.unitsType === 'Single'
        ? [
            {
              ...unitData,
              utilitiesList: utilitiesList,
            },
          ]
        : [...propertyStore.units];
    return {
      ...propertyData,
      propertyAmenities: amenities,
      units,
    };
  };

  const returnMediaFiles = async (media) => {
    if (media.images.length > 0) {
      setState({
        ...state,
        photos: media.images,
      });
    }
    if (media.videos.length > 0) {
      setState({
        ...state,
        videos: media.videos,
      });
    }
  };

  const returnDistance = (distance) => {
    if (distance.length > 0) {
      setState({
        ...state,
        distance,
      });
    }
  };

  return (
    state.isLoaded && (
      <div className="content-wrapper">
        <PromptModal
          text="You can save property as a draft and continue later"
          navigate={(path) => {
            history.push(path);
          }}
          shouldBlockNavigation={(location) => {
            return state.block_navigation && !onLeaveCreateProperty(location);
          }}
        />
        <ConfirmModal
          modal_visible={state.show_modal}
          title="Would you like to save this property as draft and leave?"
          text="You can continue property creation process from My Properties at any time"
          confirmAction={saveToDraft}
          cancelAction={cancelSaveToDraft}
        />
        <NavigationDots
          unitsType={state.unitsType}
          steps={propertyStore.steps}
        />
        <div className="property-information wrapper-container">
          <header>
            <h1>{state.unitsType === 'Multi' && 'General'} Property info</h1>
          </header>
          <form className="property-information__form">
            <div className="property-main-info">
              <PropertyMainInfo
                propertyInfo={{
                  address: state.address,
                  propertyName: state.propertyName,
                  propertyType: state.propertyType,
                }}
                addressDisable={true}
                handleInputChange={handlePropertyInputChange}
              />
            </div>
            {state.unitsType === 'Single' && (
              <UnitMainInfo
                unitInfo={state.unit}
                handleInputChange={handleUnitInputChange}
                toggleUtilitiesIncluded={toggleUtilitiesIncluded}
                toggleUtility={toggleUtility}
                toggleSplitEvenly={toggleSplitEvenly}
                setBedroomPrice={setBedroomPrice}
                setRentOption={setRentOption}
              />
            )}
            <div className="property-information__media">
              <span>Photos / Videos</span>
              <UploadMedia
                videos={state.videos}
                photos={state.photos}
                returnMediaFiles={returnMediaFiles}
              />
            </div>
            <Amenities
              amenityType="Property"
              is_clickable={true}
              amenities={state.propertyAmenities}
              toggleAmenity={toggleAmenity}
            />

            <CampusesDistance
              propertyAddress={state.address}
              distance={state.distance}
              visibleMode
              campuses={state.campuses}
              title="Distance to Campus"
              returnDistance={returnDistance}
            />
            <div className="input property-description">
              <label htmlFor="property-description" className="input-label">
                Property description
              </label>
              <textarea
                name="propertyDescription"
                id="property-description"
                className="input-field"
                placeholder="Add your description here"
                value={state.propertyDescription}
                onChange={handlePropertyInputChange}
                required
              />
            </div>
            <NavigationButtons
              currentStep={2}
              onSubmit={onSubmit}
              saveToDraft={saveToDraftHandler}
            />
          </form>
        </div>
      </div>
    )
  );
}

export default PropertyInformation;
