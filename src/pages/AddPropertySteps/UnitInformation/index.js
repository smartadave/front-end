import React, {useState, useEffect} from 'react';
import './styles.scss';
import {useSelector, useDispatch} from 'react-redux';
import {nanoid} from 'nanoid';
import UnitMainInfo from 'components/PropertySections/UnitMainInfo';
import UploadMedia from 'components/UploadMedia';
import Amenities from 'components/PropertySections/Amenities';
import NavigationButtons from 'components/PropertySections/NavigationButtons';
import checkForm from 'utilities/validator';
import {
  handleUnitInputChangeHelper,
  setBedroomPriceHelper,
  setRentOptionHelper,
  toggleAmenityHelper,
  toggleSplitEvenlyHelper,
  toggleUtilitiesIncludedHelper,
  toggleUtilityHelper,
} from 'helpers/createProperty';
import {
  submitUnitsInfoStep,
  fetchAmenities,
  saveToDrafts,
  updateSteps,
} from 'store/actions/propertyActions';
import {NotificationManager} from 'react-notifications';
import NavigationDots from 'pages/AddPropertySteps/NavigationDots';
import {onLeaveCreateProperty} from 'helpers/createProperty';
import PromptModal from 'components/PromptModal';
import {useRefresh} from '../../../hooks/pageRefresh';
import ConfirmModal from '../../../components/ConfirmModal/confirmModal';

function UnitInformation({history}) {
  const propertyStore = useSelector((state) => state.property);
  const unitStore = useSelector((state) => state.property.units);
  const dispatch = useDispatch();

  const initialUnit = {
    unitName: '',
    area: '',
    bedrooms: '',
    bathrooms: '',
    price: '',
    rentOptions: 'Entirely',
    splitTheCosts: false,
    price_per_bedroom: [],
    utilitiesIncluded: false,
    utilitiesList: [
      {name: 'Air conditioning', selected: false},
      {name: 'Trash removal', selected: false},
      {name: 'Electricity', selected: false},
      {name: 'Gas', selected: false},
      {name: 'Cable', selected: false},
      {name: 'Sewer', selected: false},
      {name: 'Heat', selected: false},
      {name: 'Water', selected: false},
    ],
    photos: [],
    videos: [],
    links: [],
    unitAmenities: [],
    unitDescription: '',
    showMedia: false,
    showAmenities: false,
    showDescription: false,
    showDescError: false,
  };

  const [state, setState] = useState({
    unitsType: 'Multi',
    units: [],
    show_modal: false,
    block_navigation: true,
  });

  const initState = async () => {
    let units;
    const unitAmenities = await dispatch(fetchAmenities('units'));
    unitAmenities.forEach((amenity) => (amenity.selected = false));
    initialUnit.unitAmenities = unitAmenities;
    if (unitStore.length > 0 && Object.keys(unitStore[0]).length > 0) {
      units = JSON.parse(JSON.stringify(unitStore));
      units.forEach((unit) => {
        unit.id = nanoid();
        const unitIndex = unitStore.findIndex(
          (el) => el.unitName === unit.unitName,
        );
        unit.showMedia =
          unitStore[unitIndex].photos.length > 0 ||
          unitStore[unitIndex].videos.length > 0;
        unit.showAmenities = unitStore[unitIndex].unitAmenities.length > 0;
        unit.showDescription = unitStore[unitIndex].unitDescription.length > 0;
        unit.unitAmenities = unitAmenities.map((amenity) => ({
          ...amenity,
          selected: unitStore[unitIndex].unitAmenities.some(
            (name) => name === amenity.name,
          ),
        }));
        unit.utilitiesList = [...initialUnit.utilitiesList].map((utility) => ({
          ...utility,
          selected: unitStore[unitIndex].utilitiesList.some(
            (name) => name === utility.name,
          ),
        }));
      });
    } else {
      const newUnit = JSON.parse(JSON.stringify(initialUnit));
      newUnit.id = nanoid();
      units = [newUnit];
    }
    setState({
      ...state,
      units,
    });
    const update_step = {
      step3: {
        active: true,
        completed: propertyStore.steps.step3.completed,
      },
    };
    dispatch(updateSteps(update_step));
  };

  useEffect(() => {
    const in_progress = propertyStore.in_progress;

    if (!in_progress) {
      window.location = '/add-property/create-property';
    }
  }, []);

  useEffect(() => {
    initState();
  }, []);

  useEffect(() => {
    const filteredUnits = formData();
    dispatch(submitUnitsInfoStep(filteredUnits));
  }, [state]);

  useEffect(() => {
    const unlisten = history.listen((location, action) => {
      if (action === 'POP' || action === 'PUSH') {
        const update_step = {
          step3: {
            active: false,
            completed:
              action === 'PUSH' ? true : propertyStore.steps.step3.completed,
          },
        };
        dispatch(updateSteps(update_step));
      }
    });

    return () => {
      unlisten();
    };
  }, []);

  useRefresh();

  const handleInputChange = ({target}) => {
    const units = handleUnitInputChangeHelper(target, state.units);
    setState({
      ...state,
      units,
    });
  };

  const toggleUtilitiesIncluded = (id) => {
    const units = toggleUtilitiesIncludedHelper(id, state.units);
    setState({
      ...state,
      units,
    });
  };

  const toggleUtility = (name, id) => {
    const units = toggleUtilityHelper(name, id, state.units);
    setState({
      ...state,
      units,
    });
  };

  const setRentOption = ({target}) => {
    const units = setRentOptionHelper(target, state.units);
    setState({
      ...state,
      units,
    });
  };

  const toggleSplitEvenly = (id) => {
    const units = toggleSplitEvenlyHelper(id, state.units);
    setState({
      ...state,
      units,
    });
  };

  const setBedroomPrice = ({target}) => {
    const units = setBedroomPriceHelper(target, state.units);
    setState({
      ...state,
      units,
    });
  };

  const toggleAmenity = (item_id, unit_id) => {
    const units = toggleAmenityHelper(item_id, unit_id, state.units);
    setState({
      ...state,
      units,
    });
  };

  const expandSection = ({target}) => {
    const units = [...state.units];
    const unitIndex = units.findIndex((unit) => unit.id === target.id);
    units[unitIndex][target.name] = !units[unitIndex][target.name];
    setState({
      ...state,
      units,
    });
  };

  const addUnitHandler = async () => {
    const units = [...state.units];
    const newUnit = JSON.parse(JSON.stringify(initialUnit));
    newUnit.id = nanoid();
    newUnit.unitAmenities = await dispatch(fetchAmenities('units'));
    units.push(newUnit);
    setState({
      ...state,
      units,
    });
  };

  const deleteUnitHandler = ({target}) => {
    const units = [...state.units];
    const filteredUnits = units.filter((unit) => unit.id !== target.id);
    setState({
      ...state,
      units: filteredUnits,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const descriptionEntered = state.units.every(
      (unit) => unit.showDescription,
    );
    if (!descriptionEntered) {
      const descIds = state.units
        .filter((unit) => !unit.showDescription)
        .map((unit) => unit.id);
      const units = [...state.units];
      descIds.forEach((id) => {
        const unit = units.find((unit) => unit.id === id);
        unit.showDescription = true;
        unit.showDescError = true;
      });
      setState({
        ...state,
        units,
      });
    }

    if (checkForm(e.target.form)) {
      const filteredUnits = formData();
      const update_step = {
        step3: {
          active: false,
          completed: true,
        },
      };
      dispatch(updateSteps(update_step));
      dispatch(submitUnitsInfoStep(filteredUnits));
      history.push('/add-property/preview-property');
    } else {
      NotificationManager.error('Please fill all mandatory fields');
    }
  };

  const goToPreviousStepHandler = () => {
    const update_step = {
      step3: {
        active: false,
        completed: propertyStore.steps.step3,
      },
    };
    dispatch(updateSteps(update_step));
    history.goBack();
  };

  const saveToDraftHandler = () => {
    setState({
      ...state,
      show_modal: true,
      block_navigation: false,
    });
  };

  const saveToDraft = () => {
    const draftData = formData();
    const data = {
      ...draftData,
    };
    dispatch(saveToDrafts(data, history));
  };

  const cancelSaveToDraft = () => {
    setState({
      ...state,
      show_modal: false,
      block_navigation: true,
    });
  };

  const formData = () => {
    const {currentStep, steps, ...storeData} = propertyStore;
    const {units, ...rest} = state;
    const unitsData = [...units].map((unit) => {
      const {
        id,
        showMedia,
        showAmenities,
        showDescription,
        showDescError,
        ...data
      } = unit;
      data.unitAmenities = [...data.unitAmenities]
        .filter((amenity) => amenity.selected)
        .map((amenity) => amenity.name);
      data.utilitiesList = [...data.utilitiesList]
        .filter((utility) => utility.selected)
        .map((utility) => utility.name);
      return data;
    });
    return {
      ...storeData,
      units: [...unitsData],
    };
  };

  // const returnMediaFiles = async (media, unit_key) => {
  //   console.log(media, unit_key, state)
  //   if (media.images.length > 0) {
  //     setState({
  //       ...state,
  //       units: [
  //         ...state.units,
  //         units[unit_key] = {...state.units[unit_key], photos: media.images}
  //       ]
  //     });
  //   }
  //   if (media.videos.length > 0) {
  //     setState({
  //       ...state,
  //       units: [
  //         ...state.units,
  //         units[unit_key] = {...state.units[unit_key], videos: media.videos}
  //       ]
  //     });
  //   }
  // };

  return (
    <div className="content-wrapper">
      <PromptModal
        when={state.show_modal}
        text="You can save property as a draft and continue later"
        navigate={(path) => {
          history.push(path);
        }}
        shouldBlockNavigation={(location) => {
          return state.block_navigation && !onLeaveCreateProperty(location);
        }}
      />
      <ConfirmModal
        modal_visible={state.show_modal}
        title="Would you like to save this property as draft and leave?"
        text="You can continue property creation process from My Properties at any time"
        confirmAction={saveToDraft}
        cancelAction={cancelSaveToDraft}
      />
      <NavigationDots unitsType={state.unitsType} steps={propertyStore.steps} />
      <div className="units-information wrapper-container">
        <header>
          <h1>Units info</h1>
        </header>
        <form className="units-information__form" noValidate>
          {state.units.map((unit) => (
            <div className="unit-container" key={unit.id}>
              <UnitMainInfo
                unitInfo={unit}
                handleInputChange={handleInputChange}
                toggleUtilitiesIncluded={toggleUtilitiesIncluded}
                toggleUtility={toggleUtility}
                setRentOption={setRentOption}
                toggleSplitEvenly={toggleSplitEvenly}
                setBedroomPrice={setBedroomPrice}
              />
              {unit.showMedia && (
                <div className="unit-media">
                  <span>Unit Photos / Videos</span>
                  <UploadMedia videos={unit.videos} photos={unit.photos} />
                </div>
              )}
              {unit.showAmenities && (
                <Amenities
                  amenityType="Unit"
                  is_clickable={true}
                  unitId={unit.id}
                  amenities={unit.unitAmenities}
                  toggleAmenity={toggleAmenity}
                />
              )}
              {unit.showDescription && (
                <div className="input unit-description">
                  <label htmlFor="unit-description" className="input-label">
                    Unit description
                  </label>
                  <textarea
                    type="text"
                    name="unitDescription"
                    id={unit.id}
                    className={`input-field description${
                      unit.showDescError ? ' input-field--error' : ''
                    }`}
                    placeholder="Add your description here"
                    value={unit.unitDescription}
                    onChange={handleInputChange}
                    required
                  />
                  {unit.showDescError && (
                    <span className="input-filed-text-error">
                      This field is mandatory
                    </span>
                  )}
                </div>
              )}
              {(!unit.showMedia ||
                !unit.showAmenities ||
                !unit.showDescription) && <div className="hl" />}
              <div className="expand-buttons">
                {!unit.showMedia && (
                  <button
                    type="button"
                    className="btn-link"
                    name="showMedia"
                    id={unit.id}
                    onClick={expandSection}>
                    Unit Photos/Videos
                  </button>
                )}
                {!unit.showAmenities && (
                  <button
                    type="button"
                    className="btn-link"
                    name="showAmenities"
                    id={unit.id}
                    onClick={expandSection}>
                    Unit amenities
                  </button>
                )}
                {!unit.showDescription && (
                  <button
                    type="button"
                    className="btn-link"
                    name="showDescription"
                    id={unit.id}
                    onClick={expandSection}>
                    Unit description
                  </button>
                )}
                {state.units.length > 1 && (
                  <button
                    type="button"
                    className="btn-link delete-unit"
                    name="deleteUnit"
                    id={unit.id}
                    onClick={deleteUnitHandler}>
                    Delete unit
                  </button>
                )}
              </div>
            </div>
          ))}
          <button
            className="add-unit-button"
            type="button"
            onClick={addUnitHandler}>
            <img alt="" src="/images/add_circle.svg" />
            Add unit
          </button>
          <NavigationButtons
            currentStep={3}
            onSubmit={onSubmit}
            goToPreviousStep={goToPreviousStepHandler}
            saveToDraft={saveToDraftHandler}
          />
        </form>
      </div>
    </div>
  );
}

export default UnitInformation;
