import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import './styles.scss';
import checkForm from 'utilities/validator';
import {Link} from 'react-router-dom';
import {resetPassword} from 'store/actions/authActions';
import {NotificationManager} from 'react-notifications';

function ChangePassword() {
  const [state, setState] = useState({
    form: {
      old_password: '',
      new_password: '',
      confirm_password: '',
    },
    show_old_password: false,
    show_confirm_password: false,
    show_new_password: false,
  });
  const auth_user = useSelector((state) => state.auth.user);

  const dispatch = useDispatch();

  const handleInputChange = ({target}) => {
    setState({
      ...state,
      form: {
        ...state.form,
        [target.name]: target.value,
      },
    });
  };

  const toggleShowOldPassword = (e) => {
    e.preventDefault();
    setState({
      ...state,
      show_old_password: !state.show_old_password,
    });
  };

  const toggleShowNewPassword = (e) => {
    e.preventDefault();
    setState({
      ...state,
      show_new_password: !state.show_new_password,
    });
  };

  const toggleShowConfirmPassword = (e) => {
    e.preventDefault();
    setState({
      ...state,
      show_confirm_password: !state.show_confirm_password,
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      const data = {
        currentPassword: state.form.old_password,
        newPassword: state.form.new_password,
        email: auth_user.email,
      };
      const success_change = await dispatch(resetPassword(data));
      if (success_change) {
        NotificationManager.success('Password Change');
      }
    }
  };

  return (
    <div className="change-password">
      <header>
        <h1>Change password</h1>
      </header>
      <form
        className="change-password__form"
        name="sign-up-data"
        onSubmit={onSubmit}
        noValidate="novalidate"
        autoComplete="off">
        <div className="input">
          <label htmlFor="old_password" className="input-label">
            Old Password
          </label>
          <div className="input-with-icon">
            <input
              type={state.show_old_password ? 'text' : 'password'}
              name="old_password"
              id="password"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.old_password}
            />
            <button
              type="button"
              className="btn-show-password"
              onClick={toggleShowOldPassword}>
              <img
                alt="password"
                src={
                  state.show_old_password
                    ? '/images/hide-password.svg'
                    : '/images/show-password.svg'
                }
              />
            </button>
          </div>
        </div>
        <div className="input">
          <label htmlFor="new_password" className="input-label">
            New Password
          </label>
          <div className="input-with-icon">
            <input
              type={state.show_new_password ? 'text' : 'password'}
              name="new_password"
              id="password"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.new_password}
            />
            <button
              type="button"
              className="btn-show-password"
              onClick={toggleShowNewPassword}>
              <img
                alt="password"
                src={
                  state.show_new_password
                    ? '/images/hide-password.svg'
                    : '/images/show-password.svg'
                }
              />
            </button>
          </div>
        </div>
        <div className="input">
          <label htmlFor="last_name" className="input-label">
            Confirm Password
          </label>
          <div className="input-with-icon">
            <input
              type={state.show_confirm_password ? 'text' : 'password'}
              name="confirm_password"
              id="confirm_password"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.confirm_password}
            />
            <button
              type="button"
              className="btn-show-password"
              onClick={toggleShowConfirmPassword}>
              <img
                alt="password"
                src={
                  state.show_confirm_password
                    ? '/images/hide-password.svg'
                    : '/images/show-password.svg'
                }
              />
            </button>
          </div>
        </div>
        <Link to="../forgot-password">
          <button type="button" className="btn-link">
            I forgot my password
          </button>
        </Link>
        <input
          type="submit"
          value="Save New Password"
          className="btn change-password-btn"
        />
      </form>
    </div>
  );
}

export default ChangePassword;
