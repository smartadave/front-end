import React, {useState, useEffect} from 'react';
import './styles.scss';
import ConversationsChat from 'components/Conversations/ConversationsChat';
import ConversationsList from 'components/Conversations/ConversationsList';
import {useDispatch, useSelector} from 'react-redux';
import Applications from 'components/Conversations/Applications';

function Conversations()  {
  const conversationsList = useSelector(state => state.conversations.conversationsList)

  const [state, setState] = useState({
    conversationsList: [],
  })


  useEffect(() => {
    if(conversationsList) {
      setState({
        ...state,
        conversationsList: conversationsList
      })
    }
  }, [conversationsList])

  return (
    <div className="conversations">
      {state.conversationsList && <ConversationsList />}
      {state.conversationsList.length > 0 && <ConversationsChat />}
      <div className='application-block'>
        <Applications />
      </div>
    </div>
  );
}

export default Conversations;
