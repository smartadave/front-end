import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import './styles.scss';
import checkForm from 'utilities/validator';
import {useLocation, Link} from 'react-router-dom';
import {resetPassword} from 'store/actions/authActions';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function CreateNewPassword() {
  const [state, setState] = useState({
    form: {
      new_password: '',
      confirm_password: '',
    },
    show_password: false,
    show_confirm_password: false,
    success_change: false,
  });

  const query = useQuery();
  const dispatch = useDispatch();

  const handleInputChange = ({target}) => {
    setState({
      ...state,
      form: {
        ...state.form,
        [target.name]: target.value,
      },
    });
  };

  const toggleShowPassword = (e) => {
    e.preventDefault();
    setState({
      ...state,
      show_password: !state.show_password,
    });
  };

  const toggleShowConfirmPassword = (e) => {
    e.preventDefault();
    setState({
      ...state,
      show_confirm_password: !state.show_confirm_password,
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      console.log('valid');
      const email = query.get('email_forgotten');
      const code = query.get('code');
      const data = {
        email,
        newPassword: state.form.new_password,
        newPasswordToken: code,
      };
      const success_change = await dispatch(resetPassword(data));
      setState({
        ...state,
        success_change,
      });
    }
  };

  return !state.success_change ? (
    <div className="create-new-password">
      <header>
        <h1>Create new password</h1>
        <p>
          To strengthen your password, use uppercase and lowercase letters, and
          digits.
        </p>
      </header>
      <form
        className="create-new-password__form"
        name="sign-up-data"
        onSubmit={onSubmit}
        noValidate="novalidate"
        autoComplete="off">
        <div className="input">
          <label htmlFor="last_name" className="input-label">
            New Password
          </label>
          <div className="input-with-icon">
            <input
              type={state.show_password ? 'text' : 'password'}
              name="new_password"
              id="password"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.new_password}
            />
            <button
              type="button"
              className="btn-show-password"
              onClick={toggleShowPassword}>
              <img
                alt=""
                src={
                  state.show_password
                    ? '/images/hide-password.svg'
                    : '/images/show-password.svg'
                }
              />
            </button>
          </div>
        </div>
        <div className="input">
          <label htmlFor="last_name" className="input-label">
            Confirm Password
          </label>
          <div className="input-with-icon">
            <input
              type={state.show_confirm_password ? 'text' : 'password'}
              name="confirm_password"
              id="confirm_password"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.confirm_password}
            />
            <button
              type="button"
              className="btn-show-password"
              onClick={toggleShowConfirmPassword}>
              <img
                alt=""
                src={
                  state.show_confirm_password
                    ? '/images/hide-password.svg'
                    : '/images/show-password.svg'
                }
              />
            </button>
          </div>
        </div>
        <input
          type="submit"
          value="Save New Password"
          className="btn create-new-password-btn"
        />
      </form>
    </div>
  ) : (
    <div className="success-create-new-password">
      <header>
        <h1>Well Done!</h1>
        <p>
          Your password was successfully reset. Now you can sign in to Smarta
          with it.
        </p>
      </header>
      <Link to="../login">
        <button type="button" className="btn sign-in-btn">
          Sign In
        </button>
      </Link>
    </div>
  );
}

export default CreateNewPassword;
