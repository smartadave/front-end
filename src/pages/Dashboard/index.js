import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import FilteringHeader from 'components/FilteringHeader';
import './styles.scss';
import PropertyMapView from 'components/PropertyMapView';
import PropertyListView from 'components/PropertyListView';
import {
  initPropertyState,
  getViewListFilters,
} from 'store/actions/propertyListActions';
import {getCookie} from 'utilities/cookie';
import {useWindowSize} from 'hooks/windowSize';

function Dashboard() {
  const [state, setState] = useState({
    mapCenter: '',
    loadContent: false,
    saved_filters: null,
    show_content: 'list',
  });

  const user = useSelector((state) => state.user);
  const user_role = getCookie('user').role;
  const constants = useSelector((state) => state.constants);
  const propertyList = useSelector((state) => state.propertyList);
  const window_size = useWindowSize();

  const dispatch = useDispatch();

  useEffect(() => {
    initStore();
  }, [user]);

  useEffect(() => {
    if (Object.keys(propertyList.campus).length > 0) {
      setState({
        ...state,
        mapCenter: propertyList.campus,
        loadContent: true,
      });
    }
  }, [propertyList]);

  useEffect(() => {
    !propertyList.filter_range && dispatch(getViewListFilters());
  }, []);

  const initStore = async () => {
    if (user._id) {
      let campus;
      if (user_role === 'student' && constants.campuses) {
        const _campus = {
          ...constants.campuses.find((campus) => campus.id === user.campus),
        };
        const {_id, ...coordinates} = {..._campus.coordinates[0]};
        _campus.coordinates = coordinates;
        campus = _campus;
      } else {
        const _campus = {...constants.campuses[0]};
        const {_id, ...coordinates} = {..._campus.coordinates[0]};
        _campus.coordinates = coordinates;
        campus = _campus;
      }

      const data = {
        campus,
      };
      dispatch(initPropertyState(data));
    }
  };

  const setMapCenter = (place) => {
    setState({
      ...state,
      mapCenter: place,
      show_content: window_size.width <= 1024
        ? 'map'
        : state.show_content
    });
  };

  const setShowContent = (value) => {
    setState({
      ...state,
      show_content: value,
    });
  };

  return (
    <div className="dashboard">
      {Object.keys(user).length > 0 && <FilteringHeader />}
      {window_size.width <= 1024 && (
        <div className="show_content">
          <button
            name="map"
            type="button"
            className={`show_content__button${
              state.show_content === 'map' ? ' active' : ''
            }`}
            onClick={() => setShowContent('map')}>
            Show Map
          </button>
          <button
            name="list"
            type="button"
            className={`show_content__button${
              state.show_content === 'list' ? ' active' : ''
            }`}
            onClick={() => setShowContent('list')}>
            Show List
          </button>
        </div>
      )}
      {state.loadContent && (
        <div className="properties-container">
          <PropertyMapView
            mapCenter={state.mapCenter}
            showContent={state.show_content === 'map'}
          />
          <PropertyListView
            setMapCenter={setMapCenter}
            showContent={state.show_content === 'list'}
          />
        </div>
      )}
    </div>
  );
}

export default Dashboard;
