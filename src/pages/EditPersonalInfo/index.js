import React, {useState, useEffect} from 'react';
import './styles.scss';
import {useDispatch, useSelector} from 'react-redux';
import checkForm from 'utilities/validator';
import {updateUserMainInfo} from 'store/actions/userActions';
import FormatInput from 'components/FormatInput';

function EditPersonalInfo() {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const auth_user = useSelector((state) => state.auth.user);

  const [state, setState] = useState({
    form: {
      first_name: '',
      last_name: '',
      phone_number: '',
    },
  });

  useEffect(() => {
    if (user) {
      setState({
        ...state,
        form: {
          first_name: user.firstName,
          last_name: user.lastName,
          phone_number: user.phoneNumber,
        },
      });
    }
  }, [user]);

  const handleInputChange = ({target}) => {
    setState({
      ...state,
      form: {
        ...state.form,
        [target.name]: target.value,
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      const data = {
        firstName: state.form.first_name,
        lastName: state.form.last_name,
        phoneNumber: state.form.phone_number,
      };
      dispatch(updateUserMainInfo(data, auth_user.email, auth_user.id));
    }
  };

  return (
    <div className="edit-personal-info">
      <header>
        <h1>Edit personal information</h1>
      </header>
      <form
        className="sign-up__form"
        name="edit-personal-info-data"
        noValidate="novalidate"
        onSubmit={onSubmit}
        autoComplete="off">
        <div className="sign-up__form__name-block">
          <div className="input name-block-input">
            <label htmlFor="first_name" className="input-label">
              First Name
            </label>
            <input
              type="text"
              name="first_name"
              id="first_name"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.first_name}
            />
          </div>
          <div className="input name-block-input">
            <label htmlFor="last_name" className="input-label">
              Last Name
            </label>
            <input
              type="text"
              name="last_name"
              id="last_name"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.last_name}
            />
          </div>
        </div>
        <FormatInput
          format="phone"
          label="Phone Number"
          name="phone_number"
          id="phone_number"
          required={true}
          value={state.form.phone_number}
          handleChange={handleInputChange}
        />
        <input
          type="submit"
          value="Save"
          className="btn edit-personal-info-btn"
        />
      </form>
    </div>
  );
}

export default EditPersonalInfo;
