import React, {useEffect, useState} from 'react';
import './styles.scss';
import {useLocation} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {resendVerification} from "../../store/actions/authActions";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function EmailWasSent() {
  const query = useQuery();
  const [email, setEmail] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    if (query.get('email') !== null) {
      setEmail(query.get('email'));
    }
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch(resendVerification(email));
  };

  return (
    <div className="email-was-sent">
      <header>
        <h1>Ready to get started!</h1>
        <p>
          We have sent you a confirmation letter. Please, check your inbox{' '}
          <span>{email}</span> to verify your email.
        </p>
      </header>
      <form
        className="email-was-sent__form"
        name="sign-up-data"
        onSubmit={onSubmit}
        noValidate="novalidate"
        autoComplete="off">
        <input
          type="submit"
          value="Re-send Email"
          className="btn email-was-sent-btn"
        />
      </form>
    </div>
  );
}

export default EmailWasSent;
