import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import './styles.scss';
import PropertyMapView from 'components/PropertyMapView';
import PropertyListView from 'components/PropertyListView';
import { initPropertyState } from 'store/actions/propertyListActions';
import { getCookie } from 'utilities/cookie';
import {getFavoritesPropertiesList} from 'store/actions/propertyListActions';

function Favorites() {
  const [state, setState] = useState({
    mapCenter: '',
    loadContent: false
  });

  const user = useSelector(state => state.user);
  const user_role = getCookie('user').role;
  const constants = useSelector(state => state.constants);
  const propertyList = useSelector(state => state.propertyList);

  const dispatch = useDispatch();

  useEffect(() => {
    if(propertyList.favorites_list.list.length === 0) {
      dispatch(getFavoritesPropertiesList())
    }
  }, []);

  useEffect(() => {
    initStore();
  }, [user, dispatch]);

  useEffect(() => {
    if (Object.keys(propertyList.campus).length > 0) {
      setState({
        ...state,
        mapCenter: propertyList.campus,
        loadContent: true
      });
    }
  }, [propertyList]);

  const initStore = async () => {
    if (user._id) {
      let campus;
      if (user_role === 'student' && constants.campuses) {
        const _campus = { ...constants.campuses.find(campus => campus.id === user.campus) };
        const { _id, ...coordinates } = { ..._campus.coordinates[0] };
        _campus.coordinates = coordinates;
        campus = _campus;
      } else {
        const _campus = { ...constants.campuses[0] };
        const { _id, ...coordinates } = { ..._campus.coordinates[0] };
        _campus.coordinates = coordinates;
        campus = _campus;
      }

      const data = {
        campus
      }
      dispatch(initPropertyState(data))
    }
  }

  const setMapCenter = (place) => {
    setState({
      ...state,
      mapCenter: place
    });
  }


  return (
    <div className="favorites-page">
      {state.loadContent && (
        <div className="properties-container">
          <PropertyMapView
            favorites_view={true}
            mapCenter={state.mapCenter}
             />
          <PropertyListView
            favorites_view={true}
            setMapCenter={setMapCenter} />
        </div>)
      }

    </div>
  );
}

export default Favorites;
