import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import './styles.scss';
import checkForm from 'utilities/validator';
import {forgotPassword} from "store/actions/authActions";

function ForgotPassword() {
  const [state, setState] = useState({
    form: {
      email: '',
    },
  });

  const dispatch = useDispatch();

  const handleInputChange = ({target}) => {
    setState({
      ...state,
      form: {
        ...state.form,
        [target.name]: target.value,
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      dispatch(forgotPassword(state.form.email));
    }
  };

  return (
    <div className="forgot-password">
      <header>
        <h1>Forgot your password?</h1>
        <p>
          Enter your email address below and we'll send you a link to reset your
          password.
        </p>
      </header>
      <form
        className="forgot-password__form"
        name="sign-up-data"
        onSubmit={onSubmit}
        noValidate="novalidate"
        autoComplete="off">
        <div className="input">
          <label htmlFor="first_name" className="input-label">
            Email
          </label>
          <input
            type="email"
            name="email"
            id="email"
            required
            className="input-field"
            onChange={handleInputChange}
            value={state.form.email}
          />
        </div>

        <input
          type="submit"
          value="Send Me a Reset Link"
          className="btn forgot-password-btn"
        />
      </form>
    </div>
  );
}

export default ForgotPassword;
