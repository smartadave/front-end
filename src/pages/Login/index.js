import React, {useState, useEffect} from 'react';
import {useDispatch} from 'react-redux';
import './styles.scss';
import checkForm from 'utilities/validator';
import {login, verifyEmail} from 'store/actions/authActions';
import {Link, useLocation} from 'react-router-dom';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function Login() {
  const query = useQuery();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    form: {
      email: '',
      password: '',
    },
    show_password: false,
  });

  useEffect(() => {
    if (query.get('email_verification') !== null) {
      dispatch(verifyEmail(query.get('code')));
    }
  }, []);

  const handleInputChange = ({target}) => {
    setState({
      ...state,
      form: {
        ...state.form,
        [target.name]: target.value,
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      let data = {
        password: state.form.password,
        email: state.form.email,
      };
      dispatch(login(data));
    }
  };

  const toggleShowPassword = (e) => {
    e.preventDefault();
    setState({
      ...state,
      show_password: !state.show_password,
    });
  };

  return (
    <div className="login">
      <header>
        <h1>Sign In</h1>
        <p>Welcome back!</p>
      </header>
      <form
        className="login__form"
        name="sign-up-data"
        onSubmit={onSubmit}
        noValidate="novalidate"
        autoComplete="off">
        <div className="input">
          <label htmlFor="first_name" className="input-label">
            Email
          </label>
          <input
            type="email"
            required
            name="email"
            id="email"
            className="input-field"
            onChange={handleInputChange}
            value={state.form.email}
          />
        </div>
        <div className="input">
          <label htmlFor="last_name" className="input-label">
            Password
          </label>
          <div className="input-with-icon">
            <input
              type={state.show_password ? 'text' : 'password'}
              name="password"
              id="password"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.password}
            />
            <button
              type="button"
              className="btn-show-password"
              onClick={toggleShowPassword}>
              <img
                alt="password"
                src={
                  state.show_password
                    ? '/images/hide-password.svg'
                    : '/images/show-password.svg'
                }
              />
            </button>
          </div>
        </div>
        <Link to="../forgot-password">
          <button type="button" className="btn-link">
            I forgot my password
          </button>
        </Link>
        <input type="submit" value="Sign In" className="btn login-btn" />
        <div className="sign-up-block">
          <p>Don’t have an account?</p>
          <Link to="../sign-up">
            <button type="button" className="btn-link">
              Sign Up
            </button>
          </Link>
        </div>
      </form>
    </div>
  );
}

export default Login;
