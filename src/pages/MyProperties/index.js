import React, {useEffect, useState} from 'react';
import './styles.scss';
import {useSelector, useDispatch} from 'react-redux';
import {getMyProperties} from 'store/actions/myPropertiesActions';
import MyPropertiesList from 'components/MyPropertiesList';
import MyPropertyInfo from 'components/MyPropertyInfo';
import PromptModal from 'components/PromptModal';
import {useRefresh} from 'hooks/pageRefresh';
import AlertModal from 'components/AlertModal';
import {useWindowSize} from 'hooks/windowSize';

function MyProperties({history}) {
  const myProperties = useSelector((state) => state.myProperties.properties);
  const myDrafts = useSelector((state) => state.myProperties.drafts);
  const dispatch = useDispatch();
  const window_size = useWindowSize();

  const [showFullInfo, setShowFullInfo] = useState(null);
  const [editInProgress, setEditInProgress] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showList, setShowList] = useState(false);

  useEffect(() => {
    dispatch(getMyProperties());
  }, []);

  useEffect(() => {
      console.log(window_size);
      if (window_size.width >= 1024) {
          setShowList(true);
      }
  }, []);

  useRefresh(!editInProgress);

  const setProperty = (property) => {
    if (editInProgress) {
      setShowModal(true);
      if (window_size.width <= 1024) {
          setShowList(false);
      }
    } else if (window_size.width <= 1024) {
      setShowList(false);
      setShowFullInfo(property);
    } else {
        setShowFullInfo(property);
    }
  };

  return (
    myProperties &&
    myDrafts && (
      <div className="my-properties-container">
        <PromptModal
          text="Please save all your changes before leaving this page."
          navigate={(path) => {
            history.push(path);
          }}
          shouldBlockNavigation={() => {
            return editInProgress;
          }}
        />
        <AlertModal
          modal_visible={showModal}
          title="You have unsaved changes"
          text="Please save all changes before switching to another property."
          cancelAction={() => setShowModal(false)}
        />
        {window_size.width <= 1024 && (
          <button className="show-list" onClick={() => setShowList(!showList)}>
            <img
                src={showList ? '/images/arrow_back.svg' : '/images/burger.svg'}
                alt="" />
          </button>
        )}
        <MyPropertiesList
          properties={myProperties}
          drafts={myDrafts}
          showFullInfo={showFullInfo}
          setShowFullInfo={setProperty}
          showList={showList}
          windowSize={window_size}
        />
        <MyPropertyInfo
          property={showFullInfo}
          setProperty={setProperty}
          editInProgress={editInProgress}
          setEditInProgress={setEditInProgress}
          windowSize={window_size}
        />
      </div>
    )
  );
}

export default MyProperties;
