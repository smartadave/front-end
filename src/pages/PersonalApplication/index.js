import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Link, useParams} from 'react-router-dom';
import './styles.scss';
import history from 'history.js';
import {getUserApplication} from 'store/actions/personalApplicationActions';
import ApplicationInformation from 'components/PersonalApplication/ApplicationInformation';
import RentalHistory from 'components/PersonalApplication/RentalHistory';
import WorkHistory from 'components/PersonalApplication/WorkHistory';
import PersonalReferences from 'components/PersonalApplication/PersonalReferences';
import LifestyleInformation from 'components/PersonalApplication/LifestyleInformation';
import OtherInformation from 'components/PersonalApplication/OtherInformation';
import PersonalApplicationPreview from 'components/PersonalApplication/PersonalApplicationPreview';

function PersonalApplication() {
  const [state, setState] = useState({
    personal_application_status: 'pending',
  })

  const user = useSelector((state) => state.user);
  const status = useSelector(
    (state) => state.personalApplication.status,
  );
  const dispatch = useDispatch()

  useEffect(() => {
    setState({
      ...state,
      personal_application_status: status
    })
  }, [status])

  useEffect(() => {
    if(user._id) {
      dispatch(getUserApplication(user._id))
    }
  }, [user._id])
  
  let {step} = useParams();

  const renderEditApplicationMenu = () => {
   return (
    <div className="student-application-menu">
    {link_array.map(item => {
     return <button className={step === item.link ? 'active' : ''} onClick={() => history.push(`./${item.link}`)} key={item.link}>{item.name}</button>
    })}
  </div>
   )
  }

  return (
    <>
    {user._id && <div className="personal-application">
      {step !== 'applicationPreview' && <header>
        <h1>{state.personal_application_status === 'ready' ? 'Personal information' : 'Fill out your application'}</h1>
        <p>
          We will use the information provided here to automatically generate
          your future rental applications. You can find wnd edit this
          information in your profile. Please note that once you apply, this
          information will be available to the owner / manager of the property
          you applied for so that they can verify your identity.
        </p>
      </header>}
      {state.personal_application_status === 'ready' && renderEditApplicationMenu()}
      {step === 'applicationInformation' && <ApplicationInformation routeName="applicationInformation" create={state.personal_application_status !== 'ready'} />}
      {step === 'rentalHistory' && <RentalHistory routeName="rentalHistory" create={state.personal_application_status !== 'ready'} />}
      {step === 'workHistory' && <WorkHistory routeName="workHistory" create={state.personal_application_status !== 'ready'} />}
      {step === 'personalReferences' && <PersonalReferences routeName="personalReferences" create={state.personal_application_status !== 'ready'} />}
      {step === 'lifestyleInformation' && <LifestyleInformation routeName="lifestyleInformation" create={state.personal_application_status !== 'ready'} />}
      {step === 'otherInformation' && <OtherInformation routeName="otherInformation" create={state.personal_application_status !== 'ready'} />}
      {step === 'applicationPreview' && <PersonalApplicationPreview routeName="applicationPreview" />}
    </div>}
    </>
  );
}

export default PersonalApplication;


const link_array = [
  {name: 'Application info', link: 'applicationInformation'},
  {name: 'Rental history', link: 'rentalHistory'},
  {name: 'Work history', link: 'workHistory'},
  {name: 'References', link: 'personalReferences'},
  {name: 'Lifestyle Info', link: 'lifestyleInformation'},
  {name: 'Other info', link: 'otherInformation'},
]