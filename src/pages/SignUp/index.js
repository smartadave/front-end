import React, {useState} from 'react';
import './styles.scss';
import {useDispatch, useSelector} from 'react-redux';
import checkForm from 'utilities/validator';
import {Link} from 'react-router-dom';
import {signUp} from 'store/actions/authActions';
import Selector from 'components/Selector';
import FormatInput from 'components/FormatInput';

function ManagerSignUp() {
  const dispatch = useDispatch();
  const [state, setState] = useState({
    form: {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      terms_of_use: false,
      campus: '',
      phone_number: '',
    },
    show_password: false,
    student_signup: false,
  });
  const campuses = useSelector((state) => state.constants.campuses);

  const _campuses =
    campuses &&
    campuses.map((campus) => {
      campus.value = campus.id;
      campus.name = campus.campus;
      return campus;
    });

  const handleInputChange = ({target}) => {
    setState({
      ...state,
      form: {
        ...state.form,
        [target.name]: target.value,
      },
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (checkForm(e.target)) {
      if (state.student_signup) {
        //Student Sign up flow

        let student_object = {
          firstName: state.form.first_name,
          lastName: state.form.last_name,
          email: state.form.email,
          passwordHash: state.form.password,
          role: 'student',
          campus: state.form.campus,
        };
        dispatch(signUp(student_object));
      } else {
        //Manager Sign up flow
        let manager_object = {
          firstName: state.form.first_name,
          lastName: state.form.last_name,
          email: state.form.email,
          passwordHash: state.form.password,
          phoneNumber: state.form.phone_number,
          role: 'manager',
        };
        dispatch(signUp(manager_object));
      }
    }
  };

  const toggleTermAndCond = () => {
    setState({
      ...state,
      form: {
        ...state.form,
        terms_of_use: !state.form.terms_of_use,
      },
    });
  };

  const toggleShowPassword = (e) => {
    e.preventDefault();
    setState({
      ...state,
      show_password: !state.show_password,
    });
  };

  const toggleSignUpType = (toggle_state) => {
    setState({
      ...state,
      student_signup: toggle_state,
    });
  };

  return (
    <div className="sign-up">
      <header>
        <h1>Sign Up</h1>
        <div className="change-type">
          <button
            onClick={() => toggleSignUpType(true)}
            className={`btn ${state.student_signup ? 'active' : ''}`}>
            Student
          </button>
          <button
            onClick={() => toggleSignUpType(false)}
            className={`btn ${!state.student_signup ? 'active' : ''}`}>
            Manager
          </button>
        </div>
      </header>
      <form
        className="sign-up__form"
        name="sign-up-data"
        noValidate="novalidate"
        onSubmit={onSubmit}
        autoComplete="off">
        <div className="sign-up__form__name-block">
          <div className="input name-block-input">
            <label htmlFor="first_name" className="input-label">
              First Name
            </label>
            <input
              type="text"
              name="first_name"
              id="first_name"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.first_name}
            />
          </div>
          <div className="input name-block-input">
            <label htmlFor="last_name" className="input-label">
              Last Name
            </label>
            <input
              type="text"
              name="last_name"
              id="last_name"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.last_name}
            />
          </div>
        </div>
        {state.student_signup && (
          <Selector
            label="School Campus"
            name="campus"
            options={_campuses}
            value={state.form.campus}
            handleInputChange={handleInputChange}
          />
        )}
        <div className="input">
          <label htmlFor="first_name" className="input-label">
            Email
          </label>
          <input
            type="email"
            required
            name="email"
            id="email"
            className="input-field"
            onChange={handleInputChange}
            value={state.form.email}
          />
        </div>
        {!state.student_signup && (
            <FormatInput
                format="phone"
                label="Phone Number"
                name="phone_number"
                id="phone_number"
                required={true}
                value={state.form.phone_number}
                handleChange={handleInputChange}
            />
        )}
        <div className="input">
          <label htmlFor="last_name" className="input-label">
            Password
          </label>
          <div className="input-with-icon">
            <input
              type={state.show_password ? 'text' : 'password'}
              name="password"
              id="password"
              className="input-field"
              required
              onChange={handleInputChange}
              value={state.form.password}
            />
            <button
              type="button"
              className="btn-show-password"
              onClick={toggleShowPassword}>
              <img
                alt="password"
                src={
                  state.show_password
                    ? '/images/hide-password.svg'
                    : '/images/show-password.svg'
                }
              />
            </button>
          </div>
        </div>
        <label className="input-checkbox">
          I agree to Smarta <Link to="../term-of-use">Terms of Use</Link> and{' '}
          <Link to="../term-of-use">Privacy Policy</Link>
          <input
            onClick={toggleTermAndCond}
            checked={state.terms_of_use}
            name="terms_of_use"
            required
            type="checkbox"
          />
          <span className="checkmark"/>
        </label>
        <input type="submit" value="Sign Up" className="btn sign-up-btn" />
        <div className="sign-in-block">
          <p>Already have an account?</p>
          <Link to="../login">
            <button type="button" className="btn-link">
              Sign In
            </button>
          </Link>
        </div>
      </form>
    </div>
  );
}

export default ManagerSignUp;
