import history from '../../history';
import {DEAUTHENTICATE, AUTHENTICATE, FORGOT_PASSWORD} from './types';
import {clearCookie, removeCookie, setCookie, getCookie} from 'utilities/cookie';
import http from 'utilities/http';
import {handleLoader} from './loaderActions';
import {setError} from './errorActions';
import {getUser} from './userActions';

import {NotificationManager} from 'react-notifications';

export const signUp = (data) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .post('/auth/register', data)
      .then((resp) => {
        if (resp.status === 201) {
          window.location = `/email-was-sent?email=${data.email}`;
        }
      })
      .catch((e) => {
        if (e.data && e.data.message) {
          NotificationManager.error(e.data.message);
        }
        dispatch(setError('authentication'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const auth = (resp) => {
  return (dispatch) => {
    const token = resp.data.data.payload.token;
    const refresh_token = resp.data.data.payload.refresh_token;
    const {user} = resp.data.data;

    const user_data = {
      first_name: user.firstName,
      last_name: user.lastName,
      email: user.email,
      id: user.id,
      role: user.role,
      email_verified: user.auth.email.valid,
    };
    setCookie('user', user_data);
    setCookie('token', token);
    setCookie('refresh_token', refresh_token);
    history.push('/dashboard');

    dispatch(getUser(user.id, user.role))
    dispatch({
      type: AUTHENTICATE,
      payload: {user_data, token, refresh_token},
    });
  };
};

export const verifyEmail = (code) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .get(`/auth/login?code=${code}`)
      .then((resp) => {
        if (resp.status === 200) {
          history.replace('/login');
          NotificationManager.success('Email Verify');
        }
      })
      .catch((e) => {
        NotificationManager.error(e.data.message);
        dispatch(setError('authentication'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const login = (data) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .post('/auth/login', data)
      .then((resp) => {
        if (resp.status === 201) {
          dispatch(auth(resp));
        }
      })
      .catch((e) => {
        if (e.status === 401) {
          NotificationManager.error('Incorrect password or email address');
        }
        dispatch(setError('authentication'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const logout = () => {
  const user_id = getCookie('user').id;
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .post('/auth/logout', {userId: user_id})
      .then((resp) => {
        clearCookie();
        removeCookie('user');
        removeCookie('access_token');
        removeCookie('refresh_token');

        window.location = '/login';
        dispatch({type: DEAUTHENTICATE});
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const reauthenticate = ({user_data, token, refresh_token}) => {
  return (dispatch) => {
    dispatch({
      type: AUTHENTICATE,
      payload: {user_data, token, refresh_token},
    });
  };
};

export const forgotPassword = (email) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .get(`auth/forgot-password/${email}`)
      .then((resp) => {
        if (resp.status === 200 && resp.data.success) {
          NotificationManager.success('Reset link was sent');
          dispatch({type: FORGOT_PASSWORD});
        } else {
          throw Error(resp.data.data.message);
        }
      })
      .catch((error) => {
        switch (error.message) {
          case 'LOGIN.USER_NOT_FOUND':
            NotificationManager.error('Email not found');
            break;
          case 'RESET_PASSWORD.EMAIL_SENDED_RECENTLY':
            NotificationManager.error(
              'Reset password email was sended recently',
            );
            break;
          default:
            NotificationManager.error('Something went wrong');
        }
        dispatch(setError('authentication'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const resetPassword = (data) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    return http
      .post('auth/reset-password', data)
      .then((resp) => {
        if (
          resp.data.success &&
          resp.data.message === 'RESET_PASSWORD.PASSWORD_CHANGED'
        ) {
          return true;
        } else {
          throw Error(resp.message);
        }
      })
      .catch((error) => {
        NotificationManager.error(
          'Password change error. Please try again later.',
        );
        dispatch(setError('authentication'));
        return false;
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const resendVerification = (email) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http.get(`/auth/email/resend-verification/${email}`)
        .then((resp) => {
          if (resp.status === 200 && resp.data.message === 'LOGIN.EMAIL_RESENT') {
            NotificationManager.success('Email was re-sent');
          } else {
            dispatch(setError('resend-verification', resp.data.message));
          }
        })
        .catch((error) => {
          dispatch(setError('resend-verification', error.message));
          NotificationManager.error('Something went wrong!');
        })
        .finally(() => {
          dispatch(handleLoader(false));
        });
  }
};