import {
  GET_CAMPUSES,
  GET_PROPERTY_AMENITIES,
  GET_UNIT_AMENITIES,
} from './types';
import http from 'utilities/http';
import {setError} from './errorActions';

export const getCampuses = (id, user_type) => {
  return (dispatch) => {
    http
      .get(`/campus`)
      .then((resp) => {
        if (resp.status === 200) {
          dispatch({
            type: GET_CAMPUSES,
            payload: resp.data.data,
          });
        }
      })
      .catch((e) => {
        dispatch(setError('get_campuses'));
      })
      .finally(() => {});
  };
};

export const getAmenities = () => {
  return (dispatch) => {
    http
      .get('units/amenities')
      .then(({data}) => {
        dispatch({
          type: GET_UNIT_AMENITIES,
          payload: data,
        });
      })
      .catch((error) => {
        console.log(error);
      });

    http
      .get('property/amenity/getAmenity')
      .then(({data}) => {
        dispatch({
          type: GET_PROPERTY_AMENITIES,
          payload: data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
