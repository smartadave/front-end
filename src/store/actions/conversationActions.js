import history from '../../history';
import http from 'utilities/http';
import {handleLoader} from './loaderActions';
import {setError} from './errorActions';
import {
  SET_CONVERSATIONS_LIST,
  CREATE_NEW_CONVERSATION,
  SET_CURRENT_CONVERSATION,
  RECEIVE_MESSAGE,
  SET_CURRENT_MESSAGES_LIST,
  SEND_MESSAGE,
  DELETE_CONVERSATIONS,
  APPLY_TO_PROPERTY
} from './types';
import {createNewConversations, deleteConv} from 'store/middleware/socketMiddleware';

import {NotificationManager} from 'react-notifications';

export const getConversationsList = (user_id) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .get(`/rooms/${user_id}`)
      .then((resp) => {
        if (resp.status === 200 && resp.data.length > 0) {
          dispatch({
            type: SET_CONVERSATIONS_LIST,
            payload: resp.data,
          });
        }
      })
      .catch((e) => {
        if (e.data && e.data.message) {
          NotificationManager.error(e.data.message);
        }
        dispatch(setError('getConversationsList'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const deleteConversations = (room_id) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .delete(`/rooms/${room_id}`)
      .then((resp) => {
        if (resp.status === 200) {
          deleteConv(room_id)
          dispatch({
            type: DELETE_CONVERSATIONS,
            payload: room_id,
          });
        }
      })
      .catch((e) => {
        console.log(e)
        if (e.data && e.data.message) {
          NotificationManager.error(e.data.message);
        }
        dispatch(setError('deleteConversations'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const getRoomById = (room_id) => {
   return (dispatch) => {
    http
      .get(`/rooms/getOne/${room_id}`)
      .then((resp) => {
        if (resp.status === 200) {
          history.push(`conversations/${resp.data._id}`);
          dispatch({
            type: CREATE_NEW_CONVERSATION,
            payload: resp.data,
          });
          dispatch({
            type: SET_CURRENT_CONVERSATION,
            payload: resp.data._id,
          });
        }
      })
      .catch((e) => {
        if (e.data && e.data.message) {
          NotificationManager.error(e.data.message);
        }
        dispatch(setError('getRoomById'));
      })
  };
}

export const createChatWithUsers = (data) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .post('/rooms', data)
      .then((resp) => {
        if (resp.status === 201) {
          if (resp.data.status && resp.data.status.status === 400) {
            history.push(`conversations/${resp.data.data[0]._id}`);
            dispatch({
              type: SET_CURRENT_CONVERSATION,
              payload: resp.data.data[0]._id,
            });
          } else {
            createNewConversations(data)
            dispatch(getRoomById(resp.data._id))
          }
        }
      })
      .catch((e) => {
        console.log(e)
        if (e.data && e.data.message) {
          NotificationManager.error(e.data.message);
        }
        dispatch(setError('createChatWithUsers'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const setCurrentMessageList = (current_message_id) => ({
  type: SET_CURRENT_MESSAGES_LIST,
  payload: current_message_id,
});

export const setCurrentConversation = (id) => ({
  type: SET_CURRENT_CONVERSATION,
  payload: id,
});

export const receiveMessage = (data) => {
  return {
    type: RECEIVE_MESSAGE,
    payload: data,
  };
};

export const sendNewMessage = (data) => {
  return {
    type: SEND_MESSAGE,
    payload: data,
  };
};

export const applyToPropertyAction = (data) => {
  return {
    type: APPLY_TO_PROPERTY,
    payload: data
  }
}

export const uploadFiles = (file) => {
  let formData = new FormData();
  formData.append('upload', file);
  return (dispatch) => {
    dispatch(handleLoader(true));
    return http
      .post('s3/upload/photo', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then((data) => {
        return(data.data);
      })
      .catch((e) => {
        if (e.data && e.data.message) {
          NotificationManager.error(e.data.message);
        }
        dispatch(setError('uploadFiles'));
      }).finally(() => {
        dispatch(handleLoader(false));
      });
  };
};
