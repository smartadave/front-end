import {SET_ERROR} from './types';

export const setError = (scope = '', error = '') => {
  return {
    type: SET_ERROR,
    payload: {
      scope,
      error,
    },
  };
};
