import {HANDLE_LOADER} from './types';

export const handleLoader = (state) => {
  return {
    type: HANDLE_LOADER,
    payload: state
  };
};