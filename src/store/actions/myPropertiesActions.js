import {handleLoader} from './loaderActions';
import http from 'utilities/http';
import {getCookie} from '../../utilities/cookie';
import {NotificationManager} from 'react-notifications';
import {setError} from './errorActions';
import {
  ADD_UNIT, DELETE_PROPERTY, DELETE_UNIT,
  GET_MY_PROPERTIES, PUBLISH_DRAFT,
  SAVE_PROPERTY_CHANGES, UPDATE_UNIT,
} from './types';

export const getMyProperties = () => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    const user_id = getCookie('user').id;
    const data = {
      properties: [],
      drafts: [],
    };
    http
      .get(`property/${user_id}`)
      .then((resp) => {
        if (resp.status === 200) {
          data.properties = resp.data;
          data.properties.forEach((property) => {
            property.edit_type = 'properties';
          });
          return http.get(`property/drafts/${user_id}`);
        } else {
          throw Error(resp.data.message);
        }
      })
      .then((resp) => {
        if (resp.status === 200) {
          data.drafts = resp.data;
          data.drafts.forEach((draft) => {
            draft.edit_type = 'drafts';
          });
          dispatch({
            type: GET_MY_PROPERTIES,
            payload: {
              data,
            },
          });
        } else {
          throw Error(resp.data.message);
        }
      })
      .catch((error) => {
        console.log(error)
        dispatch(setError('get_my_properties', error));
        NotificationManager.error(
          'Something went wrong. Please try again later',
        );
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const savePropertyChanges = (type, data) => {
  return (dispatch) => {
    dispatch({
      type: SAVE_PROPERTY_CHANGES,
      payload: {
        type,
        data,
      },
    });
  };
};

export const updateProperty = (type, data) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    const api_url = type === 'properties' ? 'property' : 'property/drafts/update';
    return http
      .put(`${api_url}/${data._id}`, data)
      .then((resp) => {
        if (resp.status === 200) {
          dispatch(savePropertyChanges(type, data));
          return {
            status: true,
            message: resp.data.status.message
          };
        } else {
          return {
            status: false,
            message: 'Something went wrong.'
          }
        }
      })
      .catch((error) => {
        return {
          status: false,
          message: error.data.message
        };
      })
      .finally(() => dispatch(handleLoader(false)));
  };
};

export const changeActiveListing = (data) => {
  return (dispatch) => {
    return http.put(`property/${data._id}`, data)
        .then((resp) => {
            if (resp.status === 200) {
              dispatch(savePropertyChanges('properties', data));
              return true
            } else {
              return false;
            }
        })
        .catch(error => {
          console.log(error);
        })
        .finally(() => {
          dispatch(handleLoader(false));
        });
  };
};

export const deleteProperty = (property_id, type) => {
  return (dispatch) => {
    dispatch(handleLoader(true))
    const api_url = type === 'properties' ? 'property' : 'property/drafts';
    return http.delete(`${api_url}/${property_id}`)
        .then((resp) => {
          if (resp.status === 200) {
            dispatch({
              type: DELETE_PROPERTY,
              payload: {
                type,
                property_id
              }
            })
            return true;
          } else {
            return false
          }
        })
        .catch((error) => {
          console.log(error);
          return false;
        })
        .finally(() => {
          dispatch(handleLoader(false));
        });
  }
};

export const updateUnit = (unit, property_id, type) => {
  return (dispatch) => {
    handleLoader(true);
    const api_url = type === 'properties' ? 'units' : 'units/draft/update';
    return http.put(`${api_url}/${unit._id}`, unit)
        .then((resp) => {
          if (resp.status === 200) {
            dispatch({
              type: UPDATE_UNIT,
              payload: {
                unit: resp.data.data,
                property_id,
                type
              }
            })
            return {
              status: true,
              _id: resp.data.data._id
            }
          } else {
            dispatch(setError('update-unit', resp.data.message));
            return {
              status: false,
              message: 'Something went wrong!'
            }
          }
        })
        .catch((error) => {
          dispatch(setError('update-unit', error.data.message));
          return {
            status: false,
            message: error.data.message
          }
        })
        .finally(() => {
          dispatch(handleLoader(false));
        });
  }
};

export const createUnit = (unit, property_id, type) => {
  return (dispatch) => {
    handleLoader(true);
    const api_url = type === 'properties' ? 'units/create' : 'units/save-draft';
    return http.post(`${api_url}?propertyId=${property_id}`, unit)
        .then((resp) => {
          if (resp.status === 201) {
            dispatch({
              type: ADD_UNIT,
              payload: {
                property_id,
                unit: resp.data.data
              }
            })
            return {
              status: true,
              _id: resp.data.data._id
            }
          } else {
            dispatch(setError('add-unit', resp.data.message));
            return {
              status: false,
              message: 'Something went wrong.'
            }
          }
        })
        .catch((error) => {
          dispatch(setError('add-unit', error.data.message));
          return {
            status: false,
            message: error.data.message
          }
        })
        .finally(() => {
          dispatch(handleLoader(false));
        });
  }
};

export const deleteUnit = (id, property_id, type) => {
  return dispatch => {
    const api_url = type === 'properties' ? 'units' : 'units/drafts';
    dispatch(handleLoader(true));
    return http.delete(`${api_url}/${id}`)
        .then((resp) => {
          if (resp.status === 200) {
            console.log(resp);
            dispatch({
              type: DELETE_UNIT,
              payload: {
                type,
                unit_id: id,
                property_id
              }
            })
            return {
              status: true,
              message: resp.data.response.message
            }
          } else {
            dispatch(setError('delete-unit', resp.data.message));
            return {
              status: false,
              message: 'Something went wrong'
            }
          }
        })
        .catch((error) => {
          setError('delete-unit', error.data.message);
          return {
            status: false,
            message: error.data.message
          }
        })
  }
};

export const publishDraft = (draft) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    return http.post('property/drafts/publish', draft)
        .then((resp) => {
          if (resp.status === 201) {
            dispatch({
              type: PUBLISH_DRAFT,
              payload: {
                draft: resp.data.data
              }
            });
            return {
              status: true,
              data: resp.data.data,
              message: 'Property published'
            }
          } else {
            return {
              status: false,
              message: 'Something went wrong'
            }
          }
        })
        .catch((error) => {
          setError('publish-draft', error.data.message);
          return {
            status: false,
            message: error.data.message
          }
        })
        .finally(() => {
          dispatch(handleLoader(false));
        });
  }
};