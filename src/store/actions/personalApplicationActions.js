import {
  UPDATE_STEP,
  SET_PROPERTY_APPLICATION,
  CREATE_PROPERTY_APPLICATION,
  CHANGE_STATUS_SA
} from './types';
import http from 'utilities/http';
import {handleLoader} from './loaderActions';
import {setError} from './errorActions';
import history from 'history.js';
import {NotificationManager} from 'react-notifications';
import {getCookie} from 'utilities/cookie';

export const getUserApplication = (id) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .get(`student-application/${id}`)
      .then((resp) => {
        if (resp.status === 200) {
         if(resp.data.data === null) {
          dispatch({
            type: CHANGE_STATUS_SA,
            payload: 'pending'
          })
           history.push('../personal-application/applicationInformation')
         }
         else {
           dispatch({
             type: SET_PROPERTY_APPLICATION,
             payload: {data:resp.data.data, status: 'ready'}
           })
           history.push('../personal-application/applicationInformation')
         }
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(setError('getUserApplication'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const createUserApplication = (id,data) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .post(`student-application/${id}`, data)
      .then((resp) => {
        if (resp.status === 201) {
          NotificationManager.success('', 'Student Application create')
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(setError('createUserApplication'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const updateUserApplication = (id,data) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .put(`student-application/${id}`, data)
      .then((resp) => {
        if (resp.status === 201) {
          NotificationManager.success('', 'Student was update')
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(setError('createUserApplication'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};


export const updateStep = (data) => {
  return (dispatch) => {
    return dispatch({
      type: UPDATE_STEP,
      payload: {step: data.step, data: data.data}
    });
  };
};