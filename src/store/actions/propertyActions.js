import {handleLoader} from './loaderActions';
import {
  NAVIGATE_TO_PROPERTY_STEP,
  SUBMIT_FIRST_STEP,
  SUBMIT_PROPERTY_INFO_STEP,
  SUBMIT_UNITS_INFO_STEP,
  PREVIOUS_PROPERTY_STEP,
  UPDATE_STEPS,
  CLEAR_PROPERTY_STORE,
  INIT_PROPERTY_STATE,
} from './types';
import http from 'utilities/http';
import {getCookie} from 'utilities/cookie';
import {setError} from './errorActions';
import {NotificationManager} from 'react-notifications';

export const initPropertyState = () => {
  return (dispatch) => {
    dispatch({
      type: INIT_PROPERTY_STATE,
    });
  };
};

export const setStep = (type) => {
  return (dispatch) => {
    return dispatch({type});
  };
};

export const navigateToStep = (step) => {
  return (dispatch) => {
    return dispatch({
      type: NAVIGATE_TO_PROPERTY_STEP,
      payload: {
        current_step: step,
      },
    });
  };
};

export const submitFirstStep = (data) => {
  return (dispatch) => {
    const payloadData = {
      unitsType: data.unitsType,
      address: data.address,
      propertyType: data.propertyType,
      propertyName: data.propertyName,
      units: [],
      current_step: 2,
      campuses: data.campuses,
    };

    if (data.unitsType === 'Single') {
      payloadData.units = [
        {
          bedrooms: data.bedrooms,
          bathrooms: data.bathrooms,
        },
      ];
    }
    dispatch({
      type: SUBMIT_FIRST_STEP,
      payload: payloadData,
    });
  };
};

export const submitPropertyInfoStep = (data) => {
  return (dispatch) => {
    dispatch({
      type: SUBMIT_PROPERTY_INFO_STEP,
      payload: {
        data,
        next_step: data.unitsType === 'Single' ? 4 : 3,
      },
    });
  };
};

export const submitUnitsInfoStep = (data) => {
  return (dispatch) => {
    dispatch({
      type: SUBMIT_UNITS_INFO_STEP,
      payload: {
        data,
      },
    });
  };
};

export const saveToDrafts = (data, history) => {
  return (dispatch) => {
    //TODO HOT FIX, PLEASE FIX IT IN FUTURE
    const campuses = data.campuses.map((campus) => {
      return {_id: campus.id};
    });
    data.campuses = campuses;
    dispatch(handleLoader(true));
    const user_id = getCookie('user').id;
    http
      .post(`property/save-draft/${user_id}`, data)
      .then((response) => {
        if (response.status === 201 && response.statusText === 'Created') {
          NotificationManager.success(response.data.status.message);
          history.replace('/dashboard');
          dispatch(clearStore());
        } else {
          throw Error(response.message);
        }
      })
      .catch((error) => {
        console.log(error);
        if (error.message) {
          NotificationManager.error(error.message);
          dispatch(setError('property'));
        } else {
          NotificationManager.error('Something went wrong!');
          dispatch(setError('property'));
        }
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const publishProperty = (data, history) => {
  return (dispatch) => {
    //TODO HOT FIX, PLEASE FIX IT IN FUTURE
    const campuses = data.campuses.map((campus) => {
      return {_id: campus.id};
    });
    data.campuses = campuses;
    dispatch(handleLoader(true));
    const user_id = getCookie('user').id;
    return http
      .post(`property/create/${user_id}`, data)
      .then((resp) => {
        NotificationManager.success('Property Created');
        history.push('/dashboard');
        dispatch(clearStore());
      })
      .catch((error) => {
        if (error.message) {
          NotificationManager.error(error.message);
          dispatch(setError('property'));
        } else {
          NotificationManager.error('Something went wrong!');
          dispatch(setError('property'));
        }
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const goToPreviousStep = (data) => {
  return (dispatch) => {
    dispatch({
      type: PREVIOUS_PROPERTY_STEP,
      payload: {
        data,
      },
    });
  };
};

export const updateSteps = (step) => {
  return (dispatch) => {
    dispatch({
      type: UPDATE_STEPS,
      payload: {step},
    });
  };
};

export const clearStore = () => {
  console.log('clear');
  return (dispatch) => {
    dispatch({
      type: CLEAR_PROPERTY_STORE,
    });
  };
};

export const fetchAmenities = (type) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    return http
      .get(
        `${
          type === 'property'
            ? 'property/amenity/getAmenity'
            : 'units/amenities'
        }`,
      )
      .then(({data}) => {
        return data.map((amenity) => ({
          ...amenity,
          selected: false,
        }));
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const uploadPhotosToS3 = (data) => {
  const formData = new FormData();
  formData.append("upload", data);

	return (dispatch) => {
    dispatch(handleLoader(true));
    return http
      .post("s3-upload/photo", formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then((data) => {
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
}

export const shareProperty = (propertyId, userId, email) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    return http
        .post(`property/share/property`, {
          propertyId,
          userId,
          email
        })
        .then((resp) => {
          if (resp.status === 201) {
            return {
              status: true,
              message: resp.data.message
            }
          } else {
            dispatch(setError('share-property', resp.data.message));
            return {
              status: false,
              message: 'Something went wrong'
            }
          }
        })
        .catch((error) => {
          dispatch(setError('share-property', error.message));
          return {
            status: false,
            message: error.message
          }
        })
        .finally(() => {
          dispatch(handleLoader(false));
        });
  }
};