import {getCookie} from 'utilities/cookie';
import http from 'utilities/http';
import {
  GET_MAP_VIEW_LIST,
  INIT_PROPERTY_LIST,
  GET_VIEW_LIST,
  SET_CURRENT_FILTER,
  SET_FAVORITES_PROPERTIES,
  SET_FILTER_RANGE,
} from './types';
import {handleLoader} from './loaderActions';
import {NotificationManager} from 'react-notifications';
import {setError} from './errorActions';

export const initPropertyState = (data) => {
  return (dispatch) => {
    dispatch({
      type: INIT_PROPERTY_LIST,
      payload: {
        ...data,
      },
    });
  };
};

export const getMapViewList = (filtering) => {
  return (dispatch) => {
    http
      .post(`property/view/list/map?id=${getCookie('user').id}`, filtering)
      .then((resp) => {
        if (resp.status === 201) {
          dispatch({
            type: GET_MAP_VIEW_LIST,
            payload: {
              data: resp.data.data ? resp.data.data : [],
            },
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const getMapCardInfo = (id) => {
  return http
    .get(`property/view/list/get-one?propertyId=${id}`)
    .then((resp) => {
      if (resp.status === 200) {
        return resp.data.data.data[0];
      } else {
        throw Error();
      }
    })
    .catch((error) => {
      return error;
    });
};

export const getViewList = (
  filtering,
  start_of,
  finish_of,
  is_new_filtering,
) => {
  return (dispatch) => {
    http
      .post(`property/view/list?id=${getCookie('user').id}`, {
        filtering: filtering,
        start_of: start_of,
        finish_of: finish_of,
      })
      .then((resp) => {
        if (resp.status === 201) {
          dispatch({
            type: GET_VIEW_LIST,
            payload: {
              data: resp.data.data ? resp.data.data : [],
              start_of: resp.data.start_of,
              finish_of: resp.data.finish_of,
              count: resp.data.count,
              is_new_filtering,
            },
          });
          dispatch({
            type: SET_CURRENT_FILTER,
            payload: {filtering},
          });
        } else throw Error('Cannot load properties');
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const getPropertyFullInfo = (propertyId) => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    return http
      .get(`property/getOne/${propertyId}`)
      .then((resp) => {
        if (resp.status === 200) {
          return resp.data;
        } else {
          throw Error('Cannot load property information');
        }
      })
      .catch((error) => {
        NotificationManager.error(error.data.message);
        dispatch(setError('propertyList'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const getFavoritesPropertiesList = () => {
  return (dispatch) => {
    dispatch(handleLoader(true));
    return http
      .get(`property/view/list/favourites?userId=${getCookie('user').id}`)
      .then((resp) => {
        if (resp.status === 200) {
          dispatch({
            type: SET_FAVORITES_PROPERTIES,
            payload: {
              list: resp.data.response.data,
              mapList: resp.data.response.mapView,
            },
          });
        } else {
          throw Error('Cannot load favorites list');
        }
      })
      .catch((error) => {
        NotificationManager.error(error.data.message);
        dispatch(setError('getFavoritesPropertiesList'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const getViewListFilters = () => {
  return (dispatch) => {
    return http
      .get(`property/view/list/filters`)
      .then((resp) => {
        if (resp.status === 200) {
          dispatch({
            type: SET_FILTER_RANGE,
            payload: resp.data.data
          })
        } else {
          throw Error('Cannot load favorites list');
        }
      })
      .catch((error) => {
        NotificationManager.error(error.data.message);
        dispatch(setError('getFavoritesPropertiesList'));
      })
      .finally(() => {});
  };
};
