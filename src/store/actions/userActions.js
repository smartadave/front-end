import {
  GET_USER,
  UPDATE_USER_MAIN_INFO,
  SET_SAVED_FILTER,
  ADD_PROPERTY_TO_FAVORITES,
  REMOVE_PROPERTY_FROM_FAVORITES,
  REMOVE_PROPERTY_FROM_FAVORITES_LIST,
  ADD_PROPERTY_TO_FAVORITES_LIST,
  SET_CURRENT_FILTER
} from './types';
import http from 'utilities/http';
import {handleLoader} from './loaderActions';
import {setError} from './errorActions';
import {NotificationManager} from 'react-notifications';
import {getCookie} from 'utilities/cookie';

export const getUser = (id, user_type) => {
  const api_link_type = user_type === 'student' ? 'students' : 'manager';
  return (dispatch) => {
    http
      .get(`/${api_link_type}/${id}`)
      .then(async (resp) => {
        if (resp.status === 200) {
          dispatch(getFilters(id));
          dispatch({
            type: GET_USER,
            payload: resp.data,
          });
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(setError('authentication'));
      })
      .finally(() => {});
  };
};

export const updateUserMainInfo = (data, user_type, id) => {
  const api_link_type = user_type === 'student' ? 'students' : 'manager';
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .put(`/${api_link_type}/${id}`, data)
      .then((resp) => {
        if (resp.status === 200) {
          dispatch({
            type: UPDATE_USER_MAIN_INFO,
            payload: data,
          });
        } else {
          NotificationManager.error("User can't be update");
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(setError('authentication'));
        NotificationManager.error("User can't be update");
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const createNewFilter = (id, filters) => {
  const user_id = getCookie('user').id;
  return (dispatch) => {
    http
      .post(`/users/filters/save?id=${user_id}`, filters)
      .then((resp) => {
        if (resp.status === 201) {
          dispatch({
            type: SET_SAVED_FILTER,
            payload: filters,
          });
          // NotificationManager.success('Filters save');
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(setError('createNewFilter'));
        // NotificationManager.success('Something went wrong');
      })
      .finally(() => {});
  };
};

export const getFilters = () => {
  const user_id = getCookie('user').id;
  return (dispatch) => {
    dispatch(handleLoader(true));
    http
      .get(`/users/filters/get-filters?id=${user_id}`)
      .then((resp) => {
        console.log(resp);
        if (resp.status === 200) {
          dispatch({
            type: SET_SAVED_FILTER,
            payload: resp.data.data.savedFilters,
          });
          dispatch({
            type: SET_CURRENT_FILTER,
            payload: resp.data.data.savedFilters,
          })
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(setError('getFilters'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};

export const getUserInfo = (id, user_role) => {
  return (dispatch) => {
    const api_link_type = user_role === 'student' ? 'students' : 'manager';
    return http
      .get(`/${api_link_type}/${id}`)
      .then((resp) => {
        if (resp.status === 200) {
          return resp.data;
        } else return {};
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const handleFavoritesProperty = (propertyId, status, property) => {
  console.log(status)
  return (dispatch) => {
    dispatch(handleLoader(true));
    return http
      .put(
        `property/view/list/save-to-favourites?userId=${
          getCookie('user').id
        }&propertyId=${propertyId}&status=${status}`,
      )
      .then((resp) => {
        if (resp.status === 200) {
          dispatch({
            type:
              status === 'add'
                ? ADD_PROPERTY_TO_FAVORITES
                : REMOVE_PROPERTY_FROM_FAVORITES,
            payload: propertyId,
          });
          dispatch({
            type:
              status === 'add'
                ? ADD_PROPERTY_TO_FAVORITES_LIST
                : REMOVE_PROPERTY_FROM_FAVORITES_LIST,
            payload: property,
          });
          NotificationManager.success(
            `${
              status === 'add'
                ? 'The property is added to favorites.'
                : 'The property is removed from favorites.'
            }`,
          );
          return true;
        }
      })
      .catch((error) => {
        console.log(error)
        NotificationManager.error(error.data.message);
        dispatch(setError('handleFavoritesProperty'));
      })
      .finally(() => {
        dispatch(handleLoader(false));
      });
  };
};


export const saveFilter = (filters) => {
  return dispatch => {
    // dispatch({
    //   type: SET_SAVED_FILTER,
    //   payload: {...filters},
    // });
    dispatch({
      type: SET_CURRENT_FILTER,
      payload: {...filters},
    })
  };
}