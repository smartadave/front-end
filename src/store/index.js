import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducers";
import {socketMiddleware} from 'store/middleware/socketMiddleware';

const store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(socketMiddleware, thunk)
  )
);

export default store;
