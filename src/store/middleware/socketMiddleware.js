import io from 'socket.io-client';
import {
  getCookie,
  setCookie,
  removeCookie,
  clearCookie,
} from 'utilities/cookie';
import history from 'history.js';
import {receiveMessage, applyToPropertyAction} from 'store/actions/conversationActions';

const socket = io('ws://localhost:3001', {
  transports: ['websocket'],
  query: {
    Authentication: `${getCookie('token')}`,
  },
});

export const sendMessage = (data) => {
  socket.emit('message', data);
};


export const applyToProperty = (data) => {
  socket.emit('apply_property', data)
}

export const createNewConversations = (data) => {
  console.log(data)
  socket.emit('create_new_conversations', data)
}

export const deleteConv = (data) => {
  console.log(data)
  socket.emit('delete_conversations', data)
}

export const socketMiddleware = (store) => {
  socket.on('connect', (data) => {
    socket.emit('join');
    console.log('connect', data);
  });

  socket.on('apply_property_on', data => {
    history.push(`/conversations/${data.roomId}`)
    store.dispatch(applyToPropertyAction(data))
  })

  socket.on('create_new_conversations_on', data => {
    console.log(data)
  })

  socket.on('delete_conversations_on', data => {
    console.log(data)
  })

  socket.on('receive_message', (data) => {
    store.dispatch(receiveMessage(data));
  });

  socket.on('connect_error', (err) => {
    console.log(err);
  });
  
  socket.on('disconnect', (reason) => {
    console.log('disconnect', reason);
    socket.connect();

    socketMiddleware()
  });


  return (next) => (action) => {
    return next(action);
  };
};

export default socketMiddleware;
