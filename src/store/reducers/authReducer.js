import {DEAUTHENTICATE, AUTHENTICATE, FORGOT_PASSWORD} from "../actions/types";

const initialState = {
  user: null,
  token: null,
  refresh_token: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATE:
      return {
        ...state,
        user: action.payload.user_data,
        token: action.payload.token,
        refresh_token: action.payload.refresh_token,
      };
    case DEAUTHENTICATE:
      return initialState;
    case FORGOT_PASSWORD:
        return initialState;
    default:
      return state;
  }
};
