import {
  GET_CAMPUSES,
  GET_PROPERTY_AMENITIES,
  GET_UNIT_AMENITIES,
} from '../actions/types';

const neighborhoods = [
  'Brighton',
  'Downtown',
  'Eastwood',
  'Elmwood',
  'Far Westside',
  'Lakefront',
  'Lincoln Park',
  'Meadowbrook',
  'Near Northeast',
  'Near Westside',
  'North Valley',
  'Northside',
  'Outer Comstock',
  'Salt Springs',
  'Sedgwick',
  'Skunk City',
  'Skytop',
  'South Valley',
  'Southwest',
  'Strathmore',
  'University Hill',
  'Washington Square',
  'Westcott',
  'Westside',
];

const initialState = {
  neighborhoods: neighborhoods,
  property_amenities: [],
  unit_amenities: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CAMPUSES:
      return {...state, campuses: action.payload};
    case GET_PROPERTY_AMENITIES:
      return {...state, property_amenities: action.payload};
    case GET_UNIT_AMENITIES:
      return {...state, unit_amenities: action.payload};
    default:
      return state;
  }
};
