import history from '../../history';
import {
  SET_CONVERSATIONS_LIST,
  CREATE_NEW_CONVERSATION,
  SET_CURRENT_CONVERSATION,
  SET_CURRENT_MESSAGES_LIST,
  RECEIVE_MESSAGE,
  DELETE_CONVERSATIONS,
  APPLY_TO_PROPERTY
} from '../actions/types';

const initialState = {
  conversationsList: [],
  currentConversationId: null,
  currentMessagesList: [],
  currentConversations: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CONVERSATIONS_LIST:
      return {...state, conversationsList: action.payload};
    case CREATE_NEW_CONVERSATION:
      return {
        ...state,
        conversationsList: [...state.conversationsList, action.payload],
      };
    case SET_CURRENT_CONVERSATION:
      return {...state, currentConversationId: action.payload};
    case SET_CURRENT_MESSAGES_LIST:
      const active_conversation_index = state.conversationsList.findIndex(
        (i) => i._id === action.payload,
      );
      return {
        ...state,
        currentMessagesList:
          state.conversationsList[active_conversation_index].messages,
        currentConversations:  state.conversationsList[active_conversation_index],
      };
    case DELETE_CONVERSATIONS:
      const delete_conversation_index = state.conversationsList.findIndex(
        (i) => i._id === action.payload,
      );
      const copy_of_conversationsList = [...state.conversationsList]
      copy_of_conversationsList.splice(delete_conversation_index, 1)

      return {
        ...state,
        conversationsList: copy_of_conversationsList,
        currentConversationId: null,
        currentMessagesList: [],
        currentConversations: {},
      };

    case RECEIVE_MESSAGE:
      const newConversationsList = state.conversationsList.map((item) => {
        if (item._id === action.payload[0]) {
          item.messages = [...item.messages, action.payload[1]];
          return item;
        } else return item;
      });
      if (action.payload[0] === state.currentConversationId) {
        return {
          ...state,
          currentMessagesList: [
            ...state.currentMessagesList,
            action.payload[1],
          ],
          conversationsList: newConversationsList,
        };
      } else {
        return {
          ...state,
          conversationsList: newConversationsList,
        };
      }
      case APPLY_TO_PROPERTY:
      let currentConversationsForUpdate = {}
      let newConversationsListProperty = state.conversationsList.map((item) => {
        if (item._id === action.payload.roomId) {
          item.related_property = [...item.related_property, action.payload.related_property];
          currentConversationsForUpdate = {...item}
          return item;
        } else return item;
      });
      if (action.payload[0] === state.currentConversationId) {
        return {
          ...state,
          currentConversations: currentConversationsForUpdate,
          conversationsList: newConversationsListProperty,
        };
      } else {
        return {
          ...state,
          conversationsList: newConversationsListProperty,
        };
      }
    default:
      return state;
  }
};
