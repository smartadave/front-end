import { combineReducers } from "redux";
import errorReducer from './errorReducer';
import authReducer from './authReducer';
import loaderReducer from './loaderReducer';
import userReducer from './userReducer';
import propertyReducer from './propertyReducer';
import constantReducer from './constantReducer';
import propertyListReducer from './propertyListReducer';
import personalApplicationReducer from './personalApplicationReducer';
import myPropertiesReducer from './myPropertiesReducer';
import conversationReducer from './conversationReducer';

const rootReducer = combineReducers({
  error: errorReducer,
  auth: authReducer,
  property: propertyReducer,
  loader: loaderReducer,
  user: userReducer,
  constants: constantReducer,
  propertyList: propertyListReducer,
  personalApplication: personalApplicationReducer,
  myProperties: myPropertiesReducer,
  conversations: conversationReducer,
});

export default rootReducer;
