import {HANDLE_LOADER, HANDLE_CONVERSATION_LOADER} from '../actions/types';

const initialState = {
  loader: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case HANDLE_LOADER:
      return {...state, loader: action.payload};
    default:
      return state;
  }
};
