import {
  DELETE_PROPERTY, DELETE_UNIT,
  GET_MY_PROPERTIES, PUBLISH_DRAFT,
  SAVE_PROPERTY_CHANGES,
  UPDATE_UNIT,
} from '../actions/types';

const initialState = {
  properties: null,
  drafts: null,
};

const updateProperties = (properties, updated_property) => {
  const updated_properties = [...properties];
  const propertyIndex = updated_properties.findIndex(
    (property) => property._id === updated_property._id,
  );
  updated_properties[propertyIndex] = updated_property;
  return updated_properties;
};

const deleteProperty = (properties, id) => {
  return [...properties].filter((property) => property._id !== id);
};

const deleteUnit = (properties, property_id, unit_id) => {
  const updated_properties = [...properties];
  const propertyIndex = updated_properties.findIndex(
      (property) => property._id === property_id
  );
  updated_properties[propertyIndex].units = updated_properties[propertyIndex].units
      .filter((unit) => unit._id !== unit_id);
  return updated_properties;
};

const updateUnits = (properties, property_id, unit_data) => {
  const updated_properties = [...properties];
  const propertyIndex = updated_properties.findIndex(
    (property) => property._id === property_id,
  );
  const units = [...updated_properties[propertyIndex].units];
  const unit_index = units.findIndex((unit) => unit._id === unit_data._id);
  units[unit_index] = unit_data;
  updated_properties[propertyIndex].units = units;
  return updated_properties;
};

export default (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case GET_MY_PROPERTIES:
      return {
        ...state,
        properties: payload.data.properties,
        drafts: payload.data.drafts,
      };
    case SAVE_PROPERTY_CHANGES:
      return {
        ...state,
        [payload.type]: updateProperties(state[payload.type], payload.data),
      };
    case UPDATE_UNIT:
      return {
        ...state,
        [payload.type]: updateUnits(
          state[payload.type],
          payload.property_id,
          payload.unit,
        ),
      };
    case DELETE_PROPERTY:
      return {
        ...state,
        [payload.type]: deleteProperty(
          state[payload.type],
          payload.property_id,
        ),
      };
    case DELETE_UNIT:
      return {
        ...state,
        [payload.type]: deleteUnit(
            state[payload.type],
            payload.property_id,
            payload.unit_id
        )
      }
    case PUBLISH_DRAFT:
      return {
        ...state,
        properties: [
            ...state.properties,
            payload.draft
        ],
        drafts: state.drafts.filter((draft) => draft._id !== payload.draft._id)
      }
    default:
      return state;
  }
};
