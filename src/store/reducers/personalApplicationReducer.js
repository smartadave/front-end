import {
  UPDATE_STEP,
  CHANGE_STATUS_SA,
  SET_PROPERTY_APPLICATION,
} from '../actions/types';

const initialState = {
  applicationInformation: {
    title: 'Application information',
    questions: {
      0: {
        'Full Name': {
          question: 'Full Name',
          answer: '',
          type: 'text',
          required: true,
        },
        'Birth Date': {
          question: 'Birth Date',
          answer: '',
          type: 'date',
          required: true,
        },
        'Main Phone #': {
          question: 'Main Phone #',
          answer: '',
          type: 'tel',
          required: true,
        },
        'Secondary Phone # (optional)': {
          question: 'Secondary Phone # (optional)',
          answer: '',
          required: false,
          type: 'tel',
        },
        Email: {
          question: 'Email',
          answer: '',
          required: true,
          type: 'email',
        },
        'Social Security Number': {
          question: 'Social Security Number',
          required: true,
          answer: '',
          type: 'password',
        },
        'Driving License Number': {
          question: 'Driving License Number',
          required: true,
          answer: '',
          type: 'password',
        },
        'Driving License State': {
          question: 'Driving License State',
          required: true,
          answer: '',
          type: 'selector',
        },
      },
    },
  },
  rentalHistory: {
    title: 'Rental history',
    questions: {
      0: {
        'Are you currently renting a home?': {
          question: 'Are you currently renting a home?',
          answer: false,
          type: 'boolean',
        },
        'Current Address': {
          question: 'Current Address',
          answer: '',
          type: 'address',
          required: true,
        },
        'How long have you been living there?': {
          question: 'How long have you been living there?',
          answer: '',
          required: true,
          type: 'number',
        },
        'Reason for leaving': {
          question: 'Reason for leaving',
          answer: '',
          required: true,
          type: 'text',
        },
        'Name of Landlord': {
          question: 'Name of Landlord',
          answer: '',
          required: true,
          type: 'text',
        },
        'Landlord Phone #': {
          question: 'Landlord Phone #',
          answer: '',
          required: true,
          type: 'tel',
        },
      },
      1: {
        'Have you ever rented a property before?': {
          question: 'Have you ever rented a property before?',
          answer: false,
          type: 'boolean',
        },
        'Current Address': {
          question: 'Current Address',
          answer: '',
          required: true,
          type: 'address',
        },
        'How long have you been living there?': {
          question: 'How long have you been living there?',
          answer: '',
          required: true,
          type: 'number',
        },
        'Reason for leaving': {
          question: 'Reason for leaving',
          answer: '',
          required: true,
          type: 'text',
        },
        'Name of Landlord': {
          question: 'Name of Landlord',
          answer: '',
          required: true,
          type: 'text',
        },
        'Landlord Phone #': {
          question: 'Landlord Phone #',
          answer: '',
          required: true,
          type: 'tel',
        },
      },
    },
  },
  workHistory: {
    title: 'Work History',
    questions: {
      0: {
        'Are you currently working somewhere?': {
          question: 'Are you currently working somewhere?',
          answer: false,
          type: 'boolean',
        },
        'Current Employer Name': {
          question: 'Current Employer Name',
          answer: '',
          required: true,
          required: true,
          type: 'text',
        },
        'How long have you been working here?': {
          question: 'How long have you been working here?',
          answer: '',
          type: 'number',
          required: true,
        },
        'Employed as': {
          question: 'Employed as',
          answer: '',
          type: 'text',
          required: true,
        },
        Address: {
          question: 'Address',
          answer: '',
          type: 'address',
          required: true,
        },
        'Phone Number': {
          question: 'Phone Number',
          answer: '',
          type: 'tel',
          required: true,
        },
        'Monthly Salary': {
          question: 'Monthly Salary',
          answer: 'Monthly Salary',
          type: 'number',
          required: true,
        },
      },
      1: {
        'Have you ever worked before?': {
          question: 'Have you ever worked before?',
          answer: false,
          type: 'boolean',
        },
        'Previous Employer Name': {
          question: 'Previous Employer Name',
          answer: '',
          type: 'text',
          required: true,
        },
        'How long did you work here?': {
          question: 'How long did you work here?',
          answer: '',
          type: 'number',
          required: true,
        },
        'Employed as': {
          question: 'Employed as',
          answer: '',
          type: 'text',
          required: true,
        },
        Address: {
          question: 'Address',
          answer: '',
          type: 'address',
          required: true,
        },
        'Phone Number': {
          question: 'Phone Number',
          answer: '',
          type: 'tel',
          required: true,
        },
      },
      2: {
        'How are you paying for monthly rent? (optional)': {
          question: 'How are you paying for monthly rent? (optional)',
          placeholder: 'Source should be where majority of rent comes from',
          type: 'selector',
          required: false,
          answer: '',
        },

        'How will this source cover your monthly rent?': {
          question: 'How will this source cover your monthly rent?',
          type: 'textarea',
          required: true,
          answer: '',
        },
      },
    },
  },
  personalReferences: {
    title: 'Personal references',
    questions: {
      0: {
        'Full Name': {
          question: 'Full Name',
          answer: '',
          required: true,
          type: 'text',
        },
        'Phone Number': {
          question: 'Phone Number',
          answer: '',
          required: true,
          type: 'tel',
        },
        Email: {
          question: 'Email',
          answer: '',
          required: true,
          type: 'email',
        },
        Relationship: {
          question: 'Relationship',
          answer: '',
          required: true,
          type: 'text',
        },
      },
      1: {
        'Full Name': {
          question: 'Full Name',
          answer: '',
          type: 'text',
          required: false,
        },
        'Phone Number': {
          question: 'Phone Number',
          answer: '',
          required: false,
          type: 'tel',
        },
        Email: {
          question: 'Email',
          answer: '',
          required: false,
          type: 'email',
        },
        Relationship: {
          question: 'Relationship',
          answer: '',
          required: false,
          type: 'text',
        },
      },
    },
  },

  lifestyleInformation: {
    title: 'Lifestyle Information',
    questions: {
      0: {
        'Do you have pets?': {
          question: 'Do you have pets?',
          answer: false,
          type: 'boolean',
        },
        'How many?': {
          question: 'How many?',
          answer: '',
          type: 'number',
        },
        'What kind of pets?': {
          question: 'What kind of pets?',
          answer: '',
          type: 'selector',
        },

        'Do you have any emotional support pets?': {
          question: 'Do you have any emotional support pets?',
          answer: false,
          type: 'boolean',
        },
        'Do you smoke?': {
          question: 'Do you smoke?',
          answer: false,
          type: 'boolean',
        },
      },
    },
  },
  otherInformation: {
    title: 'Other Information',
    questions: {
      0: {
        'Have you ever been evicted?': {
          question: 'Have you ever been evicted?',
          answer: false,
          type: 'boolean',
          long_answer: ''
        },

        'Has an applicant ever refused to pay rent when due? ': {
          question: 'Has an applicant ever refused to pay rent when due? ',
          type: 'boolean',
          answer: false,
          long_answer: ''
        },

        'Has an applicant ever been convicted of a gross misdemeanor or felony?': {
          question:
            'Has an applicant ever been convicted of a gross misdemeanor or felony?',
          type: 'boolean',
          answer: false,
          long_answer: ''
        },
      },
    },
  },
  status: 'pending',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PROPERTY_APPLICATION:
      return {
        ...state,
        status: action.payload.status,
        applicationInformation: action.payload.data.applicationInformation,
        rentalHistory: action.payload.data.rentalHistory,
        workHistory: action.payload.data.workHistory,
        personalReferences: action.payload.data.personalReferences,
        lifestyleInformation: action.payload.data.lifestyleInformation,
        otherInformation: action.payload.data.otherInformation,
      };
    case CHANGE_STATUS_SA:
      return {...state, status: action.payload};
    case UPDATE_STEP:
      return {
        ...state,
        [action.payload.step]: {
          ...state[action.payload.step],
          questions: action.payload.data,
        },
      };
    default:
      return state;
  }
};
