import {
  GET_MAP_VIEW_LIST,
  GET_VIEW_LIST,
  INIT_PROPERTY_LIST,
  SET_CURRENT_FILTER,
  SET_FAVORITES_PROPERTIES,
  REMOVE_PROPERTY_FROM_FAVORITES_LIST,
  ADD_PROPERTY_TO_FAVORITES_LIST,
  SET_FILTER_RANGE
} from '../actions/types';

const initialState = {
  favorites_list: {
    list: [],
    mapList: [],
  },
  campus: {},
  list: [],
  sort: {},
  filter: {},
  mapList: [],
  start_of: 0,
  finish_of: 5,
  count: 0,
  filter_range: null,
};

export default (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case INIT_PROPERTY_LIST:
      return {
        ...state,
        campus: payload.campus
      };
    case GET_MAP_VIEW_LIST:
      return {
        ...state,
        mapList: [...payload.data],
      };
    case GET_VIEW_LIST: {
      return {
        ...state,
        start_of: payload.start_of,
        finish_of: payload.finish_of,
        count: payload.count,
        list: payload.is_new_filtering
          ? [...payload.data]
          : [...state.list, ...payload.data],
      };
    }
    case SET_FILTER_RANGE:
      return {
        ...state,
        filter_range: {...payload}
      }
    case SET_CURRENT_FILTER:
      return {
        ...state,
        filter: {...payload}
        // sort: payload.sort
      };
    case SET_FAVORITES_PROPERTIES:
      return {
        ...state,
        favorites_list: {
          ...state.favorites_list,
          list: action.payload.list,
          mapList: action.payload.mapList,
        }
      }  
    case ADD_PROPERTY_TO_FAVORITES_LIST:
      const index = state.mapList.findIndex(i => i._id === action.payload._id)
      const mapItem = state.mapList[index]
      return {
        ...state,
        favorites_list: {
          ...state.favorites_list,
          list: [...state.favorites_list.list, action.payload],
          mapList: [...state.favorites_list.mapList, mapItem]
        }
      }  
    case REMOVE_PROPERTY_FROM_FAVORITES_LIST:
      const favorites_list_list = [...state.favorites_list.list]
      const favorites_list_map = [...state.favorites_list.mapList]

      const deleteIndexFavoritesList = state.favorites_list.list.findIndex(item => item._id === action.payload._id)
      const deleteIndexFavoritesMap = state.favorites_list.mapList.findIndex(item => item._id === action.payload._id)
      
      favorites_list_list.splice(deleteIndexFavoritesList, 1)
      favorites_list_map.splice(deleteIndexFavoritesMap, 1)
      return {
        ...state,
        favorites_list: {
          ...state.favorites_list,
          list: favorites_list_list,
          mapList: favorites_list_map
        }
      }  
    default:
      return state;
  }
};
