import {
  INIT_PROPERTY_STATE,
  PREVIOUS_PROPERTY_STEP,
  NAVIGATE_TO_PROPERTY_STEP,
  SUBMIT_FIRST_STEP,
  SUBMIT_PROPERTY_INFO_STEP,
  SUBMIT_UNITS_INFO_STEP,
  UPDATE_STEPS,
  CLEAR_PROPERTY_STORE,
} from '../actions/types';

const initialState = {
  unitsType: 'Single',
  address: '',
  propertyType: 'Apartment complex',
  units: [],
  propertyName: '',
  photos: [],
  videos: [],
  links: [],
  propertyAmenities: [],
  campuses: [],
  propertyDescription: '',
  neighbourhood: ' ',
  distance: [],
  paymentPlan: ' ',
  activeListing: true,
  current_step: 1,
  steps: {
    step1: {
      active: true,
      completed: false,
    },
    step2: {
      active: false,
      completed: false,
    },
    step3: {
      active: false,
      completed: false,
    },
    step4: {
      active: false,
      completed: false,
    },
  },
  in_progress: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case INIT_PROPERTY_STATE:
      return state;
    case SUBMIT_FIRST_STEP:
      return {
        ...state,
        unitsType: action.payload.unitsType,
        address: action.payload.address,
        propertyType: action.payload.propertyType,
        propertyName: action.payload.propertyName,
        units: action.payload.units,
        current_step: action.payload.current_step,
        campuses: action.payload.campuses,
        in_progress: true,
      };
    case SUBMIT_PROPERTY_INFO_STEP:
      return {
        ...state,
        ...action.payload.data,
        units: [...action.payload.data.units],
        current_step: action.payload.next_step,
      };
    case SUBMIT_UNITS_INFO_STEP:
      return {
        ...state,
        units: [...action.payload.data.units],
        current_step: state.current_step + 1,
      };
    case PREVIOUS_PROPERTY_STEP: {
      const steps_difference =
        state.unitsType === 'Single' && state.current_step === 4 ? 2 : 1;
      return {
        ...state,
        ...action.payload.data,
        units: [...action.payload.data.units],
        current_step: state.current_step - steps_difference,
        steps: {
          ...state.steps,
          [`step${state.current_step}`]: {
            ...state.steps[`step${state.current_step}`],
            active: false,
          },
          [`step${state.current_step - steps_difference}`]: {
            ...state.steps[`step${state.current_step - 1}`],
            active: true,
          },
        },
      };
    }
    case NAVIGATE_TO_PROPERTY_STEP:
      return {
        ...state,
        current_step: action.payload.current_step,
      };
    case UPDATE_STEPS:
      return {
        ...state,
        steps: {
          ...state.steps,
          ...action.payload.step,
        },
      };
    case CLEAR_PROPERTY_STORE:
      return {
        state: undefined,
      };
    default:
      return state;
  }
};
