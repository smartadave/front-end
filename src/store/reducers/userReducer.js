import {
  GET_USER,
  UPDATE_USER_MAIN_INFO,
  SET_SAVED_FILTER,
  ADD_PROPERTY_TO_FAVORITES,
  REMOVE_PROPERTY_FROM_FAVORITES,
} from '../actions/types';

const initialState = {
  saved_filters: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return {...state, ...action.payload};
    case UPDATE_USER_MAIN_INFO:
      return {
        ...state,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        phoneNumber: action.payload.phoneNumber,
      };
    case SET_SAVED_FILTER:
      return {
        ...state,
        saved_filters: action.payload,
      };
    case ADD_PROPERTY_TO_FAVORITES:
      return {
        ...state,
        favouriteProperties: [...state.favouriteProperties, action.payload],
      };
    case REMOVE_PROPERTY_FROM_FAVORITES:
      const deleteIndex = state.favouriteProperties.indexOf(id => id === action.payload)
      const deleteArrayFavouriteProperties = [...state.favouriteProperties]
      deleteArrayFavouriteProperties.splice(deleteIndex, 1)
      return {
        ...state,
        favouriteProperties: deleteArrayFavouriteProperties,
      };
    default:
      return state;
  }
};
