import cookie from "js-cookie";

export const setCookie = (key, value) => {
  if (process.browser) {
    cookie.set(key, value);
  }
};

export const removeCookie = key => {
  if (process.browser) {
    cookie.remove(key);
  }
};

export const clearCookie = () => {
  Object.keys(cookie.get()).forEach(function (cookieName) {
    cookie.remove(cookieName);
  });
};

export const getCookie = (key, req) => {
  return cookie.getJSON(key);
};
