export const distanceCalculate = async (startPoint, endPoint) => {
  return new Promise((resolve, reject) => {
    let ready_script = document.getElementById('distanceCalculate');

    let script = ready_script ? ready_script : document.createElement('script');
    if (!ready_script) {
      script.type = 'text/javascript';
      script.id = 'distanceCalculate';
      script.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_PLACE_API_KEY}`;
      document.getElementsByTagName('head')[0].appendChild(script);
    }
    
    if (script.readyState) {
      script.onreadystatechange = async function () {
        if (
          script.readyState === 'loaded' ||
          script.readyState === 'complete'
        ) {
          script.onreadystatechange = null;
          const data = await getDistance(startPoint, endPoint);
          resolve(data)
        }
      };
    } else {
      script.addEventListener('load', async() => {
        const data = await getDistance(startPoint, endPoint);
        resolve(data)
      })
    }
  });
};

const getDistance = (startPoint, endPoint) => {
  const route_data = {
    DRIVING: '',
    WALKING: '',
    WAY: '',
  };

  const directionsService = new window.google.maps.DirectionsService();

  return new Promise((resolve, reject) => {
    directionsService.route(
      {
        origin: startPoint,
        destination: endPoint,
        travelMode: 'WALKING',
      },
      (response, status) => {
        if (status === 'OK') {
          route_data['WALKING'] = response.routes[0].legs[0].duration.text;
          directionsService.route(
            {
              origin: startPoint,
              destination: endPoint,
              travelMode: 'DRIVING',
            },
            (response, status) => {
              if (status === 'OK') {
                route_data['DRIVING'] =
                  response.routes[0].legs[0].duration.text;
                route_data['WAY'] = response.routes[0].legs[0].distance.text;
                resolve(route_data);
                return route_data;
              } else {
                reject(status);
              }
            },
          );
        }
      },
    );
  });
};
