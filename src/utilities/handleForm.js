export const formToObject = (formData) => {
    const formArray = Array.from(formData)
            .filter(element => element.type !== "submit" && element.type !== "button")
            .map(element => ({
                [element.name]: element.value
            }));
        const obj = Object.assign({}, ...formArray);
        return obj;
};
