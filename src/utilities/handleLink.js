export const handleLink = (message) => {
  if (message.search("http") < 0) {
    return -1;
  } else {
    if (
      message.search(".png") >= 0 ||
      message.search(".jpg") >= 0 ||
      message.search(".jpeg") >= 0 ||
      message.search(".svg") >= 0
    ) {
      return -2;
    } else if (
      message.search(".mp4") >= 0 ||
      message.search(".mpg") >= 0 ||
      message.search(".mov") >= 0 ||
      message.search(".MPG") >= 0
    ) {
      return -4;
    } else if (message.search(".mp3") >= 0 || message.search(".wav") >= 0) {
      return -3;
    } else return -1;
  }
};
