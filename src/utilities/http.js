import axios from "axios";
import { getCookie, setCookie, removeCookie, clearCookie } from "./cookie";

const http = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [
    function(data, headers = {}) {
      if (
        Boolean(getCookie("token")) &&
        getCookie("token") !== "undefined"
      ) {
        headers["Authorization"] = `Bearer ${getCookie("token")}`;
      }
      if (
        Boolean(getCookie("refresh_token")) &&
        getCookie("token") !== "undefined"
      ) {
        headers["RefreshToken"] = getCookie("refresh_token")
      }

      if (headers["Content-Type"] !== "multipart/form-data") {
        headers["Content-Type"] = "application/json";
        if (typeof data !== "string") {
          data = JSON.stringify(data);
        }
      }
      if (!headers["AccessToken"]) {
        delete headers["AccessToken"];
      }
      if (!headers["RefreshToken"]) {
        delete headers["RefreshToken"];
      }

      return data;
    },
  ],
});

http.interceptors.response.use(
  async (response) => {
    return Promise.resolve(response);
  },
  async (error) => {
    console.log(error)
    if (error.config && error.response && error.response.status === 401 && getCookie("refresh_token")) {
      const payload = {
        'refresh_token': getCookie("refresh_token")
      }
      getRefreshToken(payload).then(resp => {
        if(resp) {
          console.log('here')
          error.response.config.headers['Authorization'] = 'Bearer ' + getCookie("token");
          return axios(error.response.config);
        }
        else {
          clearCookie();
          window.location = "/login";
        }
      }).catch(error => {
        clearCookie();
        window.location = "/login";
      })
    }
    else if (error.response.status === 403) {
      clearCookie();
      window.location = "/login";
    }
    return Promise.reject(error.response);
  }
);

const getErrorMessage = (error) => {
  let resp = "";

  try {
    resp = error.response.data.message;
  } catch (error) {
    resp = "Sorry, something went wrong. Please try again later.";
  }

  return resp;
};


const getRefreshToken = (payload) => {
  return http
      .post('auth/refresh', payload)
      .then((resp) => {
        console.log(resp)
        if(resp.data.status.status === 201) {
          setCookie('user', resp.data.data.user);
          setCookie('token', resp.data.data.payload.token);
          setCookie('refresh_token', resp.data.data.payload.refresh_token);
          return true
        }
        else return false
      })
      .catch((error) => {
       console.log(error)
       return false
      })
}

export default http;
export { getErrorMessage };
