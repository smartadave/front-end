import React from "react";
import { Redirect, Route } from "react-router-dom";
import { getCookie } from './cookie';

export const ProtectedRoute = ({ component: Component, ...rest }) => {
  const is_authenticate = isAuthenticate();
  return (is_authenticate) ? (
    <Route {...rest} />
  ) : (
    <Redirect to="/login" />
  );
};

export const ManagerGuard = ({ component: Component, ...rest }) => {
  const is_authenticate = isAuthenticate();
  const user = getCookie("user");
  if(is_authenticate && user.role === 'manager') {
    return  <Route {...rest} />
  }
  else {
    return <Redirect to="/dashboard" />
  }
};

export const StudentGuard = ({ component: Component, ...rest }) => {
  const is_authenticate = isAuthenticate();
  const user = getCookie("user");
  if(is_authenticate && user.role === 'student') {
    return  <Route {...rest} />
  }
  else {
    return <Redirect to="/dashboard" />
  }
};

const isAuthenticate = () => {
  const token = getCookie("token");
  const refresh_token = getCookie("refresh_token");
  return token && refresh_token;
};