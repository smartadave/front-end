var settings = {
  errored_input_classname: 'input-field--error',
  error_text_classname: 'input-filed-text-error',
  error_text_data_attribute: 'data-error-text',
  error_checkbox_classname: 'checkbox-field-error',
};

export default function checkForm(form, blur_active_element) {
  resetForm(form);

  var errorsFound = false,
    requiredInputs = form.querySelectorAll(
      'input[required], textarea[required], select[required]',
    ),
    email_regex = /.+@.+\.[A-Za-z]+$/,
    price_regex = /(?=.*\d)^\$\s?(([1-9]\d{0,2}(,\d{3})*)|0)?(\.\d{1,2})?$/,
    phone_regex = /^[(][2-9]{3}[)]\s\d{3}-\d{4}$/gm,
    password = '',
    is_reset_password = false,
    old_password = '';

  for (var i = 0; i < requiredInputs.length; i++) {
    var input = requiredInputs[i],
      val = input.value;

    if (input.type === 'checkbox') {
      if (!input.checked) {
        if (input.name === 'terms_of_use') {
          createError(
            input,
            '',
            'checkbox',
          );
          errorsFound = true;
        }
      }
      if (input.checked) {
        if (input.name === 'utilitiesIncluded') {
          var utilities = document.querySelectorAll('input.utility-checked');
          if(utilities.length === 0) {
            createError(input, 'Select at least one utility below');
            errorsFound = true;
          }
        }
      }
    }
    if (input.type === 'select-one') {
      if(input.name === 'campus' && input.value === 'placeholder') {
        createError(input, 'Select School Campus');
        errorsFound = true;
      }
    }
    if (val.length > 0) {
      if (input.type === 'email') {
        if (email_regex.test(val) === false) {
          createError(input, 'Invalid email address');
          errorsFound = true;
        } else if (val.length === 0) {
          createError(input, 'This field is mandatory');
          errorsFound = true;
        }
      }
      if (input.type === 'password') {
        if (input.name === 'old_password') {
          old_password = val;
          is_reset_password = true;
        }
        if(input.name === 'new_password') {
          password = input.name !== 'confirm_password' ? val : password;
        }
        if (input.name === 'new_password' && is_reset_password && val === old_password) {
          createError(input, 'New password can not be the same as an old one');
          errorsFound = true;
        }
        if (val.length < 6) {
          createError(input, 'Password must be 6 character long');
          errorsFound = true;
        } else if (input.name === 'confirm_password' && val !== password) {
          createError(input, 'Passwords do not match');
          errorsFound = true;
        } else if (val.length === 0) {
            createError(input, 'This field is mandatory');
            errorsFound = true;
        }
      }
      if (input.type === 'text') {
        var isInputPrice = input.name.includes('price');
        var isInputPhone = input.name.includes('phone');
        if (isInputPrice) {
          if(price_regex.test(val) === false) {
            createError(input, 'Invalid format');
            errorsFound = true;
          }
        }
        if (isInputPhone) {
          if (!phone_regex.test(val)) {
            createError(input, 'Invalid format');
            errorsFound = true;
          }
        }
        if(input.name === 'new_password') {
          password = input.name !== 'confirm_password' ? val : password;
          if (val.length < 6) {
            createError(input, 'Password must be 6 character long');
            errorsFound = true;
          }
        }
        if (input.name === 'old_password') {
          old_password = val;
          is_reset_password = true;
        }
        if (input.name === 'new_password' && is_reset_password && val === old_password) {
          createError(input, 'New password can not be the same as an old one');
          errorsFound = true;
        }
        if (input.name === 'confirm_password' && val !== password) {
          createError(input, 'Passwords do not match');
          errorsFound = true;
        } else if (input.name === 'confirm_password' && val.length < 6) {
          createError(input, 'Password must be 6 character long');
          errorsFound = true;
        }
      }
      if(input.type === 'url') {
        if(input.name === "media_link") {
          if(val.search('youtube.com') < 0 && val.search('vimeo.com') < 0) {
            createError(input, 'This Video is not support');
          }
        }
      }
    } else if (input.placeholder === 'Select one...') {
        createError(input, `Select ${input.labels[0].textContent}`);
        errorsFound = true;
    } else {
      createError(input, 'This field is mandatory');
      errorsFound = true;
    }
  }

  if (errorsFound) {
    return false;
  }

  if (blur_active_element) {
    document.activeElement.blur();
  }

  return true;
}

export function createError(input, message, type) {
  input.classList.add(
    type === 'checkbox'
      ? settings.error_checkbox_classname
      : settings.errored_input_classname,
  );

  if (message && type !== 'checkbox') {
    var errorMessageNode = document.createElement('span');

    errorMessageNode.classList.add(settings.error_text_classname);
    errorMessageNode.innerText = message;

    input.parentNode.insertBefore(errorMessageNode, input.nextSibling);
  }
}

export function resetForm(form) {
  var erroredInputs = form.querySelectorAll(
      '.' + settings.errored_input_classname,
    ),
    errorMessageNodes = form.querySelectorAll(
      '.' + settings.error_text_classname,
    );

  for (var j = 0; j < erroredInputs.length; j++) {
    var input = erroredInputs[j];
    input.classList.remove(settings.errored_input_classname);
    
  }

  for (var k = 0; k < errorMessageNodes.length; k++) {
    var errorMessageNode = errorMessageNodes[k];
    errorMessageNode.parentNode.removeChild(errorMessageNode);
  }
}
